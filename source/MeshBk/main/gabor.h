#ifndef GABOR_H
#define GABOR_H

#include <opencv2/core/core.hpp>

cv::Mat getGaborRealKernel( cv::Size ksize, double sigma, double theta,
                            double lambd, double gamma, double psi, int ktype );

cv::Mat getGaborImagineKernel( cv::Size ksize, double sigma, double theta,
                            double lambd, double gamma, double psi, int ktype );
#endif // GABOR_H
