#ifndef OPACITYFROMFILEWIDGET_H
#define OPACITYFROMFILEWIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QProgressBar>
#include <treemanagerwidget.h>

namespace Ui {
class OpacityFromFileWidget;
}

class OpacityFromFileWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OpacityFromFileWidget(QWidget *parent = 0);
    ~OpacityFromFileWidget();

    void initializeWidget();
    static QProgressBar* progressBar;
    static bool QCallBack(const int pos, const char* str);
public:

    MeshDocument* md;

    TreeItem *currentApplyNode = NULL;
    TreeItem *currentOtherNode = NULL;
    TreeItem *currentParentNode = NULL;

    void applyMask();

public:
    inline void setMeshDoc(MeshDocument *meshDoc) {md = meshDoc;}
    void setTFOptions();

public slots:
    void startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *organ, TreeItem *other);

signals:
    void endTransferFunc();

private slots:
    void on_importFileButton_clicked();

    void on_applyTFButton_clicked();

private:
    Ui::OpacityFromFileWidget *ui;
};

#endif // OPACITYFROMFILEWIDGET_H

vcg::Color4f tfOpacityFromFile(signed short intensityArray[], int index, NodeViewOptions options);
vcg::Color4f tfOpacityFromFileInverted(signed short* intensityArray, int index, NodeViewOptions options);
