/********************************************************************************
** Form generated from reading UI file 'volumepropertywidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VOLUMEPROPERTYWIDGET_H
#define UI_VOLUMEPROPERTYWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "histogramcolorwidget.h"

QT_BEGIN_NAMESPACE

class Ui_VolumePropertyWidget
{
public:
    QVBoxLayout *verticalLayout;
    QTableWidget *colorTable;
    QHBoxLayout *renderlayout;
    HistogramColorWidget *histogramColorWidget;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_3;
    QComboBox *organTypeCombobox;
    QPushButton *applyTFButton;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *applyButton;
    QSpacerItem *horizontalSpacer;
    QProgressBar *progressbar;

    void setupUi(QWidget *VolumePropertyWidget)
    {
        if (VolumePropertyWidget->objectName().isEmpty())
            VolumePropertyWidget->setObjectName(QString::fromUtf8("VolumePropertyWidget"));
        VolumePropertyWidget->resize(400, 603);
        VolumePropertyWidget->setMinimumSize(QSize(395, 0));
        verticalLayout = new QVBoxLayout(VolumePropertyWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(2, 2, 2, 2);
        colorTable = new QTableWidget(VolumePropertyWidget);
        colorTable->setObjectName(QString::fromUtf8("colorTable"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(colorTable->sizePolicy().hasHeightForWidth());
        colorTable->setSizePolicy(sizePolicy);
        colorTable->setMinimumSize(QSize(0, 0));
        colorTable->setMaximumSize(QSize(16777215, 2));

        verticalLayout->addWidget(colorTable);

        renderlayout = new QHBoxLayout();
        renderlayout->setObjectName(QString::fromUtf8("renderlayout"));

        verticalLayout->addLayout(renderlayout);

        histogramColorWidget = new HistogramColorWidget(VolumePropertyWidget);
        histogramColorWidget->setObjectName(QString::fromUtf8("histogramColorWidget"));

        verticalLayout->addWidget(histogramColorWidget);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_2 = new QLabel(VolumePropertyWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(label_2);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        organTypeCombobox = new QComboBox(VolumePropertyWidget);
        organTypeCombobox->setObjectName(QString::fromUtf8("organTypeCombobox"));

        horizontalLayout_4->addWidget(organTypeCombobox);


        verticalLayout->addLayout(horizontalLayout_4);

        applyTFButton = new QPushButton(VolumePropertyWidget);
        applyTFButton->setObjectName(QString::fromUtf8("applyTFButton"));

        verticalLayout->addWidget(applyTFButton);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        applyButton = new QPushButton(VolumePropertyWidget);
        applyButton->setObjectName(QString::fromUtf8("applyButton"));

        horizontalLayout_2->addWidget(applyButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout_2);

        progressbar = new QProgressBar(VolumePropertyWidget);
        progressbar->setObjectName(QString::fromUtf8("progressbar"));
        progressbar->setMaximumSize(QSize(16777215, 10));
        progressbar->setValue(24);

        verticalLayout->addWidget(progressbar);


        retranslateUi(VolumePropertyWidget);

        QMetaObject::connectSlotsByName(VolumePropertyWidget);
    } // setupUi

    void retranslateUi(QWidget *VolumePropertyWidget)
    {
        VolumePropertyWidget->setWindowTitle(QApplication::translate("VolumePropertyWidget", "Form", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("VolumePropertyWidget", "Probability Model", 0, QApplication::UnicodeUTF8));
        organTypeCombobox->clear();
        organTypeCombobox->insertItems(0, QStringList()
         << QApplication::translate("VolumePropertyWidget", "Artery", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("VolumePropertyWidget", "Bone", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("VolumePropertyWidget", "Kidney", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("VolumePropertyWidget", "Liver", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("VolumePropertyWidget", "Lung", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("VolumePropertyWidget", "Portalvein", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("VolumePropertyWidget", "Venous System", 0, QApplication::UnicodeUTF8)
        );
        applyTFButton->setText(QString());
        applyButton->setText(QApplication::translate("VolumePropertyWidget", "Apply", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class VolumePropertyWidget: public Ui_VolumePropertyWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VOLUMEPROPERTYWIDGET_H
