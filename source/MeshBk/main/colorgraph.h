#ifndef COLORGRAPH_H
#define COLORGRAPH_H


#include "../common/interfaces.h"
#include <QGLWidget>

class ColorGraph : public QGLWidget
{
    Q_OBJECT
public:
    explicit ColorGraph(QWidget *parent = 0);

    void resizeGL();
    void initializeGL();
    void paintGL();
    void setColorDict(QMap<float, vcg::Color4f>* color);
    float grayMax = 1023;
    float grayMin = -1024;
    QMap<float, vcg::Color4f> *colorDict = NULL;

public slots:
    void colorDictChanged(QMap<float, vcg::Color4f>* colorDict);

signals:
    void addColorPoint(float gray, float red, float green, float blue, float alpha);
private:
    QColor backgroundColor;

    void drawColorPoint(float grayValue, float alpha);
    void drawCirle(QPointF center, vcg::Color4f color);
    void drawGradient();
    void drawNumber(QPointF center, QString number);
    float alphaOffset = 10;
    float colorPointRadius = 150;
protected:
    void mouseDoubleClickEvent(QMouseEvent *event);
};

#endif // COLORGRAPH_H
