/********************************************************************************
** Form generated from reading UI file 'opacityfromfilewidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPACITYFROMFILEWIDGET_H
#define UI_OPACITYFROMFILEWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QTableWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OpacityFromFileWidget
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *importFileButton;
    QLineEdit *importFileEditText;
    QTableWidget *tableWidget;
    QPushButton *applyTFButton;
    QProgressBar *progressBar;

    void setupUi(QWidget *OpacityFromFileWidget)
    {
        if (OpacityFromFileWidget->objectName().isEmpty())
            OpacityFromFileWidget->setObjectName(QString::fromUtf8("OpacityFromFileWidget"));
        OpacityFromFileWidget->resize(400, 485);
        verticalLayout = new QVBoxLayout(OpacityFromFileWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        importFileButton = new QPushButton(OpacityFromFileWidget);
        importFileButton->setObjectName(QString::fromUtf8("importFileButton"));
        importFileButton->setEnabled(false);

        horizontalLayout->addWidget(importFileButton);

        importFileEditText = new QLineEdit(OpacityFromFileWidget);
        importFileEditText->setObjectName(QString::fromUtf8("importFileEditText"));
        importFileEditText->setEnabled(false);

        horizontalLayout->addWidget(importFileEditText);


        verticalLayout->addLayout(horizontalLayout);

        tableWidget = new QTableWidget(OpacityFromFileWidget);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        verticalLayout->addWidget(tableWidget);

        applyTFButton = new QPushButton(OpacityFromFileWidget);
        applyTFButton->setObjectName(QString::fromUtf8("applyTFButton"));
        applyTFButton->setEnabled(true);

        verticalLayout->addWidget(applyTFButton);

        progressBar = new QProgressBar(OpacityFromFileWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);
        progressBar->setTextVisible(false);

        verticalLayout->addWidget(progressBar);


        retranslateUi(OpacityFromFileWidget);

        QMetaObject::connectSlotsByName(OpacityFromFileWidget);
    } // setupUi

    void retranslateUi(QWidget *OpacityFromFileWidget)
    {
        OpacityFromFileWidget->setWindowTitle(QApplication::translate("OpacityFromFileWidget", "Form", 0, QApplication::UnicodeUTF8));
        importFileButton->setText(QApplication::translate("OpacityFromFileWidget", "Import File", 0, QApplication::UnicodeUTF8));
        applyTFButton->setText(QApplication::translate("OpacityFromFileWidget", "Apply Transfer Function", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OpacityFromFileWidget: public Ui_OpacityFromFileWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPACITYFROMFILEWIDGET_H
