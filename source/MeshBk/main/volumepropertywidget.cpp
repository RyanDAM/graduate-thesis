#include "volumepropertywidget.h"
#include "ui_volumepropertywidget.h"

#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QTableWidget>
using namespace vcg;
QProgressBar *VolumeProperty::progressbar;
NNPredict nnpredict;
VolumePropertyWidget::VolumePropertyWidget(QWidget *parent) :
    VolumeProperty(parent),
    ui(new Ui::VolumePropertyWidget)
{
    ui->setupUi(this);
    QTableWidget *colorTable = ui->colorTable;
    colorTable->setColumnCount(9);
    colorTable->horizontalHeader()->setResizeMode(2, QHeaderView::Stretch);
    colorTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Intensity"));
    colorTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Color"));
    colorTable->setHorizontalHeaderItem(2, new QTableWidgetItem("Opacity"));
    colorTable->setHorizontalHeaderItem(3, new QTableWidgetItem(""));
    colorTable->setColumnWidth(1, 50);
    colorTable->setColumnWidth(3, 24);
    // Hide save data columns
    colorTable->setColumnHidden(4, true);
    colorTable->setColumnHidden(5, true);
    colorTable->setColumnHidden(6, true);
    colorTable->setColumnHidden(7, true);
    colorTable->setColumnHidden(8, true);
    colorMapper = new QSignalMapper(this);
    grayMapper = new QSignalMapper(this);
    alphaMapper = new QSignalMapper(this);
    removeMapper = new QSignalMapper(this);
    initPreset();
    initCurrentMap();

    // Init progressbar
    progressbar = ui->progressbar;
    progressbar->setTextVisible(false);
    progressbar->setMaximum(100);
    progressbar->setMinimum(0);
    progressbar->hide();
    ui->applyTFButton->setVisible(false);
    ui->histogramColorWidget->QCallBack = QCallBack;
    ui->applyButton->setEnabled(false);
    connect(ui->histogramColorWidget, SIGNAL(colorTableChanged()), this, SLOT(colorTableChanged()));
    connect(ui->histogramColorWidget->histogramWidget, SIGNAL(colorTableChanged()), this, SLOT(colorTableChanged()));
    connect(ui->histogramColorWidget, SIGNAL(reloadHistogram()), this, SLOT(reloadHistogram()));
    connect(ui->histogramColorWidget, SIGNAL(enableApplyButton()), this, SLOT(enableApplyButton()));
}

VolumePropertyWidget::~VolumePropertyWidget()
{
    delete ui;
}

void VolumePropertyWidget::initPreset()
{
    // Init color list preset
    colorDictPreset[-1024] = Color4f(0, 0, 0, 0);
//    colorDictPreset[-3024] = Color4f(0, 0, 0, 0);
    colorDictPreset[-86.9767] = Color4f(0, 0.25098 * 255, 255, 0);
    colorDictPreset[45.3791] = Color4f(255, 0, 0, 0.169643 * 255);
    colorDictPreset[139.919] = Color4f(255, 0.894893 * 255, 0.894893 * 255, 0.589286 * 255);
    colorDictPreset[347.907] = Color4f(255, 255, 0.25098 * 255, 0.607143 * 255);
    colorDictPreset[1023] = Color4f(255, 255, 255, 0.607143 * 255);
//    colorDictPreset[1224.16] = Color4f(255, 255, 255, 0.607143 * 255);
//    colorDictPreset[3071] = Color4f(0.827451 * 255, 0.658824 * 255, 255, 0.616071 * 255);
}

void VolumePropertyWidget::initCurrentMap()
{
    QMap<float, Color4f>::Iterator it = colorDictPreset.begin();
    for (; it != colorDictPreset.end(); ++it) {
        currentMap[it.key()] = it.value();
    }
}

Color4f VolumePropertyWidget::mapIntensity(float intensity)
{
    // If there is no color map, just return the intensity
    if (currentMap.size() == 0) return Color4f(intensity, intensity, intensity, 255);
    QMap<float, Color4f>::Iterator i = currentMap.lowerBound(intensity);
    if (i == currentMap.begin()) {
        return currentMap[i.key()];
    } else {
        float lowerBound = (i - 1).key();
        float upperBound = (i.key());
        Color4f lowerColor = currentMap[lowerBound];
        Color4f upperColor = currentMap[upperBound];

        // Linear Interpolation
        float red = lowerColor[0] +
                1.0 * (intensity - lowerBound) *
                (upperColor[0] - lowerColor[0]) / (upperBound - lowerBound);
        float green = lowerColor[1] +
                (intensity - lowerBound) *
                1.0 * (upperColor[2] - lowerColor[1]) / (upperBound - lowerBound);
        float blue = lowerColor[2] +
                (intensity - lowerBound) *
                1.0 * (upperColor[2] - lowerColor[2]) / (upperBound - lowerBound);
        float alpha = lowerColor[3] +
                1.0 * (intensity - lowerBound) *
                (upperColor[3] - lowerColor[3]) / (upperBound - lowerBound);
        return Color4f(red, green ,blue , alpha);
    }
}

bool VolumePropertyWidget::QCallBack(const int pos, const char *str)
{
    progressbar->show();
    progressbar->setValue(pos);
}

void VolumeProperty::updateTexture3dRgbaDataSuccess() {
   progressbar->hide();
}

void VolumePropertyWidget::usedColorTableChange(int isChecked) {
    isUsedColorTable = isChecked;
}

// Whenever user add / remove a volume, current histogram should be updated as well
void VolumePropertyWidget::updateHistogram()
{
    //TODO already connected
}

vcg::Color4f VolumePropertyWidget::mapIndex(int index) {
    if (indexMap.size() > 0) {
        if (indexMap[index]) {
            return Color4f(255, 0, 0, 255);
        } else {
            return Color4f(0, 0, 0, 0);
        }
    } else {
        return Color4f(0, 0, 0, 0);
    }
}

void VolumePropertyWidget::setMeshDocument(MeshDocument *md)
{
    this->md = md;
    connect(md, SIGNAL(updateHistogram()), this, SLOT(updateHistogram()));
}

void VolumePropertyWidget::applyHistogram(TransfuncOptions options, TreeItem* parentNode, TreeItem* applyNode, TreeItem* otherNode, signed short* intensityData, vcg::CallBackPos* cb)
{
    int index = 0;
    int count = 100;
    int step = applyNode->maxIndex / count;
    count = applyNode->maxIndex / step;
    for (int i = 0; i <= applyNode->maxIndex; ++i) {
        int intensity = intensityData[i] - 1024; // TODO hardcode 1024
        if (intensity >= options.intensityLeftBound && intensity <= options.intensityRightBound) {
            applyNode->opacity[i] = 1 * parentNode->opacity[i];
            otherNode->opacity[i] = 0;
        } else {
            applyNode->opacity[i] = 0;
            otherNode->opacity[i] = 1 * parentNode->opacity[i];
        }
        if (i % step == 0) {
            if (cb)
                QCallBack(100 * index * 1.0/ count, "Building child node opcaity data");
            index++;
        }
    }
    // Update parent node
//    currentParentNode->transfuncOptions.type = TransfuncTypes::HISTOGRAM;
//    currentParentNode->transfuncOptions.intensityLeftBound = histogramGraph->leftBound;
//    currentParentNode->transfuncOptions.intensityRightBound = histogramGraph->rightBound;
    parentNode->transfuncOptions = options;
    // Update apply node
    applyNode->updateHistogram(md->globalTexture3DIntensityData, cb);
    applyNode->transfunc = tfhistogram;              // Set tf to apply node

//    TransfuncOptions applyOptions;                               // Init tf option
//    applyOptions.type = TransfuncTypes::NONE;                    //
//    currentApplyNode->transfuncOptions = applyOptions;           // asign tf option
    NodeViewOptions viewOptions;                            //
    viewOptions.colorTable = applyNode->colorTable;  // Init view option
    viewOptions.opacity = applyNode->opacity;        //
    applyNode->viewOptions = viewOptions;            // asign view option

    // Update other node
    otherNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
    otherNode->transfunc = tfhistograminverted;              // set tf to other node
//    TransfuncOptions optionsInverted;                               //  Init tf option
//    optionsInverted.type = TransfuncTypes::NONE;                    //
//    currentOtherNode->transfuncOptions = options;                   // asign tf option
    NodeViewOptions viewOptionsInverted;                            //
    viewOptionsInverted.colorTable = otherNode->colorTable;  // Init view option
    viewOptionsInverted.opacity = otherNode->opacity;        //
    otherNode->viewOptions = viewOptionsInverted;            // asign view option
}

// Main function call when apply a transfer function
// Each TF type willl be handled by a specific logic along
void VolumePropertyWidget::on_applyTFButton_clicked()
{
    switch (currentTransFuncType) {
    case TransfuncTypes::HISTOGRAM:{
        TransfuncOptions options;
        options.type = TransfuncTypes::HISTOGRAM;
        options.intensityLeftBound = ui->histogramColorWidget->histogramWidget->leftBound;
        options.intensityRightBound = ui->histogramColorWidget->histogramWidget->rightBound;
        applyHistogram(options, currentParentNode, currentApplyNode, currentOtherNode, md->globalTexture3DIntensityData, QCallBack);
        break;
    }

    case TransfuncTypes::PROBABILITY_MODEL:{
        QComboBox *organTypeCB = ui->organTypeCombobox;
        NNClassTypes type = NNClassTypes::ORTHERS;
        QString typeString = organTypeCB->currentText();

        if (typeString == "Artery") type = NNClassTypes::ARTERY;
        if (typeString == "Bone") type = NNClassTypes::BONE;
        if (typeString == "Kidney") type = NNClassTypes::KIDNEY;
        if (typeString == "Liver") type = NNClassTypes::LIVER;
        if (typeString == "Lung") type = NNClassTypes::LUNG;
        if (typeString == "Portalvein") type = NNClassTypes::PORTALVEIN;
        if (typeString == "Venous System") type = NNClassTypes::VENOUSSYSTEM;

        int index = 0;
        int count = 100;
        int step = currentApplyNode->maxIndex / count;
        count = currentApplyNode->maxIndex / step;

        for (int i = 0; i <= currentApplyNode->maxIndex; ++i) {
            int intensity = md->globalTexture3DIntensityData[i] - 1024;

            if(nnpredict.predict(intensity, type)){
                currentApplyNode->opacity[i] = 1 * currentParentNode->opacity[i];
                currentOtherNode->opacity[i] = 0;
            } else {
                currentApplyNode->opacity[i] = 0;
                currentOtherNode->opacity[i] = 1 * currentParentNode->opacity[i];
            }
            if (i % step == 0) {
                QCallBack(100 * index * 1.0/ count, "Building child node opcaity data");
                index++;
            }
        }
        currentParentNode->transfuncOptions.type = TransfuncTypes::PROBABILITY_MODEL;

        currentApplyNode->transfunc = tfprobabilitymodel;
        TransfuncOptions options;
        options.type = TransfuncTypes::NONE;
        NodeViewOptions viewOptions;
        viewOptions.colorTable = currentApplyNode->colorTable;
        viewOptions.opacity = currentApplyNode->opacity;
        viewOptions.class_type = type;
        currentApplyNode->transfuncOptions = options;
        currentApplyNode->viewOptions = viewOptions;

        TransfuncOptions optionsInverted;
        optionsInverted.type = TransfuncTypes::PROBABILITY_MODEL;
        NodeViewOptions viewOptionInverted;
        viewOptionInverted.colorTable = currentOtherNode->colorTable;
        viewOptionInverted.opacity = currentOtherNode->opacity;
        viewOptionInverted.class_type = type;
        currentOtherNode->transfunc = tfprobabilitymodelinverted;
        currentOtherNode->transfuncOptions = optionsInverted;
        currentOtherNode->viewOptions = viewOptionInverted;

        currentApplyNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
        currentOtherNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
        break;
    }
    case TransfuncTypes::MEDIAN_FILTER: {
        signed short* newIntensity = medianFilter();
//        delete newIntensity;

        TransfuncOptions options;
        options.type = TransfuncTypes::MEDIAN_FILTER;
        NodeViewOptions viewOptions;
        viewOptions.opacity = currentApplyNode->opacity;
        viewOptions.colorTable = currentApplyNode->colorTable;

        currentApplyNode->transfunc = tfMedianFilter;
        currentApplyNode->transfuncOptions = options;

        TransfuncOptions optionsInverted;
        optionsInverted.type = TransfuncTypes::MEDIAN_FILTER;
        NodeViewOptions viewOptionsInverted;
        viewOptionsInverted.opacity = currentOtherNode->opacity;
        viewOptionsInverted.colorTable = currentOtherNode->colorTable;

        currentOtherNode->transfunc = tfMedianFilterInverted;
        currentOtherNode->transfuncOptions = optionsInverted;

        currentApplyNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
        currentOtherNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
        break;
    }
    default:
        break;
    }
    ui->applyTFButton->setVisible(false);
    emit endTransferFunc(currentTransFuncType);
    // Reset all nodes
    currentTransFuncType = TransfuncTypes::NONE;
    currentApplyNode = NULL;
    currentOtherNode = NULL;
    currentParentNode = NULL;
}

void VolumePropertyWidget::startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *applyNode, TreeItem *otherNode)
{
    if (type == TransfuncTypes::HISTOGRAM) {
        currentApplyNode = applyNode;
        currentOtherNode = otherNode;
        currentParentNode = parentNode;
        ui->applyTFButton->setText("Apply Histogram TF");
        currentTransFuncType = type;
        // Show slider
        ui->histogramColorWidget->setBoundingSliderVisibility(true);
//        startHistogram();
    }

    if (type == TransfuncTypes::PROBABILITY_MODEL) {
        currentApplyNode = applyNode;
        currentOtherNode = otherNode;
        currentParentNode = parentNode;
        ui->applyTFButton->setText("Apply Probability TF");
        currentTransFuncType = type;
    }

    if (type == TransfuncTypes::MEDIAN_FILTER) {
        currentApplyNode = applyNode;
        currentOtherNode = otherNode;
        currentParentNode = parentNode;
        ui->applyTFButton->setText("Apply Median Filter");
        currentTransFuncType = type;
    }
    ui->applyTFButton->setVisible(true);
}

void VolumePropertyWidget::selectedNodeChanged(TreeItem *selectedItem)
{
    // Update general information: colortable & isUsetable checkbox
    if (selectedItem == NULL) {
        currentSelectedNode = NULL;
        ui->histogramColorWidget->setColorTable(NULL);
        ui->histogramColorWidget->histogramWidget->setHistogram(NULL);
        ui->histogramColorWidget->setIsUseColorTable(false);
        ui->applyButton->setEnabled(false);
        return;
    }
    currentSelectedNode = selectedItem;
    ui->histogramColorWidget->setColorTable(currentSelectedNode->colorTable);
    ui->histogramColorWidget->histogramWidget->setHistogram(currentSelectedNode->histogram);
    ui->histogramColorWidget->setIsUseColorTable(currentSelectedNode->viewOptions.isUseColorTable);
    ui->applyButton->setEnabled(false);
    // Check if this node has dirty children and enable apply button
    if (!currentSelectedNode->isDirty && currentSelectedNode->isHasDirtyChildren()){
        ui->applyButton->setEnabled(true);
    } else {
        ui->applyButton->setEnabled(false);
    }
    //
    ui->histogramColorWidget->resetBoundingSlider();
    ui->histogramColorWidget->setBoundingSliderVisibility(false);
    TransfuncOptions options = currentSelectedNode->transfuncOptions;
    switch (currentSelectedNode->transfuncOptions.type) {
        case TransfuncTypes::HISTOGRAM: {
            // Firstly reset flag of slider changed;
            ui->histogramColorWidget->isSliderChanged = false;

            QSlider *leftSlider = ui->histogramColorWidget->leftSlider;
            QSlider *rightSlider = ui->histogramColorWidget->rightSlider;
            ui->histogramColorWidget->setBoundingSliderVisibility(true);
            int grayMax = ui->histogramColorWidget->histogramWidget->grayMax;
            int grayMin = ui->histogramColorWidget->histogramWidget->grayMin;
            int range = grayMax - grayMin + 1;
            int leftStop = (options.intensityLeftBound + 1024) * leftSlider->maximum() / range; // TODO hardcode
            int rightStop = (options.intensityRightBound + 1024) * rightSlider->maximum() / range;
            leftSlider->setValue(leftStop);
            rightSlider->setValue(rightStop);
            break;
        }
        case TransfuncTypes::PROBABILITY_MODEL: {
            break;
        }
    }
}

void VolumePropertyWidget::colorTableChanged()
{
    ui->applyButton->setEnabled(true);
}

void VolumePropertyWidget::reloadHistogram()
{
    if (currentSelectedNode) {
        currentSelectedNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
        ui->histogramColorWidget->histogramWidget->setHistogram(currentSelectedNode->histogram);
        ui->histogramColorWidget->histogramWidget->update();
    }
}

signed short* VolumePropertyWidget::medianFilter() {
    if (currentSelectedNode == NULL) return NULL;
    bool *opacityMask = currentParentNode->opacity;
    if (md  == NULL) return NULL;
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int maxIndex = wp * hp * n;

    signed short *intensityOrigin = md->globalTexture3DIntensityData;
//    signed short *intensityOutput = new signed short(maxIndex);

    int index = 0;
    int count = 100;
    int step = maxIndex / count;
    count = maxIndex / step;

    for (int i = 0; i < maxIndex; ++i) {
        QList<int> sortList;

        sortList.push_back(intensityOrigin[i]);

        // Find 26 neighboors
        QList<int> neighboors = getNeighbors(i);
        foreach(int index, neighboors) {
            int intensity = intensityOrigin[index];
            if (opacityMask[index] == 1){
                sortList.push_back(intensity);
            } else {
                sortList.push_back(-2000);
            }
        }

        qSort(sortList);

        // Get middle intensity
        int middleIntensity = sortList.at(sortList.size() / 2);
//        intensityOutput[i] = middleIntensity; // Size of sortList always > 1 because of current process index
        this->currentApplyNode->opacity[i] = opacityMask[i] * (middleIntensity != -2000);
        this->currentOtherNode->opacity[i] = opacityMask[i] * ((this->currentApplyNode->opacity[i] + 1) % 2); // Inverted

        if (i % step == 0) {
            QCallBack(100 * index * 1.0/ count, "Building child node opcaity data");
            index++;
        }
    }
    return /*intensityOutput*/ NULL;
}

QList<int> VolumePropertyWidget::getNeighbors(int centerIndex) {
    int wp = md->measurementPixel.X();
    int hp = md->measurementPixel.Y();
    int n = md->measurementPixel.Z();
    int maxIndex = wp * hp * n - 1;
    QList<int> neighborIndexes;
    int index;
    if (centerIndex >= 0 && centerIndex <= maxIndex){
        // Middle Slice
        index = centerIndex - wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex - wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex - wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);

        index = centerIndex + wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex + wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = centerIndex + wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
    }

    // Upper Slice
    int upperIndex = centerIndex - wp * hp;
    if (upperIndex >= 0 && upperIndex <= maxIndex) {
        index = upperIndex - wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = upperIndex - wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = upperIndex - wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);

        index = upperIndex - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = upperIndex;
        neighborIndexes.push_back(index);
        index = upperIndex + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);

        index = upperIndex + wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = upperIndex + wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = upperIndex + wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
    }

    // Below slice
    int belowIndex = centerIndex + wp * hp;
    if (belowIndex >= 0 && belowIndex <= maxIndex) {
        index = belowIndex - wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = belowIndex - wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = belowIndex - wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);

        index = belowIndex - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = belowIndex;
        neighborIndexes.push_back(index);
        index = belowIndex + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);

        index = belowIndex + wp - 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = belowIndex + wp;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
        index = belowIndex + wp + 1;
        if (index >= 0 && index <= maxIndex) neighborIndexes.push_back(index);
    }

    return neighborIndexes;
}

vcg::Color4f tfhistogram(signed short* intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (options.opacity[index] == 1) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

vcg::Color4f tfhistograminverted(signed short* intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;


    if (options.opacity[index]) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

// TRansfer function for probability model
vcg::Color4f tfprobabilitymodel(signed short* intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (nnpredict.predict(intensity, options.class_type) && options.opacity[index] == 1) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

vcg::Color4f tfprobabilitymodelinverted(signed short* intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (!nnpredict.predict(intensity, options.class_type) && options.opacity[index] == 1) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

vcg::Color4f tfMedianFilter(signed short* intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (options.opacity[index] == 1) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

vcg::Color4f tfMedianFilterInverted(signed short* intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (options.opacity[index] == 1) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

void VolumePropertyWidget::on_applyButton_clicked()
{
    emit applyButtonClicked();
    //    emit colorTableChanged(QCallBack, false);
    if (currentSelectedNode == NULL) return;
    currentSelectedNode->colorTable->updateColorTable();
    currentSelectedNode->viewOptions.isUseColorTable = ui->histogramColorWidget->isUseColorTable;
    if (currentSelectedNode){
        switch(currentSelectedNode->transfuncOptions.type) {
        case TransfuncTypes::HISTOGRAM:{
            if (ui->histogramColorWidget->isSliderChanged) {
                TreeItem* otherNode = currentSelectedNode->child(1);
                TreeItem* applyNode = currentSelectedNode->child(0); // TODO hardcode child number
                currentSelectedNode->transfuncOptions.intensityLeftBound = ui->histogramColorWidget->histogramWidget->leftBound;
                currentSelectedNode->transfuncOptions.intensityRightBound = ui->histogramColorWidget->histogramWidget->rightBound;
                applyHistogram(currentSelectedNode->transfuncOptions, currentSelectedNode, applyNode, otherNode, md->globalTexture3DIntensityData, QCallBack);
                otherNode->setDirtyChildren(otherNode);
                applyNode->setDirtyChildren(applyNode);
            }
            break;
        }
        }
    }
    ui->applyButton->setEnabled(false);
}

void VolumePropertyWidget::enableApplyButton()
{
    ui->applyButton->setEnabled(true);
}
