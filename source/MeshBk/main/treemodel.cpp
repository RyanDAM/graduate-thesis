//#include "treeitem.h"
#include "common/meshmodel.h"
#include "treemodel.h"

TreeModel::TreeModel(QObject *parent) : QAbstractItemModel(parent)
{
    QVector<QVariant> rootData;
    rootData << "Organ" << "Transfer Function";
    rootItem = new TreeItem(rootData);

//    QVector<QVariant> patientData;
//    patientData << "Patient" << "Transform func1";
//    TreeItem* patient = new TreeItem(patientData, rootItem);
//    rootItem->appendChild(patient);

//    QVector<QVariant> liverData;
//    liverData << "Liver" << "None";
//    TreeItem* liver = new TreeItem(liverData, patient);
////    liver->setTransFunc(testFunc);
////    TransfuncOptions options;
////    options.type = TransfuncTypes::HISTOGRAM;
////    liver->setTransFuncOptions(options);
//    patient->appendChild(liver);

//    QVector<QVariant> other1Data;
//    other1Data << "Others" << "Transform func2";
//    TreeItem* other1 = new TreeItem(other1Data, patient);
//    patient->appendChild(other1);

//    QVector<QVariant> boneData;
//    boneData << "Bone" << "None";
//    TreeItem* bone = new TreeItem(boneData, other1);
//    other1->appendChild(bone);

//    QVector<QVariant> other2Data;
//    other2Data << "Others" << "None";
//    TreeItem* other2 = new TreeItem(other2Data, other1);
//    other1->appendChild(other2);
}

TreeModel::~TreeModel()
{
    delete rootItem;
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();
    TreeItem *item = getItem(index);
    return item->data(index.column());
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;
    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);
    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
   if (parent.isValid() && parent.column() != 0)
       return QModelIndex();
   TreeItem *parentItem = getItem(parent);
   TreeItem *childItem = parentItem->child(row);
   if (childItem)
       return createIndex(row, column, childItem);
   else
       return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();
    TreeItem *childItem = getItem(index);
    TreeItem *parentItem = childItem->parent();
    if (parentItem == rootItem)
        return QModelIndex();
    return createIndex(parentItem->childNumber(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem = getItem(parent);
    return parentItem->childCount();
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

bool TreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole)
        return false;
    TreeItem *item = getItem(index);
    bool result = item->setData(index.column(), value);
    if (result)
        emit dataChanged(index, index);

    return result;
}

TreeItem *TreeModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
        if (item) return item;
    }
    return rootItem;
}

bool TreeModel::addChild(const QModelIndex index, TreeItem *item)
{
    TreeItem* parent = getItem(index);
    int row = index.row();
    int column = index.column();
    parent->appendChild(item);
    emit dataChanged(this->index(row, column), this->index(row + 1, column));
    return true;
}

void TreeModel::setupModelData(const QStringList &lines, TreeItem *parent)
{
    QList<TreeItem*> parents;
    QList<int> indentations;
    parents << parent;
    indentations << 0;

    int number = 0;
    while (number < lines.count()) {
        int position = 0;
        while (position < lines[number].length()) {
            if (lines[number].mid(position, 1) != "")
                break;
            position++;
        }
        QString lineData = lines[number].mid(position).trimmed();
        if (!lineData.isEmpty()) {
            // Read the column data from the rest of the line.
            QStringList columnsStrings = lineData.split("\t", QString::SkipEmptyParts);
            QVector<QVariant> columnData;
            for (int column = 0; column < columnsStrings.count(); ++column) {
                columnData << columnsStrings[column];
            }
            if (position > indentations.last()) {
                // The last child of the current parent is now the new parent
                // unless the current parent has no children
                if (parents.last()->childCount() == 0) {
                    parents << parents.last()->child(parents.last()->childCount() - 1);
                    indentations << position;
                }
            } else {
                while (position < indentations.last() && parents.count() > 0) {
                    parents.pop_back();
                    indentations.pop_back();
                }
            }
            // Append a new item to the current parent's list of children
            parents.last()->appendChild(new TreeItem(columnData, parents.last()));
        }
        number++;
    }
}

bool TreeModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    TreeItem *parentItem = getItem(parent);
    bool success;
    beginInsertRows(parent, position, position + rows - 1);
    success = parentItem->insertChildren(position, rows, rootItem->columnCount());
    endInsertRows();
    return success;
}

bool TreeModel::insertColumns(int position, int columns, const QModelIndex &parent)
{
    bool success;
    beginInsertColumns(parent, position, position + columns - 1);
    success = rootItem->insertColumns(position, columns);
    endInsertColumns();
    return success;
}

bool TreeModel::removeRows(int position, int rows, const QModelIndex &parent)
{
    TreeItem *parentItem = getItem(parent);
    bool success = true;
    beginRemoveRows(parent, position, position + rows - 1);
    success = parentItem->removeChildren(position, rows);
    endRemoveRows();
    return success;
}

bool TreeModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (role != Qt::EditRole || orientation != Qt::Horizontal)
        return false;
    bool result = rootItem->setData(section, value);
    if (result)
        emit headerDataChanged(orientation, section, section);
    return result;
}

vcg::Color4f testFunc(int index, TransfuncOptions options)
{
    // TODO remove this test function
    return vcg::Color4f(255, 0, 0, 100);
}
