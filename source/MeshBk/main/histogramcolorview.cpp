#include "histogramcolorview.h"

HistogramColorView::HistogramColorView(QWidget* parent): QGLWidget(parent) {
    this->backgroundColor = QColor(0, 200, 0);

    nameMapper = new QSignalMapper();
    colorMapper = new QSignalMapper();
    opacityMapper = new QSignalMapper();
    removeMapper = new QSignalMapper();

    this->setFixedHeight(200);
}

void HistogramColorView::initializeGL() {
    setCameraView();
}

void HistogramColorView::setCameraView() {
    qglClearColor(this->backgroundColor);

    GLdouble left = orthoLeft;
    GLdouble right = orthoRight;
    GLdouble bottom = orthoBottom;
    GLdouble top = orthoTop;
    GLdouble near = 5.0;
    GLdouble far = 15.0;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(left, right, bottom, top, near, far);

    GLdouble eyeX = 0.0;
    GLdouble eyeY = 0.0;
    GLdouble eyeZ = 10.0;
    GLdouble centerX = 0.0;
    GLdouble centerY = 0.0;
    GLdouble centerZ = 0.0;
    GLdouble upX = 0.0;
    GLdouble upY = 1.0;
    GLdouble upZ = 0.0;

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyeX, eyeY, eyeZ,
              centerX, centerY, centerZ,
              upX, upY, upZ);
}

void HistogramColorView::updateGLForcefully() {

    QPaintEvent dummy(this->rect());
    paintEvent(&dummy);
}

void HistogramColorView::paintEvent(QPaintEvent* event) {
    QPainter painter(this);
    painter.beginNativePainting();

    setCameraView();

    glClearColor(0.7, 0.7, 0.7, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3f(1.0f, 1.0f, 1.0f);

    if (texture2DLogAddr) {
        glBindTexture(GL_TEXTURE_2D, texture2DLogAddr);

        glPushAttrib(GL_ALL_ATTRIB_BITS);
        glPushMatrix();

        glEnable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
            glNormal3f(0.0f, 0.0f, 1.0f);
            glTexCoord2i(0, 0);
            glVertex3f(minAxisX, minAxisY, 0.0f);

            glTexCoord2i(1, 0);
            glVertex3f(maxAxisX, minAxisY, 0.0f);

            glTexCoord2i(1, 1);
            glVertex3f(maxAxisX, maxAxisY, 0.0f);

            glTexCoord2i(0, 1);
            glVertex3f(minAxisX, maxAxisY, 0.0f);
        glEnd();

        glBindTexture(GL_TEXTURE_2D, 0);
        glPopMatrix();
        glPopAttrib();
    } // end if (texture)

    //Draw Axis Ox, Oy
    glColor3f(1.0f, 0.0f, 0.0f);
    glLineWidth(2.0f);
    glBegin(GL_LINES);
        glVertex3f(minAxisX, minAxisY, 0.0f);
        glVertex3f(maxAxisX + 20, minAxisY, 0.0f);

        glVertex3f(minAxisX, minAxisY, 0.0f);
        glVertex3f(minAxisX, maxAxisY + 20, 0.0f);
    glEnd(); // End draw Axis

    // Draw Ox Axis points (intensity): -1000 -800 -600 -400 -200 0 200 400 600 800 1000
    glPointSize(4);
    glColor3f(1.0f, 1.0f, 0.0f);
    glBegin(GL_POINTS);
        glVertex3f(0.0f, 0.0f, 0.0f);
    glEnd();

    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_POINTS);
        glVertex3f(-1000.0f, 0.0f, 0.0f);
        glVertex3f(-800.0f, 0.0f, 0.0f);
        glVertex3f(-600.0f, 0.0f, 0.0f);
        glVertex3f(-400.0f, 0.0f, 0.0f);
        glVertex3f(-200.0f, 0.0f, 0.0f);
        glVertex3f(200.0f, 0.0f, 0.0f);
        glVertex3f(400.0f, 0.0f, 0.0f);
        glVertex3f(600.0f, 0.0f, 0.0f);
        glVertex3f(800.0f, 0.0f, 0.0f);
        glVertex3f(1000.0f, 0.0f, 0.0f);
    glEnd();
    // End draw Ox Axis points

    // Draw Oy Axis points (gradient): 0 200 400 600 800 100
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_POINTS);
        glVertex3f(-1024.0f, 200.0f, 0.0f);
        glVertex3f(-1024.0f, 400.0f, 0.0f);
        glVertex3f(-1024.0f, 600.0f, 0.0f);
        glVertex3f(-1024.0f, 800.0f, 0.0f);
        glVertex3f(-1024.0f, 1000.0f, 0.0f);
    glEnd();
    // End draw Oy Axis points

    // Draw rect is painting
    if(isMouseMoving) {
        drawDashLineRectangles(modelViewPoint1, modelViewPoint2, rectColor);
    }

    // Draw all region that user painted
    for (int i = 0; i < histRegionRectList.size(); i++) {
        HistogramRegionRect regionRect = histRegionRectList.at(i);
        drawDashLineRectangles(regionRect.point1, regionRect.point2, regionRect.color);
    } // end for

    painter.endNativePainting();
}

void HistogramColorView::resizeGL(int width, int height) {
    glViewport(0, 0, width, height);
}

void HistogramColorView::initTexture2D(int width, int height) {

    // Initialize for log of histogram Intensity-GradientManigtude
    glGenTextures(1, &texture2DLogAddr);

    glBindTexture(GL_TEXTURE_2D, texture2DLogAddr);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    texture2DLogData = new uchar[width*height*3]; // 3 chanels RGB
    for (int i = 0; i > width*height*3; i++) {
        texture2DLogData[i] = 200;
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture2DLogData);

    glBindTexture(GL_TEXTURE_2D, 0);
    //End Initialize for log of histogram Intensity-GradientManigtude
}

void HistogramColorView::updateHistogramMatAndTexture(cv::Mat mat) {
    this->histogramMat = mat;

    int width = mat.cols;
    int height = mat.rows;

    if (!texture2DAddr) {
        initTexture2D(width, height);
    }

    for (int i = 0; i < 2; i++) {
        subTexture2D(mat);

        updateGLForcefully();
    }

}

void HistogramColorView::subTexture2D(cv::Mat mat) {

    // Log of histogram Intensity-GradientManigtude substution
    int cols = mat.cols;
    int rows = mat.rows;

    cv::Mat logMat; cv::log(mat + 1, logMat);
    double minLog, maxLog;

    cv::minMaxLoc(logMat, &minLog, &maxLog);
    maxLog = maxLog - minLog;

    for (int y = 0; y < rows; y++) {
        for (int x = 0; x < cols; x++) {
            uchar value = (uchar) ((logMat.at<double>(y, x) - minLog) / maxLog * 255.0);
            texture2DLogData[3*(y*cols + x)] = value;
            texture2DLogData[3*(y*cols + x) + 1] = value;
            texture2DLogData[3*(y*cols + x) + 2] = value;
        }
    } // end for (y x)

    glBindTexture(GL_TEXTURE_2D, texture2DLogAddr);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, cols, rows, 0, GL_RGB, GL_UNSIGNED_BYTE, texture2DLogData);

    glBindTexture(GL_TEXTURE_2D, 0);
    // End Log of histogram Intensity-GradientManigtude substution
}

void HistogramColorView::mousePressEvent(QMouseEvent *event) {

    point1.setX((float) event->x());
    point1.setY((float) this->height() - event->y());
}

void HistogramColorView::mouseReleaseEvent(QMouseEvent *event) {    
    point2.setX((float) event->x());
    point2.setY((float) this->height() - event->y());

    if (point1.x() == point2.x() && point1.y() == point2.y()) // just click, this case is ignored
        return;

    changeCoordIntoModelView();

    rectColor = QColorDialog::getColor();

    addToHistRegionRectList();

    isMouseMoving = false;
    updateGLForcefully();
}

void HistogramColorView::addToHistRegionRectList() {
    HistogramRegionRect region;
    region.name = "Unname";
    region.point1 = modelViewPoint1;
    region.point2 = modelViewPoint2;
    region.color = rectColor;

    histRegionRectList.push_back(region);

    updateRegionColorTable();

    updateGLForcefully();
}

void HistogramColorView::updateRegionColorTable() {
    if(!regionColorTable)
        return;

    delete nameMapper;
    delete colorMapper;
    delete opacityMapper;
    delete removeMapper;
    nameMapper = new QSignalMapper();
    colorMapper = new QSignalMapper();
    opacityMapper = new QSignalMapper();
    removeMapper = new QSignalMapper();

    regionColorTable->setRowCount(histRegionRectList.size());

    for (int idx = 0; idx < histRegionRectList.size(); idx++) {
        HistogramRegionRect region = histRegionRectList.at(idx);

        QTextEdit* name = new QTextEdit();
        name->setText(region.name);
        regionColorTable->setCellWidget(idx, 0, name);
        connect(name, SIGNAL(textChanged()), nameMapper, SLOT(map()));
        nameMapper->setMapping(name, idx);

        QPushButton* color = new QPushButton();
        color->setStyleSheet("background-color: rgb(" + QString::number(region.color.red()) + "," + QString::number(region.color.green()) + "," + QString::number(region.color.blue()) + ")");
        regionColorTable->setCellWidget(idx, 1, color);
        connect(color, SIGNAL(clicked(bool)), colorMapper, SLOT(map()));
        colorMapper->setMapping(color, idx);

        QDoubleSpinBox* opacity = new QDoubleSpinBox();
        opacity->setMaximum(255);
        opacity->setMinimum(0);
        opacity->setSingleStep(1);
        opacity->setValue(region.color.alpha());
        regionColorTable->setCellWidget(idx, 2, opacity);
        connect(opacity, SIGNAL(valueChanged(double)), opacityMapper, SLOT(map()));
        opacityMapper->setMapping(opacity, idx);

        QPushButton *removeButton = new QPushButton();
        removeButton->setIcon(QIcon(":/images/delete.png"));
        removeButton->setIconSize(QSize(24, 24));
        regionColorTable->setCellWidget(idx, 3, removeButton);
        connect(removeButton, SIGNAL(clicked(bool)), removeMapper, SLOT(map()));
        removeMapper->setMapping(removeButton, idx);

    } // end for

    connect(nameMapper, SIGNAL(mapped(int)), this, SLOT(changeRegionName(int)));
    connect(colorMapper, SIGNAL(mapped(int)), this, SLOT(changeRegionColor(int)));
    connect(opacityMapper, SIGNAL(mapped(int)), this, SLOT(changeRegionOpacity(int)));
    connect(removeMapper, SIGNAL(mapped(int)), this, SLOT(removeRegion(int)));
}

void HistogramColorView::changeRegionName(int index) {
    QTextEdit* textEdit = qobject_cast<QTextEdit*>(regionColorTable->cellWidget(index, 0));

    histRegionRectList[index].name = textEdit->toPlainText();
}

void HistogramColorView::changeRegionColor(int index) {
    QColor newColor = QColorDialog::getColor();

    QPushButton* colorButton = qobject_cast<QPushButton*>(regionColorTable->cellWidget(index, 1));
    colorButton->setStyleSheet("background-color: rgb(" + QString::number(newColor.red()) + "," + QString::number(newColor.green()) + "," + QString::number(newColor.blue()) + ")");

    histRegionRectList[index].color.setRed(newColor.red());
    histRegionRectList[index].color.setGreen(newColor.green());
    histRegionRectList[index].color.setBlue(newColor.blue());

    updateGLForcefully();
}

void HistogramColorView::changeRegionOpacity(int index) {
    QDoubleSpinBox* opacity = qobject_cast<QDoubleSpinBox*>(regionColorTable->cellWidget(index, 2));

    histRegionRectList[index].color.setAlpha((int)opacity->value());
}

void HistogramColorView::removeRegion(int index) {

    nameMapper->removeMappings((QTextEdit*)regionColorTable->cellWidget(index, 0));
    colorMapper->removeMappings((QPushButton*)regionColorTable->cellWidget(index, 1));
    opacityMapper->removeMappings((QDoubleSpinBox*)regionColorTable->cellWidget(index, 2));
    removeMapper->removeMappings((QPushButton*)regionColorTable->cellWidget(index, 3));

    regionColorTable->removeRow(index);
    histRegionRectList.removeAt(index);

    for (int i = 0; i < regionColorTable->rowCount(); i++) {
        nameMapper->setMapping((QTextEdit*)regionColorTable->cellWidget(index, 0), index);
        colorMapper->setMapping((QPushButton*)regionColorTable->cellWidget(index, 1), index);
        opacityMapper->setMapping((QDoubleSpinBox*)regionColorTable->cellWidget(index, 2), index);
        removeMapper->setMapping((QPushButton*)regionColorTable->cellWidget(index, 3), index);
    }

    updateGLForcefully();
}

void HistogramColorView::mouseMoveEvent(QMouseEvent *event) {
    isMouseMoving = true;

    point2.setX((float) event->x());
    point2.setY((float) this->height() - event->y());

    changeCoordIntoModelView();

    rectColor = QColor(255, 0, 0);

    updateGLForcefully();
}

void HistogramColorView::changeCoordIntoModelView() {

    // Ox point
    modelViewPoint1.setX(((point1.x()*modelViewWidth) / this->width()) + orthoLeft);
    modelViewPoint2.setX(((point2.x()*modelViewWidth) / this->width()) + orthoLeft);

    //Oy point
    modelViewPoint1.setY(((point1.y()*modelViewHeight) / this->height()) + orthoBottom);
    modelViewPoint2.setY(((point2.y()*modelViewHeight) / this->height()) + orthoBottom);
}

HistogramColorView::~HistogramColorView() {

}

void HistogramColorView::drawRectangles(QPointF p1, QPointF p2, QColor color) {
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPushMatrix();

    glColor3f(color.redF(), color.greenF(), color.blueF());
    glBegin(GL_QUADS);
        glVertex3f(p1.x(), p1.y(), 0.0f);
        glVertex3f(p1.x(), p2.y(), 0.0f);
        glVertex3f(p2.x(), p2.y(), 0.0f);
        glVertex3f(p2.x(), p1.y(), 0.0f);
    glEnd();

    glPopMatrix();
    glPopAttrib();
}

void HistogramColorView::drawDashLineRectangles(QPointF p1, QPointF p2, QColor color) {
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPushMatrix();

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glLineStipple(5, 0xAAAA);
    glEnable(GL_LINE_STIPPLE);

    glColor3f(color.redF(), color.greenF(), color.blueF());
    glBegin(GL_QUADS);
        glVertex3f(p1.x(), p1.y(), 0.0f);
        glVertex3f(p1.x(), p2.y(), 0.0f);
        glVertex3f(p2.x(), p2.y(), 0.0f);
        glVertex3f(p2.x(), p1.y(), 0.0f);
    glEnd();

    glPopMatrix();
    glPopAttrib();
}
