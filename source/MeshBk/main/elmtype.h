#ifndef ELMTYPE_H
#define ELMTYPE_H

#include <armadillo>
using namespace arma;

enum ActivationFunction {
    NONE = 0,
    SIGMOID
};

enum NetworkType {
    REGRESSION = 0,
    CLASSIFICATION
};

struct ELMData {
    public:
    mat *data;
    mat *target;

    void print() {
        (*data).print("Data:");
        (*target).print("Target:");
    }
};

#endif // ELMTYPE_H
