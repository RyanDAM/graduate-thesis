#include "featureextraction.h"
#include <math.h>
#include "fftfilter.h"

#define PI 3.1415

void imgABS(cv::Mat &fMat, cv::Mat &sMat, cv::Mat &retMat) {
    cv::Mat part1, part2;
    cv::pow(fMat, 2, part1);
    cv::pow(sMat, 2, part2);
    cv::Mat sum = part1 + part2;
    cv::sqrt(sum, retMat);
}

void meanSTD(cv::Mat &input, cv::Mat &meanOut, cv::Mat &varianceOut, cv::Size size) {
    cv::Mat fImage;
    input.convertTo(fImage, CV_64F);

    cv::blur(fImage, meanOut, size);

    cv::Mat mu2;
    cv::blur(fImage.mul(fImage), mu2, size);

    cv::sqrt(mu2 - meanOut.mul(meanOut), varianceOut);
}

cv::Mat getFeatureVector(int row, int col, QList<cv::Mat> &dataMats) {
    int featureLength = dataMats.size();
    cv::Mat ret(1, featureLength, CV_64F);

    for (int i = 0; i < featureLength; i++) {
        cv::Mat data = dataMats.at(i);
        ret.at<double>(0, i) = data.at<double>(row, col);
    }
    return ret;
}

QList<cv::Mat> featureForMat(cv::Mat &dicom) {

    QList<cv::Mat> ret;

    // Setup for gabor kernel

    cv::Mat src_f; //source file
    dicom.convertTo(src_f, CV_64F);

    int kernel_size = 20;
    double gm = 1, ps = 0;

    for (double th = 0; th < 180; th += 15) {
        for (double lm = 10; lm < 14; lm += 5) {
            for (double sig = 2; sig < 2.4; sig += 1) {

//    int kernel_size = 11;
//    double gm = 1, ps = 0;

//    for (double th = 0; th < 180; th += 15) {
//        for (double lm = 8; lm < 10; lm += 5) {
//            for (double sig = 1.5; sig < 2.5; sig += 1) {
                // build gabor filter
                cv::Mat kernelReal = getGaborRealKernel(cv::Size(kernel_size,kernel_size), sig, th, lm, gm, ps, CV_64F);
                cv::Mat kernelImagine = getGaborImagineKernel(cv::Size(kernel_size,kernel_size), sig, th, lm, gm, ps, CV_64F);

                cv::Mat real;
                cv::filter2D(src_f, real, src_f.depth(), kernelReal);

                cv::Mat imagine;
                cv::filter2D(src_f, imagine, src_f.depth(), kernelImagine);

                cv::Mat absMat;
                imgABS(real, imagine, absMat);
                //cv::normalize(absMat, absMat, 0, 255, cv::NORM_MINMAX);

                // calculate mean, variance of filtered image
                cv::Mat mean, variance;
                meanSTD(absMat, mean, variance, cv::Size(3, 3));

                // add to result
                ret.append(mean);
                ret.append(variance);
            }
        }
    }
    // add original intensive matrix
    ret.append(src_f);

    return ret;
}
