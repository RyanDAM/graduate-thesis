/********************************************************************************
** Form generated from reading UI file 'opacityfrommaskwidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPACITYFROMMASKWIDGET_H
#define UI_OPACITYFROMMASKWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OpacityFromMaskWidget
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *importMaskButton;
    QLineEdit *importMaskEditText;
    QSpacerItem *verticalSpacer;
    QPushButton *applyButton;
    QProgressBar *progressBar;

    void setupUi(QWidget *OpacityFromMaskWidget)
    {
        if (OpacityFromMaskWidget->objectName().isEmpty())
            OpacityFromMaskWidget->setObjectName(QString::fromUtf8("OpacityFromMaskWidget"));
        OpacityFromMaskWidget->resize(400, 300);
        verticalLayout = new QVBoxLayout(OpacityFromMaskWidget);
        verticalLayout->setSpacing(5);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        importMaskButton = new QPushButton(OpacityFromMaskWidget);
        importMaskButton->setObjectName(QString::fromUtf8("importMaskButton"));

        horizontalLayout->addWidget(importMaskButton);

        importMaskEditText = new QLineEdit(OpacityFromMaskWidget);
        importMaskEditText->setObjectName(QString::fromUtf8("importMaskEditText"));

        horizontalLayout->addWidget(importMaskEditText);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        applyButton = new QPushButton(OpacityFromMaskWidget);
        applyButton->setObjectName(QString::fromUtf8("applyButton"));

        verticalLayout->addWidget(applyButton);

        progressBar = new QProgressBar(OpacityFromMaskWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);
        progressBar->setTextVisible(false);

        verticalLayout->addWidget(progressBar);


        retranslateUi(OpacityFromMaskWidget);

        QMetaObject::connectSlotsByName(OpacityFromMaskWidget);
    } // setupUi

    void retranslateUi(QWidget *OpacityFromMaskWidget)
    {
        OpacityFromMaskWidget->setWindowTitle(QApplication::translate("OpacityFromMaskWidget", "Form", 0, QApplication::UnicodeUTF8));
        importMaskButton->setText(QApplication::translate("OpacityFromMaskWidget", "Import Mask", 0, QApplication::UnicodeUTF8));
        applyButton->setText(QApplication::translate("OpacityFromMaskWidget", "Apply TF", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OpacityFromMaskWidget: public Ui_OpacityFromMaskWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPACITYFROMMASKWIDGET_H
