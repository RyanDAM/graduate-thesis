/****************************************************************************
** Meta object code from reading C++ file 'spaceselectionwidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "spaceselectionwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'spaceselectionwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SpaceSelectionWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x05,

 // slots: signature, parameters, type, tag, flags
      68,   40,   21,   21, 0x0a,
     145,  132,   21,   21, 0x0a,
     176,   21,   21,   21, 0x0a,
     203,   21,   21,   21, 0x0a,
     232,   21,   21,   21, 0x08,
     261,   21,   21,   21, 0x08,
     298,  292,   21,   21, 0x08,
     337,   21,   21,   21, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SpaceSelectionWidget[] = {
    "SpaceSelectionWidget\0\0endTransferFunc()\0"
    "type,parentNode,organ,other\0"
    "startTransferFunc(TransfuncTypes,TreeItem*,TreeItem*,TreeItem*)\0"
    "selectedItem\0selectedNodeChanged(TreeItem*)\0"
    "changeBoundingCubeParams()\0"
    "changeBoundingSphereParams()\0"
    "on_cubeColorButton_clicked()\0"
    "on_sphereColorButton_clicked()\0index\0"
    "on_boundRegionTabs_currentChanged(int)\0"
    "on_applyTFButton_clicked()\0"
};

void SpaceSelectionWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SpaceSelectionWidget *_t = static_cast<SpaceSelectionWidget *>(_o);
        switch (_id) {
        case 0: _t->endTransferFunc(); break;
        case 1: _t->startTransferFunc((*reinterpret_cast< TransfuncTypes(*)>(_a[1])),(*reinterpret_cast< TreeItem*(*)>(_a[2])),(*reinterpret_cast< TreeItem*(*)>(_a[3])),(*reinterpret_cast< TreeItem*(*)>(_a[4]))); break;
        case 2: _t->selectedNodeChanged((*reinterpret_cast< TreeItem*(*)>(_a[1]))); break;
        case 3: _t->changeBoundingCubeParams(); break;
        case 4: _t->changeBoundingSphereParams(); break;
        case 5: _t->on_cubeColorButton_clicked(); break;
        case 6: _t->on_sphereColorButton_clicked(); break;
        case 7: _t->on_boundRegionTabs_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_applyTFButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SpaceSelectionWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SpaceSelectionWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_SpaceSelectionWidget,
      qt_meta_data_SpaceSelectionWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SpaceSelectionWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SpaceSelectionWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SpaceSelectionWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SpaceSelectionWidget))
        return static_cast<void*>(const_cast< SpaceSelectionWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int SpaceSelectionWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void SpaceSelectionWidget::endTransferFunc()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
