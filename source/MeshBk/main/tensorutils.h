#ifndef TENSORUTILS_H
#define TENSORUTILS_H

#include <QMap>
#include <opencv2/opencv.hpp>

void tensorHandle(QMap<int, cv::Mat> &allDicomData, QMap<int, cv::Mat> &output);

#endif // TENSORUTILS_H
