#include "opacityfromfilewidget.h"
#include "ui_opacityfromfilewidget.h"
#include "tensorutils.h"

QProgressBar* OpacityFromFileWidget::progressBar;

QMap<int, cv::Mat> handledMask;

bool handledMaskForUse = 0;

const int numOfLabel = 4;

int detectLabel[numOfLabel];

void resetDetectLabel() {
    for (int i = 0; i < numOfLabel; i++) {
        detectLabel[i] = 0;
    }
}

bool haveChooselabel() {
    for (int i = 0; i < numOfLabel; i++) {
        if (detectLabel[i]) return true;
    }
    return false;
}

void checkAllLabel(Ui::OpacityFromFileWidget *ui) {
    resetDetectLabel();
    for (int i = 0; i < numOfLabel; i++) {
        if (ui->tableWidget->item(i, 0)->checkState() == Qt::Checked) {
            detectLabel[i] = 1;
        }
    }
}

// Load dicom slices from a folder path
QMap<int, cv::Mat> load3DVolumeDicom(QString path)
{

    QDir dirOriginal(path);
    assert(dirOriginal.exists());

    QFileInfoList originalDicoms = dirOriginal.entryInfoList();
    QMap<int, cv::Mat> dataSliceMap;

    int count = 100;
    int step = originalDicoms.size() / count;
    count = originalDicoms.size() / step;

    for (int i = 0; i < originalDicoms.size(); i++) {
        try {
            QFileInfo file = originalDicoms.at(i);
            DicomInfo dicom(file.absoluteFilePath());
            dataSliceMap[dicom.getInstanceNumber()] = dicom.getOrginIntensityImage(1024);
        } catch (imebra::StreamReadError exception) {
            //std::cout << "imebra::StreamReadError" << std::endl;
        } catch (imebra::StreamOpenError exception) {
            //std::cout << "imebra::StreamOpenError" << std::endl;
        } catch (imebra::MissingTagError exception) {
            //std::cout << "imebra::MissingTagError" << std::endl;
        } catch (imebra::StreamEOFError exception) {
            //std::cout << "imebra::StreamEOFError" << std::endl;
        } catch (imebra::CodecWrongFormatError exception) {
            //std::cout << "imebra::CodecWrongFormatError" << std::endl;
        }
    }
    return dataSliceMap;
}

OpacityFromFileWidget::OpacityFromFileWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OpacityFromFileWidget)
{
    ui->setupUi(this);
    initializeWidget();

    QHeaderView* header = ui->tableWidget->horizontalHeader();
    header->setResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->tableWidget->setRowCount(numOfLabel);
    ui->tableWidget->setColumnCount(1);
    QStringList list;
    list << "Organ";
    ui->tableWidget->setHorizontalHeaderLabels(list);

    QTableWidgetItem *item0 = new QTableWidgetItem("Bone");
    item0->setCheckState(Qt::Unchecked);
    ui->tableWidget->setItem(0, 0, item0);
    QTableWidgetItem *item1 = new QTableWidgetItem("Kidneys");
    item1->setCheckState(Qt::Unchecked);
    ui->tableWidget->setItem(1, 0, item1);
    QTableWidgetItem *item2 = new QTableWidgetItem("Liver");
    item2->setCheckState(Qt::Unchecked);
    ui->tableWidget->setItem(2, 0, item2);
    QTableWidgetItem *item3 = new QTableWidgetItem("Other");
    item3->setCheckState(Qt::Unchecked);
    ui->tableWidget->setItem(3, 0, item3);
}

OpacityFromFileWidget::~OpacityFromFileWidget()
{
    delete ui;
}

void OpacityFromFileWidget::initializeWidget()
{
    ui->progressBar->setMaximumHeight(10);
    ui->progressBar->setMaximum(100);
    ui->progressBar->setMinimum(0);
    ui->progressBar->hide();
    progressBar = ui->progressBar;
}

bool OpacityFromFileWidget::QCallBack(const int pos, const char *str)
{
    progressBar->show();
    progressBar->setValue(pos);
}

void OpacityFromFileWidget::startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *organ, TreeItem *other)
{
    if (type == TransfuncTypes::OPACITY_FILE) {
        if (md != NULL) {
            currentParentNode = parentNode;
            currentApplyNode = organ;
            currentOtherNode = other;

            ui->applyTFButton->show();
        }
    }
}

void OpacityFromFileWidget::on_importFileButton_clicked()
{
    //QString fileName = QFileDialog::getOpenFileName(this, "Import Opacity File");

    QFileDialog folderChooser;
    folderChooser.setFileMode(QFileDialog::Directory);
    folderChooser.setOption(QFileDialog::ShowDirsOnly);
    QString filePath = folderChooser.getExistingDirectory(this, tr("Open Patient DICOM Folder"), "");

    if (filePath == "")
        return;

    ui->importFileEditText->setText(filePath);
}

void OpacityFromFileWidget::on_applyTFButton_clicked()
{
    checkAllLabel(ui);
    if (haveChooselabel() == false) {
        return;
    }

    if (handledMaskForUse == 0) {
        MeshDocument* md = this->md;
        MeshModel * meshModel = md->mm();
        QString dicomPath = meshModel->dicomPath;
        QMap<int, cv::Mat> rawData = load3DVolumeDicom(dicomPath);
        std::cout << "Load raw data OK" << std::endl;
        tensorHandle(rawData, handledMask);
    }

    handledMaskForUse = 1;

    applyMask();

    setTFOptions();

    //ui->applyTFButton->hide();
    qDebug() << "Machine Learning Based TF Finishes";
}

void OpacityFromFileWidget::applyMask() {



    // Prepare cude data
    int cols = md->measurementPixel.X();
    int rows = md->measurementPixel.Y();
    int slices = md->measurementPixel.Z();

    int modelSize = rows * cols * slices;

    // make transparent for all node
    for (int i = 0; i < modelSize; i++) {
        currentApplyNode->opacity[i] = 0;
        currentOtherNode->opacity[i] = currentParentNode->opacity[i];
    }

    for (int slice = 0; slice < slices; slice++) {
        cv::Mat maskMat = handledMask[slice];
        cv::Mat maskCast;
        maskMat.convertTo(maskCast, CV_16U);
        for (int r = 16; r < rows - 16; r++) {
            for (int c = 16; c < cols - 16; c++) {
                int index = slice*rows*cols + r*cols + c;
                bool needCompute = currentParentNode->opacity[index];
                if (needCompute) {
                    int voxelValue = maskCast.at<unsigned short>(r - 16, c - 16);
                    if (detectLabel[voxelValue] == 1) {
                        currentApplyNode->opacity[index] = 1;
                        currentOtherNode->opacity[index] = 0;
                    }
                }
            }
        }
    }
}



void OpacityFromFileWidget::setTFOptions()
{
    currentParentNode->transfuncOptions.type = TransfuncTypes::OPACITY_FILE;

    TransfuncOptions tfOptions;
    tfOptions.type = TransfuncTypes::NONE;
    NodeViewOptions viewOption;
    viewOption.opacity = currentApplyNode->opacity;
    viewOption.colorTable = currentApplyNode->colorTable;
    currentApplyNode->transfunc = tfOpacityFromFile;
    currentApplyNode->transfuncOptions = tfOptions;
    currentApplyNode->viewOptions = viewOption;
    currentApplyNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);

    TransfuncOptions itfOptions;
    itfOptions.type = TransfuncTypes::NONE;
    NodeViewOptions viewOptionInverted;
    viewOptionInverted.opacity = currentOtherNode->opacity;
    viewOptionInverted.colorTable = currentOtherNode->colorTable;
    currentOtherNode->transfunc = tfOpacityFromFileInverted;
    currentOtherNode->transfuncOptions = itfOptions;
    currentOtherNode->viewOptions = viewOptionInverted;
    currentOtherNode->updateHistogram(md->globalTexture3DIntensityData, QCallBack);
}

vcg::Color4f tfOpacityFromFile(signed short intensityArray[], int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;

    if (options.opacity[index] != 0) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

vcg::Color4f tfOpacityFromFileInverted(signed short *intensityArray, int index, NodeViewOptions options)
{
    float color = intensityArray[index] * 1.0 * 255 / 2048;
    int intensity = intensityArray[index] - 1024;
    if (options.opacity[index] != 0) {
        if (options.isUseColorTable) { // Only show color
            vcg::Color4f colormap = options.colorTable->colorTable[intensity];
            return vcg::Color4f(colormap[0], colormap[1], colormap[2], colormap[3]);
        } else {
            return vcg::Color4f(color, color, color, 255);
        }
    } else {
        return vcg::Color4f(0, 0, 0, 0);
    }
}

//void tempOldFunction() {
//    std::string OSELMPath = "/home/bku/Parallels Shared Folders/Home/graduate-thesis/ABC/";
//    OSELM * oelm = new OSELM();

//    bool loadOK = oelm->loadNet(OSELMPath);

//    if (!loadOK) {
//        QMessageBox::information(this, tr("Neural network"), tr("Can't initial neural network"));
//        return;
//    }

//    int cols = md->measurementPixel.X();
//    int rows = md->measurementPixel.Y();
//    int slices = md->measurementPixel.Z();

//    int modelSize = rows * cols * slices;

//    progressBar->show();

//    progressBar->setValue(0);
//    // make transparent for all node
//    for (int i = 0; i < modelSize; i++) {
//        currentApplyNode->opacity[i] = 0;
//        currentOtherNode->opacity[i] = currentParentNode->opacity[i];
//    }

//    progressBar->setValue(5);

//    MeshDocument* md = this->md;
//    MeshModel * meshModel = md->mm();
//    QString dicomPath = meshModel->dicomPath;

//    //std::cout << "Load dicom " << std::endl;
//    QMap<int, DataSlice> allDicomData = load3DVolumeDicom(dicomPath);

//    float step = ((float)95 / (float)slices);
//    float current = 5;
//    // get each slice data and handle
//    for (int slice = 0; slice < slices; slice++) {
//        DataSlice sliceData = allDicomData[slice];
//        QList<cv::Mat> features = featureForMat(sliceData.mat);


//        cv::Mat norm;
//        cv::normalize(sliceData.mat, norm, 0, 2048, cv::NORM_MINMAX);
//        norm.convertTo(norm, CV_32S);
//        for (int r = 0; r < rows; r++) {
//            for (int c = 0; c < cols; c++) {
//                int index = slice*rows*cols + r*cols + c;
//                bool needCompute = currentParentNode->opacity[index];
//                if (needCompute) {
//                    int voxelValue = norm.at<int>(r, c);

//                    if (voxelValue > 0) {
//                        cv::Mat feature = getFeatureVector(r, c, features);
//                        arma::mat fea(reinterpret_cast<double*>(feature.data), feature.rows, feature.cols );

//                        arma::mat ret = oelm->classifyClass(fea);
//                        int flagClass = (int)ret[0];

//                        if (flagClass) {
//                            currentApplyNode->opacity[index] = 1;
//                            currentOtherNode->opacity[index] = 0;
//                        }
//                    }
//                }

//            }
//        }
//        current += step;
//        progressBar->setValue((int)current);
//    }

//    progressBar->hide();

//    delete oelm;

//    // Mapping

//    setTFOptions();

//    //ui->applyTFButton->hide();
//    qDebug() << "Machine Learning Based TF Finishes";
//}
