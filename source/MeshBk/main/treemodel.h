#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include <QStringList>
#include <common/meshmodel.h>
//class TreeItem;
class TreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    TreeModel(QObject *parent = 0);
    ~TreeModel();

    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    TreeItem *getItem(const QModelIndex &index) const;
    bool addChild(const QModelIndex index, TreeItem* item);
private:
    void setupModelData(const QStringList &lines, TreeItem *parent);
    TreeItem *rootItem;

    // QAbstractItemModel interface
public:
    bool insertRows(int position, int rows, const QModelIndex &parent);
    bool insertColumns(int position, int columns, const QModelIndex &parent);
    bool removeRows(int row, int count, const QModelIndex &parent);
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role);
};
#endif // TREEMODEL_H

vcg::Color4f testFunc(int index, TransfuncOptions options);
