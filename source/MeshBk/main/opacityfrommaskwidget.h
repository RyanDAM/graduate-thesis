#ifndef OPACITYFROMMASKWIDGET_H
#define OPACITYFROMMASKWIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QProgressBar>
#include <treemanagerwidget.h>
#include "common/meshmodel.h"
#include "utils_opendiomsfolder/dicominfo.h"

namespace Ui {
class OpacityFromMaskWidget;
}

class OpacityFromMaskWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OpacityFromMaskWidget(QWidget *parent = 0);
    ~OpacityFromMaskWidget();

    void initializeWidget();
    static QProgressBar* progressBar;
    static bool QCallBack(const int pos, const char* str);
public:
    MeshDocument* md;

    TreeItem *currentApplyNode = NULL;
    TreeItem *currentOtherNode = NULL;
    TreeItem *currentParentNode = NULL;
public:
    inline void setMeshDoc(MeshDocument *meshDoc) {md = meshDoc;}
    void setTFOptions();

public slots:
    void startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *organ, TreeItem *other);

signals:
    void endTransferFunc();
private slots:
    void on_importMaskButton_clicked();

    void on_applyButton_clicked();

private:
    Ui::OpacityFromMaskWidget *ui;
};

#endif // OPACITYFROMMASKWIDGET_H

vcg::Color4f tfOpacityFromMask(signed short intensityArray[], int index, NodeViewOptions options);
vcg::Color4f tfOpacityFromMaskInverted(signed short* intensityArray, int index, NodeViewOptions options);
