/****************************************************************************
** Meta object code from reading C++ file 'colorgraph.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "colorgraph.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'colorgraph.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ColorGraph[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      38,   12,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      93,   83,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ColorGraph[] = {
    "ColorGraph\0\0gray,red,green,blue,alpha\0"
    "addColorPoint(float,float,float,float,float)\0"
    "colorDict\0colorDictChanged(QMap<float,vcg::Color4f>*)\0"
};

void ColorGraph::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ColorGraph *_t = static_cast<ColorGraph *>(_o);
        switch (_id) {
        case 0: _t->addColorPoint((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4])),(*reinterpret_cast< float(*)>(_a[5]))); break;
        case 1: _t->colorDictChanged((*reinterpret_cast< QMap<float,vcg::Color4f>*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ColorGraph::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ColorGraph::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_ColorGraph,
      qt_meta_data_ColorGraph, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ColorGraph::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ColorGraph::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ColorGraph::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ColorGraph))
        return static_cast<void*>(const_cast< ColorGraph*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int ColorGraph::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void ColorGraph::addColorPoint(float _t1, float _t2, float _t3, float _t4, float _t5)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
