#include "fftfilter.h"
#include <iostream>

void shift(cv::Mat magI) {

    // crop if it has an odd number of rows or columns
    magI = magI(cv::Rect(0, 0, magI.cols & -2, magI.rows & -2));

    int cx = magI.cols/2;
    int cy = magI.rows/2;

    cv::Mat q0(magI, cv::Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
    cv::Mat q1(magI, cv::Rect(cx, 0, cx, cy));  // Top-Right
    cv::Mat q2(magI, cv::Rect(0, cy, cx, cy));  // Bottom-Left
    cv::Mat q3(magI, cv::Rect(cx, cy, cx, cy)); // Bottom-Right

    cv::Mat tmp;                            // swap quadrants (Top-Left with Bottom-Right)
    q0.copyTo(tmp);
    q3.copyTo(q0);
    tmp.copyTo(q3);
    q1.copyTo(tmp);                     // swap quadrant (Top-Right with Bottom-Left)
    q2.copyTo(q1);
    tmp.copyTo(q2);
}

void updateMag(cv::Mat complex, std::string title = "Spect") {

    cv::Mat magI;
    cv::Mat planes[] = {cv::Mat::zeros(complex.size(), CV_32F), cv::Mat::zeros(complex.size(), CV_32F)};
    cv::split(complex, planes);                // planes[0] = Re(DFT(I)), planes[1] = Im(DFT(I))

    cv::magnitude(planes[0], planes[1], magI);    // sqrt(Re(DFT(I))^2 + Im(DFT(I))^2)

    // switch to logarithmic scale: log(1 + magnitude)
    magI += cv::Scalar::all(1);
    cv::log(magI, magI);
    shift(magI);
    cv::normalize(magI, magI, 1, 0, cv::NORM_INF); // Transform the matrix with float values into a
                                              // viewable image form (float between values 0 and 1).
    cv::imshow(title, magI);
}

cv::Mat createGausFilterMask(cv::Size mask_size, int x, int y, int ksize, bool normalization, bool invert) {
    // Some corrections if out of bounds
    if(x < (ksize / 2)) {
        ksize = x * 2;
    }
    if(y < (ksize / 2)) {
        ksize = y * 2;
    }
    if(mask_size.width - x < ksize / 2 ) {
        ksize = (mask_size.width - x ) * 2;
    }
    if(mask_size.height - y < ksize / 2 ) {
        ksize = (mask_size.height - y) * 2;
    }

    // call openCV gaussian kernel generator
    double sigma = -1;
    cv::Mat kernelX = cv::getGaussianKernel(ksize, sigma, CV_32F);
    cv::Mat kernelY = cv::getGaussianKernel(ksize, sigma, CV_32F);
    // create 2d gaus
    cv::Mat kernel = kernelX * kernelY.t();
    // create empty mask
    cv::Mat mask = cv::Mat::zeros(mask_size, CV_32F);
    cv::Mat maski = cv::Mat::zeros(mask_size, CV_32F);

    // copy kernel to mask on x,y
    cv::Mat pos(mask, cv::Rect(x - ksize / 2, y - ksize / 2, ksize, ksize));
    kernel.copyTo(pos);

    // create mirrored mask
    cv::Mat posi(maski, cv::Rect(( mask_size.width - x) - ksize / 2, (mask_size.height - y) - ksize / 2, ksize, ksize));
    kernel.copyTo(posi);
    // add mirrored to mask
    cv::add(mask, maski, mask);

    // transform mask to range 0..1
    if(normalization) {
        cv::normalize(mask, mask, 0, 1, cv::NORM_MINMAX);
    }

    // invert mask
    if(invert) {
        mask = cv::Mat::ones(mask.size(), CV_32F) - mask;
    }

    return mask;
}

cv::Mat fftFilter(cv::Mat &inputImage, cv::Mat &kernel) {

    int orr = inputImage.rows;
    int orc = inputImage.cols;

    // Go float
    cv::Mat fImage;
    inputImage.convertTo(fImage, CV_32F);

    cv::Mat paddedImage, paddedKernel;                            //expand input image to optimal size

    int r = cv::getOptimalDFTSize( fImage.rows );
    int c = cv::getOptimalDFTSize( fImage.cols ); // on the border add zero values

    cv::copyMakeBorder(fImage, paddedImage, 0, r - fImage.rows, 0, c - fImage.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));
    cv::Mat planesImage[] = {cv::Mat(paddedImage), cv::Mat::zeros(paddedImage.size(), CV_32F)};
    cv::Mat complexImage;
    cv::merge(planesImage, 2, complexImage);         // Add to the expanded another plane with zeros

    cv::copyMakeBorder(kernel, paddedKernel, 0, r - kernel.rows, 0, c - kernel.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));
    cv::Mat planesKernel[] = {cv::Mat(paddedKernel), cv::Mat::zeros(paddedKernel.size(), CV_32F)};
    cv::Mat complexKernel;
    cv::merge(planesKernel, 2, complexKernel);         // Add to the expanded another plane with zeros

    // FFT
    cv::Mat fourierTransformImage;
    cv::dft(complexImage, fourierTransformImage, cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);
    cv::Mat fourierTransformKernel;
    cv::dft(complexKernel, fourierTransformKernel, cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);

    // Some processing
    //updateMag(fourierTransformImage);
    cv::Mat filteredSpectrum;
    cv::mulSpectrums(fourierTransformImage, fourierTransformKernel, filteredSpectrum, cv::DFT_ROWS); // only DFT_ROWS accepted
    //updateMag(filteredSpectrum, "filltered");

    // IFFT
    cv::Mat inverseTransform;
    cv::dft(filteredSpectrum, inverseTransform, cv::DFT_INVERSE|cv::DFT_REAL_OUTPUT);

    cv::normalize(inverseTransform, inverseTransform, 0, 255, cv::NORM_MINMAX);

    // Back to 8-bits
    cv::Mat finalImage;
    inverseTransform.convertTo(finalImage, CV_8U);

    return finalImage(cv::Rect( 0, 0, orc, orr ));
}
