#config += debug_and_release
TEMPLATE      = subdirs
CONFIG += ordered

SUBDIRS       =     ../meshbkplugins/utils_opendiomsfolder \
                    ../common\
                    ../main\
                    ../meshbkplugins/io_base\
#                ../meshbkplugins/io_3ds\
#                ../meshbkplugins/io_bre\
#                ../meshbkplugins/io_collada \
#                ../meshbkplugins/io_ctm \
#                ../meshbkplugins/io_json \
#                ../meshbkplugins/io_u3d\
#                ../meshbkplugins/io_tri\
#                ../meshbkplugins/io_x3d \
#                ../meshbkplugins/io_gts \
#                ../meshbkplugins/io_expe \
#                ../meshbkplugins/io_pdb \
#                ../meshbkplugins/filter_aging \
#                ../meshbkplugins/filter_ao \
#                ../meshbkplugins/filter_autoalign \
#                ../meshbkplugins/filter_camera \
#                ../meshbkplugins/filter_bnpts \
#                ../meshbkplugins/filter_clean \
#                ../meshbkplugins/filter_colorize \
#                ../meshbkplugins/filter_colorproc \
#                ../meshbkplugins/filter_color_projection \
#                ../meshbkplugins/filter_create \
#                ../meshbkplugins/filter_csg \
#                ../meshbkplugins/filter_dirt \
#                ../meshbkplugins/filter_fractal \
                ../meshbkplugins/filter_func \
#                ../meshbkplugins/filter_img_patch_param \
#                ../meshbkplugins/filter_isoparametrization \
#                ../meshbkplugins/filter_layer \
#                ../meshbkplugins/filter_measure \
                ../meshbkplugins/filter_meshing \
#                ../meshbkplugins/filter_mutualinfoxml \
#                ../meshbkplugins/filter_mls \
#                ../meshbkplugins/filter_photosynth \
#                ../meshbkplugins/filter_plymc \
                ../meshbkplugins/filter_poisson \
#                ../meshbkplugins/filter_qhull \
#                ../meshbkplugins/filter_quality \
#                ../meshbkplugins/filter_sampling \
#                ../meshbkplugins/filter_sdfgpu \
#                ../meshbkplugins/filter_select \
#                ../meshbkplugins/filter_ssynth \
#                ../meshbkplugins/filter_texture \
#                ../meshbkplugins/filter_trioptimize \
#                ../meshbkplugins/filter_unsharp \
#                ../meshbkplugins/filter_zippering \
#                                # rendering stuff
#                ../meshbkplugins/render_splatting \
                ../meshbkplugins/render_gdp \
#                ../meshbkplugins/render_radiance_scaling \
#                ../meshbkplugins/render_rfx \
#                ../meshbkplugins/decorate_base \
#                ../meshbkplugins/decorate_background \
#                ../meshbkplugins/decorate_shadow \
#                ../meshbkplugins/decorate_raster_proj \
#                ../meshbkplugins/edit_select \
#                ../meshbkplugins/edit_pickpoints \
#                ../meshbkplugins/edit_align \
#                ../meshbkplugins/edit_measure \
#                ../meshbkplugins/edit_hole \
                ../meshbkplugins/edit_slice\
#                ../meshbkplugins/edit_point\
#                ../meshbkplugins/edit_quality \
#                ../meshbkplugins/edit_texture \
#                ../meshbkplugins/edit_manipulators \
#                ../meshbkplugins/edit_arc3D \
