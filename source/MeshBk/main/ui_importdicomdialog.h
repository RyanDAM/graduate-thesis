/********************************************************************************
** Form generated from reading UI file 'importdicomdialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMPORTDICOMDIALOG_H
#define UI_IMPORTDICOMDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QTableWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ImportDICOMDialog
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *openFolderBtn;
    QLineEdit *patientDICOMPath;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QTableWidget *organTable;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *isGeneratePointCloud;
    QHBoxLayout *horizontalLayout_3;
    QProgressBar *progressBar;
    QDialogButtonBox *dialogButtons;
    QLabel *statusLabel;
    QCheckBox *importAllCheckbox;

    void setupUi(QDialog *ImportDICOMDialog)
    {
        if (ImportDICOMDialog->objectName().isEmpty())
            ImportDICOMDialog->setObjectName(QString::fromUtf8("ImportDICOMDialog"));
        ImportDICOMDialog->resize(582, 477);
        horizontalLayoutWidget = new QWidget(ImportDICOMDialog);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(90, 10, 381, 31));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMaximumSize);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        openFolderBtn = new QPushButton(horizontalLayoutWidget);
        openFolderBtn->setObjectName(QString::fromUtf8("openFolderBtn"));

        horizontalLayout->addWidget(openFolderBtn);

        patientDICOMPath = new QLineEdit(horizontalLayoutWidget);
        patientDICOMPath->setObjectName(QString::fromUtf8("patientDICOMPath"));

        horizontalLayout->addWidget(patientDICOMPath);

        gridLayoutWidget = new QWidget(ImportDICOMDialog);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 60, 561, 331));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        organTable = new QTableWidget(gridLayoutWidget);
        organTable->setObjectName(QString::fromUtf8("organTable"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(organTable->sizePolicy().hasHeightForWidth());
        organTable->setSizePolicy(sizePolicy);

        gridLayout->addWidget(organTable, 0, 0, 1, 1);

        layoutWidget = new QWidget(ImportDICOMDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 400, 551, 83));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        isGeneratePointCloud = new QCheckBox(layoutWidget);
        isGeneratePointCloud->setObjectName(QString::fromUtf8("isGeneratePointCloud"));
        isGeneratePointCloud->setChecked(false);

        verticalLayout_2->addWidget(isGeneratePointCloud);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        progressBar = new QProgressBar(layoutWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setStyleSheet(QString::fromUtf8(""));
        progressBar->setValue(24);

        horizontalLayout_3->addWidget(progressBar);

        dialogButtons = new QDialogButtonBox(layoutWidget);
        dialogButtons->setObjectName(QString::fromUtf8("dialogButtons"));
        dialogButtons->setOrientation(Qt::Horizontal);
        dialogButtons->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        horizontalLayout_3->addWidget(dialogButtons);


        verticalLayout_2->addLayout(horizontalLayout_3);

        statusLabel = new QLabel(layoutWidget);
        statusLabel->setObjectName(QString::fromUtf8("statusLabel"));
        QFont font;
        font.setPointSize(8);
        statusLabel->setFont(font);
        statusLabel->setStyleSheet(QString::fromUtf8("padding-top: -10px;\n"
"color: rgb(0, 190, 38);"));
        statusLabel->setMargin(0);

        verticalLayout_2->addWidget(statusLabel);


        horizontalLayout_2->addLayout(verticalLayout_2);

        importAllCheckbox = new QCheckBox(ImportDICOMDialog);
        importAllCheckbox->setObjectName(QString::fromUtf8("importAllCheckbox"));
        importAllCheckbox->setGeometry(QRect(10, 40, 111, 21));

        retranslateUi(ImportDICOMDialog);
        QObject::connect(dialogButtons, SIGNAL(accepted()), ImportDICOMDialog, SLOT(accept()));
        QObject::connect(dialogButtons, SIGNAL(rejected()), ImportDICOMDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ImportDICOMDialog);
    } // setupUi

    void retranslateUi(QDialog *ImportDICOMDialog)
    {
        ImportDICOMDialog->setWindowTitle(QApplication::translate("ImportDICOMDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        openFolderBtn->setText(QApplication::translate("ImportDICOMDialog", "Import", 0, QApplication::UnicodeUTF8));
        isGeneratePointCloud->setText(QApplication::translate("ImportDICOMDialog", "Generate PointCloud", 0, QApplication::UnicodeUTF8));
        statusLabel->setText(QString());
        importAllCheckbox->setText(QApplication::translate("ImportDICOMDialog", "Import all", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ImportDICOMDialog: public Ui_ImportDICOMDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMPORTDICOMDIALOG_H
