#ifndef HISTOGRAM2DWIDGET_H
#define HISTOGRAM2DWIDGET_H

#include <QWidget>

namespace Ui {
class Histogram2DWidget;
}

class Histogram2DWidget : public QWidget
{
    Q_OBJECT

public:
    explicit Histogram2DWidget(QWidget *parent = 0);
    ~Histogram2DWidget();

private:
    Ui::Histogram2DWidget *ui;
};

#endif // HISTOGRAM2DWIDGET_H
