#ifndef FEATUREEXTRACTION_H
#define FEATUREEXTRACTION_H

#include <QList>
#include "gabor.h"
#include "armadillo"

QList<cv::Mat> featureForMat(cv::Mat &dicom);

cv::Mat getFeatureVector(int row, int col, QList<cv::Mat> &dataMats);

void imgABS(cv::Mat &fMat, cv::Mat &sMat, cv::Mat &retMat);

void meanSTD(cv::Mat &input, cv::Mat &meanOut, cv::Mat &varianceOut, cv::Size size);

#endif // FEATUREEXTRACTION_H
