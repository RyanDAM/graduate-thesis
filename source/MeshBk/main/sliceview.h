#ifndef SLICEVIEW_H
#define SLICEVIEW_H

#include "../common/interfaces.h"
#include <QGLWidget>
#include <QMouseEvent>
#include <QPoint>
//#include "treeitem.h"

class SliceView : public QGLWidget
{
    Q_OBJECT
public:
    explicit SliceView(QWidget *parent = 0);
    enum class SelectionMode {NONE, CIRCLE, RECT};
    void resizeGL();
    void initializeGL();
//    void paintGL();
    void paintEvent(QPaintEvent *event);
    SelectionMode currentSlectionMode = SelectionMode::NONE;
    MeshDocument *md;
    int radius = 10;
    QList<QPair<vcg::Point3f, vcg::Color4f>> selections;
    QList<QPoint> selectionPoints;
    QMap<int, bool> intensitySelection;
    void updateIntensitySelection();
    int tolerance = 2;
    int selectionMean = 0;
    GLubyte lastColor[3];
    int getTextureIndex(float x, float y, float z);
    QList<int> getNeighbors(int centerIndex);
    QColor backgroundColor;
    QMap<int, bool> selectionFlag;
    QMap<int, bool> getIndexMap();
    void clearAllSelectionData();
protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void readPixels(vcg::Point3f location);
    void drawLine(QPoint start, QPoint cur);
    void drawPoint(QPoint point);
    void drawPoints(QList<QPoint> &points);
    void keyPressEvent(QKeyEvent *);
public slots:
    void backgroundChange(QColor color);
    void backgroundChange(int color);
    void updateIntersectionMesh(MeshModel *mm, MeshDocument *md, vcg::Point3f normal);
    void initTexture3D();
    void toleranceChanged(int value);
signals:
    void showSelection(QMap<int, bool> intensitySelection);
    void hideSelection(QMap<int, bool> intensitySelection);
    void changeBackgroundColor(QColor color);
private:
    MeshModel *mm;
    RenderMode rm;
    QPointF point = QPointF(0, 0);
    vcg::Point3f normalVector;
    GLuint texture3D;
    vcg::Point3f scale;
    vcg::Point3f lastHit;
    bool isDrag = false;
    void drawCirle(QPointF center, float radius);
};

#endif // GLWIDGET_H
