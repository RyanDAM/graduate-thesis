#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
        QApplication a(argc, argv);
        MainWindow *window = new MainWindow();
        window->show();
        window->showMaximized();
        window->setWindowTitle(QString("MeshBk"));

        return a.exec();
}
