#include "histogram2dwidget.h"
#include "ui_histogram2dwidget.h"

Histogram2DWidget::Histogram2DWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Histogram2DWidget)
{
    ui->setupUi(this);
}

Histogram2DWidget::~Histogram2DWidget()
{
    delete ui;
}
