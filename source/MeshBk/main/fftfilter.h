#ifndef FFTFILTER_H
#define FFTFILTER_H

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

cv::Mat fftFilter(cv::Mat &inputImage, cv::Mat &kernel);

#endif // FFTFILTER_H
