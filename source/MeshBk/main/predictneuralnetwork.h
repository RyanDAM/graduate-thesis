#ifndef PREDICTNEURALNETWORK_H
#define PREDICTNEURALNETWORK_H

#include "../common/interfaces.h"

#define NNSIZE          20

class NNData{
public:
    NNClassTypes type;
    NNData();
    ~NNData();
    float convert(int input);
    float reserve(float output);
    float get_max_input_y();
    float get_min_input_y();
    float tansig(float x);
    float* getIW();
    float* getLW();
    float* getB1();
    float getB2();
    float predict(int input_density);
    void loadData(NNClassTypes type);
    QMap<int, float> histogram;

private:
    float ymax_x;
    float ymin_x;
    float xmax_x;
    float xmin_x;

    float ymax_y;
    float ymin_y;
    float xmax_y;
    float xmin_y;

    float iw[NNSIZE];
    float lw[NNSIZE];
    float b1[NNSIZE];
    float b2;
};

class NNPredict{
private:
    float arteryAlpha;
    float boneAlpha;
    float kidneyAlpha;
    float liverAlpha;
    float lungAlpha;
    float portalveinAlpha;
    float venoussystemAlpha;
    float othersAlpha;

    QMap<int, NNClassTypes> pre;
    float findmax(float p1, float p2, float p3, float p4, float p5, float p6, float p7, float p8);
    NNClassTypes predictMax(int intensity);
public:
    NNData arteryN;
    NNData boneN;
    NNData liverN;
    NNData kidneyN;
    NNData lungN;
    NNData portalveinN;
    NNData venoussystemN;
    NNData othersN;
    NNPredict();
    ~NNPredict();
    bool predict(int intensity, NNClassTypes type);
};

void histogram_matching(signed short *global, int size, vcg::CallBackPos* cb = 0);


#endif // PREDICTNEURALNETWORK_H


