#ifndef TREEMANAGERWIDGET_H
#define TREEMANAGERWIDGET_H

#include <QTreeView>
#include <QWidget>
#include <QTableWidget>

#include "treemodel.h"
#include "tfcombodelegate.h"

namespace Ui {
class TreeManagerWidget;
}

enum class TFType {NONE, HISTOGRAM, TRANSFUNC2, TRANSFUNC3};
const int TRANSFUNC_COL = 1;
const int NAME_COL = 0;

class TreeManagerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TreeManagerWidget(QWidget *parent = 0);
    QTreeView *treeView;
    TreeModel *treeModel;
    void initialize();
    ~TreeManagerWidget();
    void insertRow();
    void insertChild();
    bool insertColumn();
    bool removeColumn();
    void removeRow();
    TreeItem* addRootNode(QString name, vcg::CallBackPos* cb = 0);
    QList<QPair<TransFunc, NodeViewOptions>> getListTransFunc(QModelIndex index);
    QList<TreeItem*> getListNodes(QModelIndex index);
    MeshDocument *md = NULL;
    void setMeshDocument(MeshDocument *md);
    static QProgressBar *progressbar;
    static bool QCallBack(const int pos, const char *str);
    TreeItem* newChildNode(QString name, QModelIndex = QModelIndex());
    QPair<TreeItem*, QModelIndex>* insertNewNode(QString name, QModelIndex = QModelIndex());
    void showContexMenu(TreeItem *item, const QPoint& globalPos);

    QTableWidget *viewTable;
    QSignalMapper *removeItemMapper;
    QList<TreeItem *> viewItems;
    void removeViewItemAndChildren(TreeItem *parentItem);
    TreeItem* getSelectedItem();
public slots:
    void selectionChanged(QItemSelection, QItemSelection);
    void dataChanged(QModelIndex topleft, QModelIndex bottomright);
    void endTransferFunc(TransfuncTypes type);
    void onCustomContextMenuRequested(const QPoint& pos);
    void removeNode();
    void addToView();
    void exportMask();
    void importMask();
    void viewSpecificNode();
    void updateViewTable();
    void removeViewItem(int index);
    void resetMask();

private slots:
    void on_splitButton_clicked();

    void on_insertButton_clicked();

    void on_deleteButton_clicked();
    void on_viewButton_clicked();

    void on_pushButton_clicked();

signals:
    void startTransferFunc(TransfuncTypes transfuncType, TreeItem*, TreeItem*, TreeItem*);
    void viewThisItem(TreeItem *item, vcg::CallBackPos* = 0);
    void viewSelectedItems(QList<TreeItem *> items, vcg::CallBackPos* = 0);
    void viewButtonClick();
    void selectedNodeChanged(TreeItem *selectedItem);

private:
    Ui::TreeManagerWidget *ui;
};
#endif // TREEMANAGERWIDGET_H
