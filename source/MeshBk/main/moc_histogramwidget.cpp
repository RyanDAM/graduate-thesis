/****************************************************************************
** Meta object code from reading C++ file 'histogramwidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "histogramwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'histogramwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_HistogramWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   17,   16,   16, 0x05,
      71,   61,   16,   16, 0x05,
     115,   16,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
     135,   16,   16,   16, 0x0a,
     153,   17,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_HistogramWidget[] = {
    "HistogramWidget\0\0colorPoint\0"
    "selectedPointChange(ColorPoint*)\0"
    "colorDict\0colorDictChanged(QMap<float,vcg::Color4f>*)\0"
    "colorTableChanged()\0updatePinPoints()\0"
    "removeColorPoint(ColorPoint*)\0"
};

void HistogramWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HistogramWidget *_t = static_cast<HistogramWidget *>(_o);
        switch (_id) {
        case 0: _t->selectedPointChange((*reinterpret_cast< ColorPoint*(*)>(_a[1]))); break;
        case 1: _t->colorDictChanged((*reinterpret_cast< QMap<float,vcg::Color4f>*(*)>(_a[1]))); break;
        case 2: _t->colorTableChanged(); break;
        case 3: _t->updatePinPoints(); break;
        case 4: _t->removeColorPoint((*reinterpret_cast< ColorPoint*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData HistogramWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject HistogramWidget::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_HistogramWidget,
      qt_meta_data_HistogramWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HistogramWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HistogramWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HistogramWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HistogramWidget))
        return static_cast<void*>(const_cast< HistogramWidget*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int HistogramWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void HistogramWidget::selectedPointChange(ColorPoint * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void HistogramWidget::colorDictChanged(QMap<float,vcg::Color4f> * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void HistogramWidget::colorTableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
