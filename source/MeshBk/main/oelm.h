#ifndef OELM_H
#define OELM_H

#include <armadillo>
#include "elmtype.h"
using namespace arma;

class OSELM {
    private:
    bool firstLearn;
    int numberOfHiddenNeurons;
    int numberOfInput;
    double threshold;
    double arphi;

    arma::mat hiddenWeights;
    arma::mat hiddenBiases;
    arma::mat outputWeights;
    arma::mat lastP;
    arma::mat trainD;

    public:
    ActivationFunction activeFunc;
    std::string path;

    OSELM() {
    }

    OSELM(std::string path) {
        this->path = path;
        this->loadNet(path);
    }

    OSELM(int numberOfInput, int numberOfHiddenNeurons, ActivationFunction activeFunc) {
        this->path = "";
        this->firstLearn = true;
        this->numberOfInput = numberOfInput;
        this->activeFunc = activeFunc;
        this->numberOfHiddenNeurons = numberOfHiddenNeurons;

        this->hiddenWeights = arma::mat(this->numberOfHiddenNeurons, this->numberOfInput, arma::fill::randu)*2 - 1;
        this->hiddenBiases = arma::mat(this->numberOfHiddenNeurons, 1, arma::fill::randu)*2 - 1;
    }

    bool ready() {
        return numberOfHiddenNeurons
                && numberOfInput;
    }

    void onlineSequenceLearning(mat * dataSet, int N0, int Ni, double C, double threshold);

    void startLearning(mat * dataSet, double C);

    arma::mat feedForward(mat &data);

    arma::mat classify(mat &data);

    arma::mat classifyClass(mat &data);

    void printErrorRate(arma::mat &data);

    double testAccurate(ELMData * data);

    void activation(arma::mat &m, arma::mat *ret);

    double activation(double input);

    double setThresholdTo(double threshold);

    void printNet();

    public:

    bool saveNet(std::string path);

    bool loadNet(std::string path);

    private:

    void findArphiNumber(mat &data, double threshold);

    public:

    ~OSELM() {
        // TODO dealloc
    }

};

#endif // OELM_H
