#include "colorgraph.h"
#include <qmath.h>
#include <QColorDialog>

using namespace vcg;

ColorGraph::ColorGraph(QWidget *parent) :
    QGLWidget(parent)
{
    this->backgroundColor = QColor(0, 0, 0);
}

void ColorGraph::resizeGL()
{
}

void ColorGraph::initializeGL()
{
    float grayLength = grayMax - grayMin + 1;
    qglClearColor(this->backgroundColor);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
//    float ratio = this->width() * 1.0 / this->height();
//    float maxWidth = qMax(grayLength, this->width() * 1.0f);
//    float maxHeight = maxWidth * 1.0 / ratio;
    glOrtho(grayMin, grayMax, 0, 255, -0.5, 0.5);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 0, 0, 0, 0, -1, 0, 1, 0);
//    this->alphaOffset = (maxHeight - colorPointRadius) * 1.0 / 255;
}

void ColorGraph::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    drawGradient();
}

void ColorGraph::setColorDict(QMap<float, Color4f> *colorDict)
{
    this->colorDict = colorDict;
    update();
}

void ColorGraph::colorDictChanged(QMap<float, vcg::Color4f> *colorTable)
{
    this->colorDict = colorTable;
    update();
}

void ColorGraph::drawColorPoint(float grayValue, float alpha)
{
    glPointSize(5.0f);
    glBegin(GL_POINTS);
    glVertex2f(grayValue, alpha);
}

void ColorGraph::drawCirle(QPointF center, vcg::Color4f color) {
    int step = 300;
    center.setY(center.y() * alphaOffset);
//    glColor3f(1, 1, 1);
    glColor3f(color[0] / 255, color[1] / 255, color[2] / 255);
    glBegin(GL_POLYGON);
    for (int i = 0; i < step; ++i) {
        float angle = 2.0 * M_PI * i / step;
        float x = center.x() + qCos(angle) * colorPointRadius;
        float y = center.y() + qSin(angle) * colorPointRadius;
        glVertex2f(x, y);
    }
    glEnd();
//    glLineWidth(2);
//    glColor3f(0,0,0);
//    glBegin(GL_LINE_LOOP);
//    for (int i = 0; i < step; ++i) {
//        float angle = 2.0 * M_PI * i / step;
//        float x = center.x() + qCos(angle) * colorPointRadius;
//        float y = center.y() + qSin(angle) * colorPointRadius;
//        glVertex2f(x, y);
//    }
//    glEnd();
    glLineWidth(1);
//    glColor3f(color[0] / 255, color[1] / 255, color[2] / 255);
    glColor3f(0, 0, 0);
    glBegin(GL_LINE_LOOP);
    for (int i = 0; i < step; ++i) {
        float angle = 2.0 * M_PI * i / step;
        float x = center.x() + qCos(angle) * colorPointRadius;
        float y = center.y() + qSin(angle) * colorPointRadius;
        glVertex2f(x, y);
    }
    glEnd();
}

void ColorGraph::drawGradient()
{
    if (colorDict == NULL) return;
    if (colorDict->size() == 0) return;
    QMap<float, vcg::Color4f>::Iterator it = colorDict->begin();
    glBegin(GL_QUAD_STRIP);
    glColor3f(it.value()[0] * 1.0 / 255, it.value()[1] * 1.0 / 255, it.value()[2] * 1.0 / 255);
    glVertex2f(grayMin, 255);
    glVertex2f(grayMin, 0);

    for (;it != colorDict->end(); ++it) {
        glColor3f(it.value()[0] * 1.0 / 255, it.value()[1] * 1.0 / 255, it.value()[2] * 1.0 / 255);
        glVertex2f(it.key(), 255);
        glVertex2f(it.key(), 0);
    }
    it--;
    glColor3f(it.value()[0] * 1.0 / 255, it.value()[1] * 1.0 / 255, it.value()[2] * 1.0 / 255);
    glVertex2f(grayMax, 255);
    glVertex2f(grayMax, 0);

    glEnd();
}

void ColorGraph::drawNumber(QPointF center, QString number)
{
    float offsetX = -50;
    float offsetY = -105;
//    glColor3f(0, 0, 0);
    this->renderText(center.x() + offsetX, center.y() + offsetY, 0, number, QFont("Times", 10, QFont::Normal));
}

void ColorGraph::mouseDoubleClickEvent(QMouseEvent *event)
{
    float gray = grayMin + event->pos().x() * (grayMax - grayMin) * 1.0 / this->width();
    gray = (gray < grayMin) ? grayMin : gray;
    gray = (gray > grayMax) ? grayMax : gray;
//    float realY = (event->pos().y() - colorPointRadius);
//    realY = (realY < colorPointRadius) ? colorPointRadius : realY;
    float alpha = event->pos().y() * 1.0 * (255 * alphaOffset + 2 * colorPointRadius) / this->height() - colorPointRadius;
    alpha = 255 - alpha / alphaOffset;
    alpha = (alpha > 255) ? 255 : alpha;
    alpha = (alpha < 0) ? 0 : alpha;

//    QColor color = QColorDialog::getColor();
//    if (color.isValid()) {
//        emit addColorPoint(gray, color.red(), color.green(), color.blue(), alpha);
//    }
}


