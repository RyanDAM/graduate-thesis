#include "tensorutils.h"
#include "tensorflow/cc/client/client_session.h"
#include "tensorflow/cc/ops/standard_ops.h"
#include "tensorflow/core/framework/graph.pb.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/graph/default_device.h"
#include "tensorflow/core/graph/graph_def_builder.h"
#include "tensorflow/core/lib/core/threadpool.h"
#include "tensorflow/core/lib/strings/stringprintf.h"
#include "tensorflow/core/platform/init_main.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/platform/env.h"
#include "tensorflow/core/public/session.h"
#include "tensorflow/core/protobuf/meta_graph.pb.h"

#include <fstream>
#include <string>

void readString(std::string path, cv::Mat &out) {
    std::ifstream infile(path);
    double number1 = 0, number2 = 0, number3 = 0;
    char c1, c2;
    if (infile.is_open()) {
        while (infile >> number1 >> c1 >> number2 >> c2 >> number3 && c1 == ',' && c2 == ',') {
            //std::cout << number1 << " " << number2 << " " << number3 << std::endl;
        }
    }
    double * data = new double[3];
    data[0] = number1;
    data[1] = number2;
    data[2] = number3;
    out = cv::Mat(1, 3, CV_64F, data);
}

void readTrainMatrix(cv::Mat &meanMatrix, cv::Mat &stdMatrix) {
    readString("/home/bku/TensorflowExample/TrainMatrix/train_mean.out", meanMatrix);
    readString("/home/bku/TensorflowExample/TrainMatrix/train_std.out", stdMatrix);
}

void handleTensorData(QMap<int, cv::Mat> &dataSlices, QMap<int, cv::Mat> &output) {

    // Read trainning mean, std
    cv::Mat meanMatrix, stdMatrix;
    readTrainMatrix(meanMatrix, stdMatrix);
    double mean1, mean2, mean3;
    double std1, std2, std3;
    mean1 = meanMatrix.at<double>(0, 0);
    mean2 = meanMatrix.at<double>(0, 1);
    mean3 = meanMatrix.at<double>(0, 2);
    std1 = stdMatrix.at<double>(0, 0);
    std2 = stdMatrix.at<double>(0, 1);
    std3 = stdMatrix.at<double>(0, 2);
    std::cout << "Read trainning Mean: " << "& STD: OK" << std::endl;

    // Equalize histogram
    QMap<int, cv::Mat> handledMatrix;
    {
        cv::Mat tempSliceData = dataSlices[0];
        int rows = tempSliceData.rows;
        int cols = tempSliceData.cols;
        int slices = dataSlices.size();
        int targetR = 480, targetC = 480;
        int padT = (rows - targetR)/2;
        int padB = rows - targetR - padT;
        int padL = (cols - targetC)/2;
        int padR = cols - targetC - padL;
        for (int sl = 0; sl < slices; sl++) {
            cv::Mat dicomMat = dataSlices[sl];
            dicomMat.setTo(-1024, dicomMat == -2048);
            cv::Mat equalImg;

            // Option 1: Apply Adaptive Equalization
//            cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
//            clahe->setClipLimit(5);
//            dicomMat.convertTo(equalImg, CV_16U);
//            clahe->apply(equalImg, equalImg);
//            equalImg.convertTo(equalImg, CV_32F);

            // Option 3: Do nothing
            dicomMat.convertTo(equalImg, CV_32F);

            cv::Mat mean;
            cv::Mat stddev;
            cv::meanStdDev (equalImg, mean, stddev );
            double meanVal = mean.at<double>(0);
            double stddevVal = stddev.at<double>(0);
            cv::Mat minusMean = equalImg - meanVal;
            cv::Mat divideSTD = minusMean / stddevVal;
            handledMatrix[sl] = divideSTD(cv::Range(padT, rows - padB), cv::Range(padL, cols - padR));
        }
    }

    std::cout << "Histogram Equalization for " << handledMatrix.size() << " dicoms OK" << std::endl;

    {
        int slices = handledMatrix.size();
        for (int sl = 0; sl < slices; sl++) {
            cv::Mat chanel1, chanel2, chanel3;

            chanel2 = handledMatrix[sl].clone();
            chanel2 = chanel2 - mean2;
            chanel2 = chanel2 / std2;

            if (sl - 1 >= 0) {
                chanel1 = handledMatrix[sl - 1].clone();
            } else {
                chanel1 = cv::Mat(chanel2.rows, chanel2.cols, chanel2.type(), cvScalar(0));
            }
            chanel1 = chanel1 - mean1;
            chanel1 = chanel1 / std1;

            if (sl + 1 < slices) {
                chanel3 = handledMatrix[sl + 1].clone();
            } else {
                chanel3 = cv::Mat(chanel2.rows, chanel2.cols, chanel2.type(), cvScalar(0));
            }
            chanel3 = chanel3 - mean3;
            chanel3 = chanel3 / std3;

            cv::Mat out;
            cv::Mat in[] = {chanel1, chanel2, chanel3};
            cv::merge(in, 3, out);
            output[sl] = out;
        }
    }
    std::cout << "Compress chanels for " << output.size() << " inputs OK" << std::endl;
}

void convertMapToTensor(cv::Mat &input, tensorflow::Tensor &tensor) {
    const float * source_data = (float*) input.data;
    auto input_tensor_mapped = tensor.tensor<float, 4>();
    int height = 480;
    int width = 480;
    int depth = 3;

    for (int y = 0; y < height; ++y) {
        const float* source_row = source_data + (y * width * depth);
        for (int x = 0; x < width; ++x) {
            const float* source_pixel = source_row + (x * depth);
            for (int c = 0; c < depth; ++c) {
                const float* source_value = source_pixel + c;
                input_tensor_mapped(0, y, x, c) = (float)*source_value;
            }
        }
    }
}

void convertTensorToMap(tensorflow::Tensor &tensor, cv::Mat &ouput, int rows, int cols) {
    auto outputTensorMapped = tensor.tensor<float, 3>();
    for (int y = 0; y < rows; ++y) {
        for (int x = 0; x < cols; ++x) {
            ouput.at<float>(y, x) = (float)outputTensorMapped(0, y, x);
        }
    }
}

void tensorHandle(QMap<int, cv::Mat> &allDicomData, QMap<int, cv::Mat> &outputData) {
    // For reading checkpoint, check link below
    // INFO: https://stackoverflow.com/questions/35508866/tensorflow-different-ways-to-export-and-run-graph-in-c/43639305#43639305

    using namespace tensorflow;
    using namespace tensorflow::ops;

    // Initialize a tensorflow session
      Session* session;
      Status status = NewSession(SessionOptions(), &session);
      if (!status.ok()) {
        std::cout << status.ToString() << std::endl;
        return;
      } else {
        std::cout << "Create session OK" << std::endl;
      }

      // Read in the protobuf graph we exported
      // (The path seems to be relative to the cwd. Keep this in mind
      // when using `bazel run` since the cwd isn't where you call
      // `bazel run` but from inside a temp folder.)
      //
      //const std::string pathToGraph = "/home/bku/TensorflowExample/TensorModel/graph.pb";
      const std::string pathToGraph = "/home/bku/TensorflowExample/TensorModel/Checkpoint/fcn_stscan-0.meta";
      const std::string checkpointPath = "/home/bku/TensorflowExample/TensorModel/Checkpoint/fcn_stscan-0";

      // Read in the protobuf graph we exported
      MetaGraphDef graph_def;
      status = ReadBinaryProto(Env::Default(), pathToGraph, &graph_def);
      if (!status.ok()) {
        std::cout << status.ToString() << std::endl;
      } else {
        std::cout << "Read graph meta OK" << std::endl;
      }

      // Add the graph to the session
      status = session->Create(graph_def.graph_def());
      if (!status.ok()) {
        std::cout << status.ToString() << std::endl;
        return;
      } else {
          std::cout << "Bind graph to session OK" << std::endl;
      }

      // Read weights from the saved checkpoint
      Tensor checkpointPathTensor(DT_STRING, TensorShape());
      checkpointPathTensor.scalar<std::string>()() = checkpointPath;
      status = session->Run(
              {{ graph_def.saver_def().filename_tensor_name(), checkpointPathTensor },},
              {},
              {graph_def.saver_def().restore_op_name()},
              nullptr);
      if (!status.ok()) {
          std::cout << status.ToString() << std::endl;
      } else {
          std::cout << "Read checkpoint to graph OK" << std::endl;
      }

      // Setup inputs and outputs:

      // Our graph doesn't require any inputs, since it specifies default values,
      // but we'll change an input to demonstrate.
      Tensor isTrain(DT_BOOL, TensorShape());
      isTrain.scalar<bool>()() = 0;

      // Prepare data for tensor
      QMap<int, cv::Mat> rawData;
      handleTensorData(allDicomData, rawData);
      // Clear raw data
      allDicomData.clear();

      for (int slice = 0; slice < rawData.size(); slice++) {

          Tensor inputTensor(DT_FLOAT, TensorShape({1, 480, 480, 3}));
          convertMapToTensor(rawData[slice], inputTensor);
          //std::cout << "Construct inputTensor with info:" << inputTensor.DebugString() << " OK" << std::endl;

          std::vector<std::pair<string, tensorflow::Tensor>> inputs = {
            { "input_img:0", inputTensor},
            { "is_train:0", isTrain},
          };

          // The session will initialize the outputs
          std::vector<tensorflow::Tensor> outputs;

          // Run the session, evaluating our "c" operation from the graph
          status = session->Run(inputs, {"logprob:0"}, {}, &outputs);
          if (!status.ok()) {
            std::cout << status.ToString() << std::endl;
            return;
          } else {
              //std::cout << "Run logprob:0 OK" << std::endl;
          }

          // Grab the first output (we only evaluated one graph node: "c")
          // and convert the node to a scalar representation.
          // std::cerr << "final output size=" << outputs.size() << std::endl;
          tensorflow::Tensor output = std::move(outputs.at(0));

          // (There are similar methods for vectors and matrices here:
          // https://github.com/tensorflow/tensorflow/blob/master/tensorflow/core/public/tensor.h)s
          Scope root = Scope::NewRootScope();
          auto dim = Const(root, 3);
          auto out = ArgMax(root, output, dim);
          std::vector<Tensor> finalArgMax;
          ClientSession session2(root);
          // Run and fetch v
          TF_CHECK_OK(session2.Run({out}, &finalArgMax));
          tensorflow::Tensor argMaxOut = finalArgMax[0];

          auto castArgMaxRet = Cast(root, argMaxOut, DT_FLOAT);
          std::vector<Tensor> castArgMax;
          // Run and fetch v
          TF_CHECK_OK(session2.Run({castArgMaxRet}, &castArgMax));
          tensorflow::Tensor casted = castArgMax[0];
          cv::Mat argMaxMatrix = cv::Mat(480, 480, CV_32F, cvScalar(0));
          convertTensorToMap(casted, argMaxMatrix, 480, 480);
          outputData[slice] = argMaxMatrix;
      }

      // Free any resources used by the session
      session->Close();
}
