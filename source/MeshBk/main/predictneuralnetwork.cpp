#include "predictneuralnetwork.h"
#include "math.h"
#include <fstream>

//-------------------------------------Class NNData-----------------------------
int min_range = -2048;
int max_range = 2047;
int var = 1024;

NNData::NNData(){
    type = NNClassTypes::ORTHERS;
    ymax_x = 1024;
    ymin_x = -1023;
    xmax_x = 1;
    xmin_x = -1;

    ymax_y = 0;
    ymin_y = 0;
    xmax_y = 1;
    xmin_y = -1;
    b2 = 0;
}

NNData::~NNData(){

}

float NNData::convert(int input){
//    y = (ymax-ymin)*(x-xmin)/(xmax-xmin) + ymin;
    float y = (this->ymax_x-this->ymin_x)*(input*1.0-this->xmin_x)/(this->xmax_x-this->xmin_x) + this->ymin_x;
    return y;
}

float NNData::reserve(float output){
    float y = (output - this->ymin_y) * (this->xmax_y - this->xmin_y) / (this->ymax_y - this->ymin_y) + this->xmin_y;
    return y;
}

float NNData::get_max_input_y(){
    return this->ymax_y;
//    return max(input_y);//TODO return max value of input Y
}

float NNData::get_min_input_y(){
    return this->ymin_y;
//    return min(input_y);//TODO return min value of input Y
}

float NNData::tansig(float x){
    return 2/(1+exp(-2*x))-1;
//    return tan((double)x);
}

float* NNData::getIW(){
    return this->iw;
}

float* NNData::getLW(){
    return this->lw;
}

float* NNData::getB1(){
    return this->b1;
}

float NNData::getB2(){
    return this->b2;
}

void NNData::loadData(NNClassTypes type){
    QString filename;
    QString homePath = QDir::currentPath();
    switch (type) {
    case NNClassTypes::BONE:
        filename = homePath + "/trainresult/boneNN.txt";
        break;
    case NNClassTypes::ARTERY:
        filename = homePath + "/trainresult/arteryNN.txt";
        break;
    case NNClassTypes::KIDNEY:
        filename = homePath + "/trainresult/kidneyNN.txt";
        break;
    case NNClassTypes::LIVER:
        filename = homePath + "/trainresult/liverNN.txt";
        break;
    case NNClassTypes::LUNG:
        filename = homePath + "/trainresult/lungNN.txt";
        break;
    case NNClassTypes::PORTALVEIN:
        filename = homePath + "/trainresult/portalveinNN.txt";
        break;
    case NNClassTypes::VENOUSSYSTEM:
        filename = homePath + "/trainresult/venoussystemNN.txt";
        break;
    case NNClassTypes::ORTHERS:
    default:
        filename = homePath + "/trainresult/otherNN.txt";
        break;
    }

    QFile file(filename);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "file.error():" << filename;
        return;
    }
    QTextStream textStream(&file);

    this->xmin_x = textStream.readLine().toFloat();
    this->xmax_x = textStream.readLine().toFloat();
    this->ymin_x = textStream.readLine().toFloat();
    this->ymax_x = textStream.readLine().toFloat();

    this->xmin_y = textStream.readLine().toFloat();
    this->xmax_y = textStream.readLine().toFloat();
    this->ymin_y = textStream.readLine().toFloat();
    this->ymax_y = textStream.readLine().toFloat();

    for(int i = 0; i < 20; i++){
        this->b1[i] = textStream.readLine().toFloat();
    }

    this->b2 = textStream.readLine().toFloat();

    for(int i = 0; i < 20; i++){
        this->iw[i] = textStream.readLine().toFloat();
    }

    for(int i = 0; i < 20; i++){
        this->lw[i] = textStream.readLine().toFloat();
    }

}

float NNData::predict(int input_density){
    //conver input to [-1 1]
    float input_net = this->convert(input_density);
    // tansig(net.IW{1} * a  + net.b{1});
//    float h1 = this->getB1();
    float h1[NNSIZE];

    //TODO: Need to add net.b{1} to h1.
    for(int i = 0; i < 20; i++){
        h1[i] = input_net * this->getIW()[i] + this->getB1()[i];
        h1[i] = this->tansig(h1[i]);
    }

    // (net.LW{2} * y1 + net.b{2});
    float output_net = this->getB2();
    for(int i = 0; i < 20; i++){
        output_net += h1[i] * this->getLW()[i];
    }
    //reserve output_net to origin range
    float output_predict = this->reserve(output_net);
    return output_predict;
}

//-------------------------------------Class NNPredict-----------------------------
//init NNdata from file
NNPredict::NNPredict(){
    this->arteryN.loadData(NNClassTypes::ARTERY);
    this->boneN.loadData(NNClassTypes::BONE);
    this->kidneyN.loadData(NNClassTypes::KIDNEY);
    this->liverN.loadData(NNClassTypes::LIVER);
    this->lungN.loadData(NNClassTypes::LUNG);
    this->othersN.loadData(NNClassTypes::ORTHERS);
    this->portalveinN.loadData(NNClassTypes::PORTALVEIN);
    this->venoussystemN.loadData(NNClassTypes::VENOUSSYSTEM);

    QString filename;
    QString homePath = QDir::currentPath();
    filename = homePath + "/trainresult/alpha.txt";
    QFile file(filename);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "file.error():" << homePath;
        return;
    }
    QTextStream textStream(&file);

    this->arteryAlpha = textStream.readLine().toFloat();
    this->boneAlpha = textStream.readLine().toFloat();
    this->kidneyAlpha = textStream.readLine().toFloat();
    this->liverAlpha = textStream.readLine().toFloat();
    this->lungAlpha = textStream.readLine().toFloat();
    this->portalveinAlpha = textStream.readLine().toFloat();
    this->venoussystemAlpha = textStream.readLine().toFloat();
    this->othersAlpha = textStream.readLine().toFloat();
    float tmp = 0;
    //Calculate value
    for(int i = -2048; i < 2048; i++){
        NNClassTypes tmpType = this->predictMax(i);

        this->pre.insert(i, tmpType);
        tmp = this->arteryN.predict(i);
        this->arteryN.histogram.insert(i, tmp > 0? tmp:0);

        tmp = this->boneN.predict(i);
        this->boneN.histogram.insert(i, tmp > 0? tmp:0);

        tmp = this->kidneyN.predict(i);
        this->kidneyN.histogram.insert(i, tmp > 0? tmp:0);

        tmp = this->liverN.predict(i);
        this->liverN.histogram.insert(i, tmp > 0? tmp:0);

        tmp = this->lungN.predict(i);
        this->lungN.histogram.insert(i, tmp > 0? tmp:0);

        tmp = this->portalveinN.predict(i);
        this->portalveinN.histogram.insert(i, tmp > 0? tmp:0);

        tmp = this->venoussystemN.predict(i);
        this->venoussystemN.histogram.insert(i, tmp > 0? tmp:0);
    }
}

NNPredict::~NNPredict(){
    delete this;
}

//find maximum betwen eight float numbers
float NNPredict::findmax(float p1, float p2, float p3, float p4, float p5, float p6, float p7, float p8){
    float max = p1;
    if(p2 > max) max = p2;
    if(p3 > max) max = p3;
    if(p4 > max) max = p4;
    if(p5 > max) max = p5;
    if(p6 > max) max = p6;
    if(p7 > max) max = p7;
    if(p8 > max) max = p8;
    return max;
}

NNClassTypes NNPredict::predictMax(int intensity){
    float f1, f2, f3, f4, f5, f6, f7, f8;
    f1 = this->arteryN.predict(intensity) /** (this->arteryAlpha > 0 ? this->arteryAlpha: - this->arteryAlpha)*/;
    f2 = this->boneN.predict(intensity) /** (this->boneAlpha > 0 ? this->boneAlpha: - this->boneAlpha)*/;
    f3 = this->kidneyN.predict(intensity) /** (this->kidneyAlpha > 0 ? this->kidneyAlpha: - this->kidneyAlpha)*/;
    f4 = this->liverN.predict(intensity) /** (this->liverAlpha > 0 ? this->liverAlpha: - this->liverAlpha)*/;
    f5 = this->lungN.predict(intensity) /** (this->lungAlpha > 0 ? this->lungAlpha: - this->lungAlpha)*/;
    f6 = this->othersN.predict(intensity) /** (this->othersAlpha > 0 ? this->othersAlpha: - this->othersAlpha)*/;
    f7 = this->portalveinN.predict(intensity) /** (this->portalveinAlpha > 0 ? this->portalveinAlpha: - this->portalveinAlpha)*/;
    f8 = this->venoussystemN.predict(intensity) /** (this->venoussystemAlpha > 0 ? this->venoussystemAlpha: - this->venoussystemAlpha)*/;

    float max = this->findmax(f1, f2, f3, f4, f5, f6, f7, f8);

    if(f1 == max) { return NNClassTypes::ARTERY;}
    if(f2 == max) {return NNClassTypes::BONE;}
    if(f3 == max) {return NNClassTypes::KIDNEY;}
    if(f4 == max) {return NNClassTypes::LIVER;}
    if(f5 == max) {return NNClassTypes::LUNG;}
    if(f7 == max) {return NNClassTypes::PORTALVEIN;}
    if(f8 == max) {return NNClassTypes::VENOUSSYSTEM;}
    return NNClassTypes::ORTHERS;
}

bool NNPredict::predict(int intensity, NNClassTypes type){
    return this->pre.value(intensity) == type;
}

void histogram_matching(signed short *global, int size, vcg::CallBackPos* cb){
    QString homePath = QDir::currentPath();
    QString filename = homePath + "/trainresult/global_histogram.txt";
    QFile file(filename);
    QMap< int, float>  global_histogram;
    QMap< int, float>  global_cumulative;
    QMap< int, float>  new_patient_histogram;
    QMap< int, float>  new_patient_cumulative;
    QMap< int, int> match_table;

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "file.error():" << filename;
        return;
    }
    QTextStream textStream(&file);

    //load global histogram
    //calculate the cumulative distribution probability of global
    float sum = 0;
    for(int i = min_range; i <= max_range; i++){
        float nvalue = textStream.readLine().toFloat();
        global_histogram.insert(i, nvalue);
        sum += nvalue;
        global_cumulative.insert(i, sum);
        new_patient_histogram.insert(i, 0);
    }
    file.close();

    int idx = 0;
    int count = 100;
    int step = size / count;
    count = size / step;

    //histogram of new patient
    int num_intensity = 0;
    for(int i = 0; i < size; i++){
        if(global[i] <= 0 || global[i] > 2047)
            continue;
        int intensity = global[i] - var;
        if(intensity >= min_range && intensity <= max_range ){
            new_patient_histogram.find(intensity).value()++;
            num_intensity++;
        }
        if (i % step == 0) {
            cb(50 * idx * 1.0/ count, "Histogram matching");
            idx++;
        }
    }

    //calculate the cumulative distribution probability of new patient
    //normalize new patient histogram
    sum = 0;
    for(int i = min_range; i <= max_range; i++){
        float nvalue = new_patient_histogram.value(i)/num_intensity;
        new_patient_histogram.find(i).value() = nvalue;
        sum = sum + nvalue;

        new_patient_cumulative.insert(i, sum);
    }

    //Create match table
    int index = min_range;
    for(int i = min_range; i <= max_range; i++){
        match_table.insert(i, index);
        qDebug() << i << " -" << index;
        while(new_patient_cumulative.value(i) >= global_cumulative.value(index) && index < max_range){
            index++;
        }
    }

    idx = 0;
    //match histogram and update global
    for(int i = 0; i < size; i++){
        if(global[i] <= 0 || global[i] > 2047){
            global[i] = 0;
            continue;
        }
        int intensity = global[i] - var;
        global[i] = match_table.value(intensity) + var;

        if (i % step == 0) {
            cb(50 + 50 * idx * 1.0/ count, "Histogram matching");
            idx++;
        }
    }
}


