#include "histogramcolorwidget.h"
#include "ui_histogramcolorwidget.h"
#include <QFileDialog>

HistogramColorWidget::HistogramColorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HistogramColorWidget)
{
    ui->setupUi(this);
    histogramWidget = ui->histogramWidget;
    colorGraphWidget = ui->colorGraphWidget;
    ui->removePinPointButton->setEnabled(false);
    initHistogram();
    connect(histogramWidget, SIGNAL(selectedPointChange(ColorPoint*)), this, SLOT(selectedPointChanged(ColorPoint*)));
    connect(this, SIGNAL(removeColorPoint(ColorPoint*)), histogramWidget, SLOT(removeColorPoint(ColorPoint*)));
    connect(histogramWidget, SIGNAL(colorDictChanged(QMap<float,vcg::Color4f>*)), colorGraphWidget, SLOT(colorDictChanged(QMap<float,vcg::Color4f>*)));
    // Hide slider as default
    setBoundingSliderVisibility(false);
    connect(ui->backgroundSlider, SIGNAL(valueChanged(int)), this, SLOT(backgroundSliderChanged(int)));
    presetList = loadPreset();
    QComboBox *cbox = ui->presetCombobox;
    if (presetList.size() > 0) {
        QMap<QString, QMap<float, vcg::Color4f>*>::Iterator it =  presetList.begin();
        for (; it != presetList.end(); ++it) {
            cbox->addItem(it.key());
        }

    }
    connect(cbox, SIGNAL(currentIndexChanged(QString)), this, SLOT(presetChanged(QString)));
    ui->isUseColoorCheckbox->setChecked(false);
    ui->presetCombobox->setDisabled(true);
    ui->isUseColoorCheckbox->setEnabled(false); // Disable isUseColor checkbox as default
}

HistogramColorWidget::~HistogramColorWidget()
{
    delete ui;
}

void HistogramColorWidget::setColorTable(IntensityColorTable *colorTable)
{
    this->colorTable = colorTable;
    if (colorTable) {
        histogramWidget->setColorDict(&(this->colorTable->colorDict));
        colorGraphWidget->setColorDict(&(this->colorTable->colorDict));
    } else {
        histogramWidget->setColorDict(NULL);
        colorGraphWidget->setColorDict(NULL);
    }
    // Enable isUseTable checkbox when current treeitem != null
    ui->isUseColoorCheckbox->setEnabled(true);
}

void HistogramColorWidget::setBoundingSliderVisibility(bool isShown)
{
    ui->leftBoundSlider->setVisible(isShown);
    ui->rightBoundSlider->setVisible(isShown);
}

void HistogramColorWidget::setIsUseColorTable(bool isUsed)
{
    this->isUseColorTable = isUsed;
    ui->isUseColoorCheckbox->setChecked(isUsed);
    ui->histogramWidget->setIsDrawColor(isUsed);
}

void HistogramColorWidget::resetBoundingSlider()
{
    ui->leftBoundSlider->setValue(0);
    ui->rightBoundSlider->setValue(ui->rightBoundSlider->maximum());
}

void HistogramColorWidget::onThresholdChange(int value)
{
    QSlider *leftSlider = ui->leftBoundSlider;
    QSlider *rightSlider = ui->rightBoundSlider;
    int range = histogramWidget->grayMax - histogramWidget->grayMin;

    histogramWidget->leftBound = histogramWidget->grayMin + leftSlider->value() * range / 1000;
    histogramWidget->rightBound = histogramWidget->grayMax - (1000 - rightSlider->value()) * range / 1000;
    histogramWidget->update();
    isSliderChanged = true;
    emit enableApplyButton();
}

void HistogramColorWidget::initHistogram()
{
   leftSlider = ui->leftBoundSlider;
   rightSlider = ui->rightBoundSlider;
   connect(leftSlider, SIGNAL(valueChanged(int)), this, SLOT(onThresholdChange(int)));
   connect(rightSlider, SIGNAL(valueChanged(int)), this, SLOT(onThresholdChange(int)));
   histogramWidget->histogram = &(this->histogram);
}

void HistogramColorWidget::selectedPointChanged(ColorPoint *colorPoint)
{
    ui->removePinPointButton->setEnabled(colorPoint != NULL);
    currentSelectedPoint = colorPoint;
}

void HistogramColorWidget::backgroundSliderChanged(int value)
{
    float percent = value * 1.0 / ui->backgroundSlider->maximum();
    QColor color(255 * percent, 255 * percent, 255 * percent);
    ui->histogramWidget->backgroundColor = color;
    ui->backgroundSlider->setStyleSheet("background-color: rgb(" + QString::number(color.red()) + "," + QString::number(color.red()) + "," + QString::number(color.red()) + ")");
    ui->histogramWidget->update();
}

void HistogramColorWidget::presetChanged(QString key)
{
    if (colorTable) {
        colorTable->setColorDict(*presetList[key]);
        setColorTable(colorTable);
        emit enableApplyButton();
    }
}

void HistogramColorWidget::on_removePinPointButton_clicked()
{
    ui->removePinPointButton->setEnabled(false);
    emit removeColorPoint(currentSelectedPoint);
}

void HistogramColorWidget::on_loadHistogram_clicked()
{
    QMap<int, int> histo = startLoadFromFolder(QCallBack);
    if (histo.size() > 0)
        histogramWidget->addExternalHistogram(histo);
//    histogramWidget->setHistogram(&(this->histogram));

}

QMap<int, int> HistogramColorWidget::startLoadFromFolder(vcg::CallBackPos *cb)
{
    QMap<int, int> histogram;
    QFileDialog folderChooser;
    folderChooser.setFileMode(QFileDialog::Directory);
    folderChooser.setOption(QFileDialog::ShowDirsOnly);
    QString organFolder = folderChooser.getExistingDirectory(this, tr("Open Mask Folder"), "/home/hieunguyen/patientdata"); //TODO hardcode recent path
    QList<QString> paths;
    if (organFolder != "")
        paths.push_back(organFolder);
    if (paths.size() == 0) {
        qDebug() << "startLoadFromFolder: No folder selected";
        return histogram;
    }
    foreach (QString path, paths) {
        DicomProcess dicomProcess;
        QMap<int, DataSlice> dataSlices = dicomProcess.extractOrganSlices(path, cb);
        int size = dataSlices.size();

        for(int i = 60; i < size; ++i) {
            cv::Mat originalMat = dataSlices[i].mat;
            cv::Mat demoMaskMat = dataSlices[i].mask;

            for (int row = 0; row < originalMat.rows; ++row) {
                for (int col = 0; col < originalMat.cols; ++col) {
                    if (demoMaskMat.at<signed short>(row, col) == 0) continue;
                    histogram[originalMat.at<signed short>(row, col) - 1024]++;
                }
            }
            if (cb) {
               cb(i * 1.0 * 100 / size, "Colecting data");
            }
        }
    }
    return histogram;
}

void HistogramColorWidget::on_resetHistogramButton_clicked()
{
//    this->histogramWidget->clearHistogram();
    histogramWidget->resetExternalHistogramList();
}

void HistogramColorWidget::on_isUseColoorCheckbox_stateChanged(int arg1)
{
    this->histogramWidget->setIsDrawColor(arg1);
    this->isUseColorTable = arg1;
    ui->presetCombobox->setDisabled(!arg1);
    emit colorTableChanged();
}

void HistogramColorWidget::on_refreshHistoButton_clicked()
{

    emit reloadHistogram();
}

QMap<QString, QMap<float, vcg::Color4f>*> HistogramColorWidget::loadPreset()
{
    QMap<QString, QMap<float, vcg::Color4f>*> presets;
    QDomDocument xmlBOM;
    QString homePath = QDir::currentPath();
    QString filename = homePath + "/presets.xml";
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Error while loading file";
        return presets;
    }
    xmlBOM.setContent(&file);
    file.close();

    QDomElement rootNode = xmlBOM.documentElement();
    QDomElement component = rootNode.firstChild().toElement();
    while (!component.isNull()) {
        if (component.tagName() == "Preset") {
            QMap<float, vcg::Color4f>* colorDict = new QMap<float, vcg::Color4f>();
            // Extract name and color
            QString name = component.attribute("name", "None");
            presets[name] = colorDict;
            QString colorPoints = component.attribute("colorPoints", "");
            QStringList list = colorPoints.split(" ");
            int number = list.at(0).toInt();
            int pointer = 1;
            for (int i = 0; i < number; ++i) {
                float intensty = list.at(pointer).toFloat();
                int red = list.at(pointer + 1).toFloat() * 255;
                int green = list.at(pointer + 2).toFloat() * 255;
                int blue = list.at(pointer + 3).toFloat() * 255;
                int alpha = list.at(pointer + 4).toFloat() * 255;
                pointer = pointer + 5;
                (*colorDict)[intensty] = vcg::Color4f(red, green, blue, alpha);
            }
        }
        // Next Component
        component = component.nextSibling().toElement();
    }

    return presets;

    // DEBUG section. JUST FOR TEST
    QMap<QString, QMap<float, vcg::Color4f>*>::Iterator it = presets.begin();
    for (; it != presets.end(); ++it) {
        QString name = it.key();
        QMap<float, vcg::Color4f>* colorDic = it.value();
        QMap<float, vcg::Color4f>::Iterator p = colorDic->begin();
        qDebug() << "Preset name: " + name;
        for (; p != colorDic->end(); ++p) {
            vcg::Color4f color = p.value();
            float intensity = p.key();
            qDebug() << QString::number(intensity) + " -> (" + QString::number(color[0]) + ", " + QString::number(color[1]) + ", " + QString::number(color[2]) + ", " + QString::number(color[3]) + ")";
        }
    }
}
