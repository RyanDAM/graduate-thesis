/****************************************************************************
** Meta object code from reading C++ file 'opacityfromfilewidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "opacityfromfilewidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'opacityfromfilewidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OpacityFromFileWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      23,   22,   22,   22, 0x05,

 // slots: signature, parameters, type, tag, flags
      69,   41,   22,   22, 0x0a,
     133,   22,   22,   22, 0x08,
     163,   22,   22,   22, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_OpacityFromFileWidget[] = {
    "OpacityFromFileWidget\0\0endTransferFunc()\0"
    "type,parentNode,organ,other\0"
    "startTransferFunc(TransfuncTypes,TreeItem*,TreeItem*,TreeItem*)\0"
    "on_importFileButton_clicked()\0"
    "on_applyTFButton_clicked()\0"
};

void OpacityFromFileWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OpacityFromFileWidget *_t = static_cast<OpacityFromFileWidget *>(_o);
        switch (_id) {
        case 0: _t->endTransferFunc(); break;
        case 1: _t->startTransferFunc((*reinterpret_cast< TransfuncTypes(*)>(_a[1])),(*reinterpret_cast< TreeItem*(*)>(_a[2])),(*reinterpret_cast< TreeItem*(*)>(_a[3])),(*reinterpret_cast< TreeItem*(*)>(_a[4]))); break;
        case 2: _t->on_importFileButton_clicked(); break;
        case 3: _t->on_applyTFButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData OpacityFromFileWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OpacityFromFileWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_OpacityFromFileWidget,
      qt_meta_data_OpacityFromFileWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OpacityFromFileWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OpacityFromFileWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OpacityFromFileWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OpacityFromFileWidget))
        return static_cast<void*>(const_cast< OpacityFromFileWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int OpacityFromFileWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void OpacityFromFileWidget::endTransferFunc()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
