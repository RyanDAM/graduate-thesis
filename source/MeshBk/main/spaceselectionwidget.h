#ifndef SPACESELECTIONWIDGET_H
#define SPACESELECTIONWIDGET_H

#include <QWidget>
#include <QColorDialog>
#include <QProgressBar>
#include <treemanagerwidget.h>
#include "glarea.h"

namespace Ui {
class SpaceSelectionWidget;
}

struct BoundingCube {
    int minX;
    int maxX;
    int minY;
    int maxY;
    int minZ;
    int maxZ;
};

struct BoundingSphere {
    float centerX;
    float centerY;
    float centerZ;
    float radius;
};

class SpaceSelectionWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SpaceSelectionWidget(QWidget *parent = 0);
    ~SpaceSelectionWidget();

public:
    static QProgressBar* progressBar;
    static bool QCallBack(const int pos, const char* str);

public:
    int sliceRows = 0;
    int sliceCols = 0;
    int numOfSlice = 0;
    float sliceXSpace = 0.0f;
    float sliceYSpace = 0.0f;
    float sliceZSpace = 0.0f;

    signed short *inputRawIntensity = NULL;

    MeshDocument *md;
    GLArea *gla;

    TreeItem *currentApplyNode = NULL;
    TreeItem *currentOtherNode = NULL;
    TreeItem *currentParentNode = NULL;
    TreeItem *currentSelectedNode = NULL;

public:
    void initializeWidgets();
    void initializeBoundingCube();
    void initializeBoundingSphere();

public:
    inline void setSliceMetaData(int rows, int cols, int num) {
        this->sliceRows = rows;
        this->sliceCols = cols;
        this->numOfSlice = num;
    }
    inline void setCoefficients(float xSpace, float ySpace, float zSpace){
        this->sliceXSpace = xSpace;
        this->sliceYSpace = ySpace;
        this->sliceZSpace = zSpace;
    }
    void setMeshDoc(MeshDocument* meshDoc);

    void setGLA(GLArea* glarea);

public:
    BoundingCube buildBoundingCube();
    BoundingSphere buildBoundingSphere();
    bool mapping(BoundingCube boundingCube, BoundingSphere boundingSphere, vcg::CallBackPos *cb = 0);
    void setTFOptions();

public slots:
    void startTransferFunc(TransfuncTypes type, TreeItem *parentNode, TreeItem *organ, TreeItem *other);
    void selectedNodeChanged(TreeItem *selectedItem);
    void changeBoundingCubeParams();
    void changeBoundingSphereParams();

signals:
    void endTransferFunc();

private slots:
    void on_cubeColorButton_clicked();

    void on_sphereColorButton_clicked();

    void on_boundRegionTabs_currentChanged(int index);

    void on_applyTFButton_clicked();

private:
    Ui::SpaceSelectionWidget *ui;
};

#endif // SPACESELECTIONWIDGET_H

vcg::Color4f tfSpaceSelection(signed short intensityArray[], int index, NodeViewOptions options);
vcg::Color4f tfSpaceSelectionInverted(signed short* intensityArray, int index, NodeViewOptions options);

