#include "tfcombodelegate.h"

#include <QComboBox>

TFComboDelegate::TFComboDelegate(QObject *parent)
{

}

QWidget *TFComboDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QComboBox *editor = new QComboBox(parent);
    QStringList list;
    list << "None" << "Histogram" << "Probability Model" << "Region Growing" << "Histogram 2D" << "Median Filter" << "Space Selection" << "Machine Learning Based" << "Opacity From Mask";
    editor->addItems(list);
    return editor;
}

void TFComboDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QString value = index.model()->data(index, Qt::DisplayRole).toString();
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
//    comboBox->addItem(value);
}

void TFComboDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    QString value = comboBox->currentText();
    model->setData(index, value);
}

void TFComboDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}
