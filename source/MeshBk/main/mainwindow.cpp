#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "rendermodeactions.h"
#include "importdicomdialog.h"



QProgressBar *MainWindow::qb;
// Firstly call parent constructor and then initialize member attribute ui
MainWindow::MainWindow() : QMainWindow(), ui(new Ui::MainWindow) , openFolderThread(new QThread){
    ui->setupUi(this);
    // Setting up application icon
    QIcon icon;
    icon.addPixmap(QPixmap(":images/meshbk_logo.png"));
    setWindowIcon(icon);

    // Create a container
    mdiarea = ui->mdiarea;
    // If you won't do this, the mdiarea will collapse and never shows out if you don't set a specific size
    this->setCentralWidget(mdiarea);
    mdiarea->showMaximized(); // Let the mdiarea expand the whole space of its parent

    // Initialize global params. We do not need to care about these at this time
    GLArea::initGlobalParameterSet(& currentGlobalParams);
    PM.loadPlugins(defaultGlobalParams); //Load all available plugin with a default param (null in our case)

    colorDialog = new QColorDialog(this);
    connect(colorDialog, SIGNAL(currentColorChanged(QColor)), this, SLOT(applyColoringFilter(QColor)));
    connect(colorDialog, SIGNAL(colorSelected(QColor)), this, SLOT(applyColoringFilter(QColor)));
    connect(colorDialog, SIGNAL(rejected()), this, SLOT(cancelColoringFilter()));

    defaultFaceColor.setRgb(200, 200, 200);
    initToolBar();
    initLayerDock();
    initVolumePropertyDock();
    initMenus();
    initProject();
    initStatusBar();
    ui->sliceViewWidget->setMeshDocument(meshDoc());
    ui->volumeProperty->setMeshDocument(meshDoc());

    connect(this->meshDoc(), SIGNAL(documentUpdated()), this, SLOT(updateLayerDock()));

    // Init Tree Manager dock
    connect(ui->treeDock, SIGNAL(visibilityChanged(bool)), this, SLOT(onTreeManagerDockToggle(bool)));
    ui->treeDock->setVisible(false);
    ui->treeManagerWidget->setMeshDocument(meshDoc());

    // Init SliceView dock
    connect(ui->sliceDock, SIGNAL(visibilityChanged(bool)), this, SLOT(onSliceDockToggle(bool)));
    ui->sliceDock->setVisible(false);
    treeManager = ui->treeManagerWidget;
    connect(ui->treeManagerWidget,
            SIGNAL(startTransferFunc(TransfuncTypes, TreeItem*, TreeItem*, TreeItem*)), ui->volumeProperty, SLOT(startTransferFunc(TransfuncTypes, TreeItem*, TreeItem*, TreeItem*)));

    connect(ui->treeManagerWidget, SIGNAL(startTransferFunc(TransfuncTypes,TreeItem*,TreeItem*,TreeItem*)), this, SLOT(startTransferFunc(TransfuncTypes,TreeItem*,TreeItem*,TreeItem*)));
    connect(ui->treeManagerWidget, SIGNAL(startTransferFunc(TransfuncTypes,TreeItem*,TreeItem*,TreeItem*)), ui->sliceViewWidget, SLOT(startTransferFunc(TransfuncTypes,TreeItem*,TreeItem*,TreeItem*)));

    // Histogram 2D Dock
    ui->histogram2DDock->setVisible(false);
    ui->histogram2DWidget->setMeshDoc(this->meshDoc());
    ui->histogram2DWidget->setGLA(GLA());
    connect(ui->treeManagerWidget,
            SIGNAL(startTransferFunc(TransfuncTypes, TreeItem*, TreeItem*, TreeItem*)), ui->histogram2DWidget, SLOT(startTransferFunc(TransfuncTypes, TreeItem*, TreeItem*, TreeItem*)));

    // Space Selection dock
    ui->spaceSelectionDock->setVisible(false);
    ui->spaceSelectionWidget->setMeshDoc(this->meshDoc());
    ui->spaceSelectionWidget->setGLA(GLA());
    connect(ui->treeManagerWidget,
            SIGNAL(startTransferFunc(TransfuncTypes, TreeItem*, TreeItem*, TreeItem*)), ui->spaceSelectionWidget, SLOT(startTransferFunc(TransfuncTypes, TreeItem*, TreeItem*, TreeItem*)));

    // Machine Learning Based Dock - (Opacity From File)
    ui->opacityFromFileDock->setVisible(false);
    ui->opacityFromFileWidget->setMeshDoc(this->meshDoc());
    connect(ui->treeManagerWidget,
            SIGNAL(startTransferFunc(TransfuncTypes, TreeItem*, TreeItem*, TreeItem*)), ui->opacityFromFileWidget, SLOT(startTransferFunc(TransfuncTypes, TreeItem*, TreeItem*, TreeItem*)));

    // Opacity From Mask Dock
    ui->opacityFromMaskDock->setVisible(false);
    ui->opacityFromMaskWidget->setMeshDoc(this->meshDoc());
    connect(ui->treeManagerWidget,
            SIGNAL(startTransferFunc(TransfuncTypes, TreeItem*, TreeItem*, TreeItem*)), ui->opacityFromMaskWidget, SLOT(startTransferFunc(TransfuncTypes, TreeItem*, TreeItem*, TreeItem*)));

    connect(ui->sliceViewWidget, SIGNAL(endTransferFunc(TransfuncTypes)), this, SLOT(endTransferFunc(TransfuncTypes)));
    connect(ui->volumeProperty, SIGNAL(endTransferFunc(TransfuncTypes)), this, SLOT(endTransferFunc(TransfuncTypes)));

    connect(ui->treeManagerWidget, SIGNAL(selectedNodeChanged(TreeItem*)), ui->volumeProperty, SLOT(selectedNodeChanged(TreeItem*)));
    connect(ui->treeManagerWidget, SIGNAL(viewButtonClick()), this, SLOT(updateGLArea()));

    connect(ui->treeManagerWidget, SIGNAL(selectedNodeChanged(TreeItem*)), ui->spaceSelectionWidget, SLOT(selectedNodeChanged(TreeItem*)));

    connect(ui->treeManagerWidget, SIGNAL(selectedNodeChanged(TreeItem*)), this, SLOT(changeCurrentTreeItem(TreeItem*)));

}

void MainWindow::applyPoissonFilter() {
    RichParameterSet* dummyParSet = new RichParameterSet();
    MeshModel *m = meshDoc()->mm();
    poissonFilter->initParameterSet(poissonToolBar, *m, *dummyParSet);
    poissonFilter->applyFilter(poissonToolBar, *meshDoc(), *dummyParSet, QCallBack);
    GLA()->endEdit();
    qDebug() << "apply poisson filter";
    updateLayerDock();
    MainWindow::globalStatusBar()->hide(); // Hide prgress bar when complete
    // If the new mesh has normals inside, flip them to make sure this mesh is visible
    vcg::tri::Clean<CMeshO>::FlipNormalOutside(meshDoc()->mm()->cm);
    meshDoc()->mm()->UpdateBoxAndNormals();
    meshDoc()->mm()->clearDataMask(MeshModel::MM_FACEFACETOPO);
    GLA()->setColorMode(vcg::GLW::CMPerFace);
    applyColoringFilter(defaultFaceColor);
    GLA()->update();
}

void MainWindow::applyNormalGenerateFilter() {
    RichParameterSet* dummyParSet = new RichParameterSet();
    MeshModel *m = meshDoc()->mm();
    normalGenerateFilter->initParameterSet(normalGenerateToolBar, *m, *dummyParSet);
    normalGenerateFilter->applyFilter(normalGenerateToolBar, *meshDoc(), *dummyParSet, QCallBack);
    GLA()->endEdit();
    qDebug() << "apply normals generate filter";
    updateLayerDock();
    MainWindow::globalStatusBar()->hide(); // Hide prgress bar when complete
}

void MainWindow::applyColoringFilter(const QColor &color) {

    meshDoc()->mm()->updateDataMask(MeshModel::MM_FACECOLOR);
    // Init default params
    RichParameterSet* dummyParSet = new RichParameterSet();
    MeshModel *m = meshDoc() ->mm();
    coloringFilter->initParameterSet(coloringAction, *m, *dummyParSet);

    // Get RGB color from QColorDialog when it changes current color
    int *r = new int, *g = new int, *b = new int, *a = new int;
    color.getRgb(r, g, b, a);
    dummyParSet->setValue(tr("r"), StringValue(QString::number(*r)));
    dummyParSet->setValue(tr("g"), StringValue(QString::number(*g)));
    dummyParSet->setValue(tr("b"), StringValue(QString::number(*b)));

    // Colorize mesh
    coloringFilter->applyFilter(coloringAction, *meshDoc(), *dummyParSet, QCallBack);
    GLA()->endEdit();
    updateLayerDock();
    MainWindow::globalStatusBar()->hide();

    qDebug() << "apply mesh coloring";
}
void MainWindow::applyInvertFacesOrientationFilter() {
    if (!meshDoc()->mm()) return;
    RichParameterSet* dummyParSet = new RichParameterSet();
    MeshModel *m = meshDoc()->mm();
    invertFaceOrientationFilter->initParameterSet(invertFaceOrientationToolBar, *m, *dummyParSet);
    invertFaceOrientationFilter->applyFilter(invertFaceOrientationToolBar, *meshDoc(), *dummyParSet, QCallBack);
    GLA()->endEdit();
    qDebug() << "apply invert faces orientation";
    updateLayerDock();
    MainWindow::globalStatusBar()->hide(); // Hide prgress bar when complete
}

MainWindow::~MainWindow()
{
    openFolderThread->quit();
    openFolderThread->wait();

    delete colorDialog;
    colorDialog = NULL;

    delete ui;
}

/** Initialize a new project
 * @brief MainWindow::createNewProject
 * @param projName: name of the project to be created
 * @return
 */
GLArea* MainWindow::createNewProject(const QString& projName)
{
    MultiViewer_Container *mvcont = new MultiViewer_Container(mdiarea);
    mdiarea->addSubWindow(mvcont);
//    connect(mvcont,SIGNAL(updateMainWindowMenus()),this,SLOT(updateMenus()));
//    filterMenu->setEnabled(!filterMenu->actions().isEmpty());
//    if (!filterMenu->actions().isEmpty())
//        updateSubFiltersMenu(true,false);
    GLArea *gla=new GLArea(mvcont, &currentGlobalParams);
    gla->sliceView = ui->sliceViewWidget->sliceView;
    mvcont->addView(gla, Qt::Horizontal);
    if (projName.isEmpty())
    {
        static int docCounter = 1;
        mvcont->meshDoc.setDocLabel(QString("Project_") + QString::number(docCounter));
        ++docCounter;
    }
    else
        mvcont->meshDoc.setDocLabel(projName);
//    mvcont->setWindowTitle(mvcont->meshDoc.docLabel());
    //if(mdiarea->isVisible())

//    layerDialog->updateTable();
//    layerDialog->updateDecoratorParsView();
    mvcont->showMaximized();
    gla->showMaximized();
    return gla;
}

bool MainWindow::importMesh(QString name, QList<Point3f> points, CallBackPos *cb)
{
    if (!GLA()) {
        this->createNewProject();
        if (!GLA()) return false;
    }
    int vertexNum = points.size();
    RenderMode rm;
    rm.drawMode = vcg::GLW::DMNone;
    MeshModel *mm = meshDoc()->addNewMesh("", name, true, rm);
    vcg::tri::Allocator<CMeshO>::AddVertices(mm->cm, vertexNum);
    for (int i = 0; i < vertexNum; ++i) {
        mm->cm.vert[i].P()[0] = points.at(i).X();
        mm->cm.vert[i].P()[1] = points.at(i).Y();
        mm->cm.vert[i].P()[2] = points.at(i).Z();
//        if (cb) {
//            cb(25 + i * 25 / (vertexNum - 1), "Importing mesh");
//        }
    }
    mm->UpdateBoxAndNormals();
    this->currentViewContainer()->resetAllTrackBall();
    turnAllLightsOn();
    updateLayerDock();
    qDebug() << "Import list of vcg::Point3f done";
    return true;

}

bool MainWindow::importMesh(QString name, QList<cv::Point3f> points, vcg::CallBackPos *cb)
{
    if (!GLA()) {
        this->createNewProject();
        if (!GLA()) return false;
    }
    int vertexNum = points.size();
    RenderMode rm;
    rm.drawMode = vcg::GLW::DMNone;
    MeshModel *mm = meshDoc()->addNewMesh("", name, true, rm);
    vcg::tri::Allocator<CMeshO>::AddVertices(mm->cm, vertexNum);
    for (int i = 0; i < vertexNum; ++i) {
        mm->cm.vert[i].P()[0] = points.at(i).x;
        mm->cm.vert[i].P()[1] = points.at(i).y;
        mm->cm.vert[i].P()[2] = points.at(i).z;
        if (cb) {
            cb(25 + i * 25 / (vertexNum - 1), "Importing mesh");
        }
    }
    mm->UpdateBoxAndNormals();
    this->currentViewContainer()->resetAllTrackBall();
    turnAllLightsOn();
    updateLayerDock();
    return true;
}

bool MainWindow::QCallBack(const int pos, const char * str)
{
//    int static lastPos=-1;
//    if(pos==lastPos) return true;
//    lastPos=pos;

//    static QTime currTime = QTime::currentTime();
////    if(currTime.elapsed()< 100) return true;
//    qDebug() << "QCallBack " << currTime.elapsed();
//    currTime.start();
    MainWindow::globalStatusBar()->show();
    MainWindow::globalStatusBar()->showMessage(str,5000);
    qb->show();
    qb->setEnabled(true);
    qb->setValue(pos);
    MainWindow::globalStatusBar()->update();
    qApp->processEvents();
    return true;
}

bool MainWindow::loadMesh(const QString& fileName, MeshIOInterface *pCurrentIOPlugin, MeshModel* mm, int& mask,RichParameterSet* prePar)
{
    if ((GLA() == NULL) || (mm == NULL))
        return false;

    QMap<int,RenderMode>::iterator it = GLA()->rendermodemap.find(mm->id());
    if (it == GLA()->rendermodemap.end())
        return false;
    RenderMode& rm = it.value();
    QFileInfo fi(fileName);
    QString extension = fi.suffix();
    if(!fi.exists())
    {
        QString errorMsgFormat = "Unable to open file:\n\"%1\"\n\nError details: file %1 does not exist.";
        QMessageBox::critical(this, tr("Meshlab Opening Error"), errorMsgFormat.arg(fileName));
        return false;
    }
    if(!fi.isReadable())
    {
        QString errorMsgFormat = "Unable to open file:\n\"%1\"\n\nError details: file %1 is not readable.";
        QMessageBox::critical(this, tr("Meshlab Opening Error"), errorMsgFormat.arg(fileName));
        return false;
    }

    // this change of dir is needed for subsequent textures/materials loading
    QDir::setCurrent(fi.absoluteDir().absolutePath());

    // retrieving corresponding IO plugin
    if (pCurrentIOPlugin == 0)
    {
        QString errorMsgFormat = "Error encountered while opening file:\n\"%1\"\n\nError details: The \"%2\" file extension does not correspond to any supported format.";
        QMessageBox::critical(this, tr("Opening Error"), errorMsgFormat.arg(fileName, extension));
        return false;
    }
    meshDoc()->setBusy(true);
    pCurrentIOPlugin->setLog(&meshDoc()->Log);
    if (!pCurrentIOPlugin->open(extension, fileName, *mm ,mask,*prePar,QCallBack,this /*gla*/))
    {
        QMessageBox::warning(this, tr("Opening Failure"), QString("While opening: '%1'\n\n").arg(fileName)+pCurrentIOPlugin->errorMsg()); // text+
        pCurrentIOPlugin->clearErrorString();
        meshDoc()->setBusy(false);
        return false;
    }
    // After opening the mesh lets ask to the io plugin if this format
    // requires some optional, or userdriven post-opening processing.
    // and in that case ask for the required parameters and then
    // ask to the plugin to perform that processing
    //RichParameterSet par;
    //pCurrentIOPlugin->initOpenParameter(extension, *mm, par);
    //pCurrentIOPlugin->applyOpenParameter(extension, *mm, par);

    QString err = pCurrentIOPlugin->errorMsg();
    if (!err.isEmpty())
    {
        QMessageBox::warning(this, tr("Opening Problems"), QString("While opening: '%1'\n\n").arg(fileName)+pCurrentIOPlugin->errorMsg());
        pCurrentIOPlugin->clearErrorString();
    }


//    saveRecentFileList(fileName);

//    if( mask & vcg::tri::io::Mask::IOM_FACECOLOR)
    GLA()->setColorMode(vcg::GLW::CMPerFace);
//    if( mask & vcg::tri::io::Mask::IOM_VERTCOLOR)
//        GLA()->setColorMode(vcg::GLW::CMPerVert);

//    renderModeTextureWedgeAct->setChecked(false);
    //renderModeTextureWedgeAct->setEnabled(false);
    if(!meshDoc()->mm()->cm.textures.empty())
    {
//        renderModeTextureWedgeAct->setChecked(true);
        //renderModeTextureWedgeAct->setEnabled(true);
        if(vcg::tri::HasPerVertexTexCoord(meshDoc()->mm()->cm) )
            GLA()->setTextureMode(rm,vcg::GLW::TMPerVert);
        if(vcg::tri::HasPerWedgeTexCoord(meshDoc()->mm()->cm) )
            GLA()->setTextureMode(rm,vcg::GLW::TMPerWedgeMulti);
    }

    // In case of polygonal meshes the normal should be updated accordingly
    if( mask & vcg::tri::io::Mask::IOM_BITPOLYGONAL)
    {
        mm->updateDataMask(MeshModel::MM_POLYGONAL); // just to be sure. Hopefully it should be done in the plugin...
        int degNum = vcg::tri::Clean<CMeshO>::RemoveDegenerateFace(mm->cm);
        if(degNum)
            GLA()->Logf(0,"Warning model contains %i degenerate faces. Removed them.",degNum);
        mm->updateDataMask(MeshModel::MM_FACEFACETOPO);
        vcg::tri::UpdateNormal<CMeshO>::PerBitQuadFaceNormalized(mm->cm);
        vcg::tri::UpdateNormal<CMeshO>::PerVertexFromCurrentFaceNormal(mm->cm);
    } // standard case
    else
    {
        vcg::tri::UpdateNormal<CMeshO>::PerFaceNormalized(mm->cm);
        if(!( mask & vcg::tri::io::Mask::IOM_VERTNORMAL) )
            vcg::tri::UpdateNormal<CMeshO>::PerVertexAngleWeighted(mm->cm);
    }
    vcg::tri::UpdateBounding<CMeshO>::Box(mm->cm);					// updates bounding box
    if(mm->cm.fn==0 && mm->cm.en==0){
        GLA()->setDrawMode(rm,vcg::GLW::DMPoints);
        if(!(mask & vcg::tri::io::Mask::IOM_VERTNORMAL))
            GLA()->setLight(false);
        else
            mm->updateDataMask(MeshModel::MM_VERTNORMAL);
    }
    if(mm->cm.fn==0 && mm->cm.en>0){
        GLA()->setDrawMode(rm,vcg::GLW::DMWire);
        if(!(mask & vcg::tri::io::Mask::IOM_VERTNORMAL))
            GLA()->setLight(false);
        else
            mm->updateDataMask(MeshModel::MM_VERTNORMAL);
    }
    else {
        applyColoringFilter(defaultFaceColor);
    }

//    updateMenus();
    int delVertNum = vcg::tri::Clean<CMeshO>::RemoveDegenerateVertex(mm->cm);
    int delFaceNum = vcg::tri::Clean<CMeshO>::RemoveDegenerateFace(mm->cm);

    if(delVertNum>0 || delFaceNum>0 )
        QMessageBox::warning(this, "MeshLab Warning", QString("Warning mesh contains %1 vertices with NAN coords and %2 degenerated faces.\nCorrected.").arg(delVertNum).arg(delFaceNum) );
    meshDoc()->setBusy(false);
    MainWindow::globalStatusBar()->hide(); // Hide progress bar when complete
    return true;
}

void MainWindow::openDICOMFolder() {
    QFileDialog folderChooser;
    folderChooser.setFileMode(QFileDialog::Directory);
    folderChooser.setOption(QFileDialog::ShowDirsOnly);

    QString folderIn = folderChooser.getExistingDirectory(this, tr("Open Folder contains DICOMs"), lastUsedDirectory.absolutePath());

    if (folderIn.isEmpty()) {
        return;
    }

    // To make sure QThread restarts completely
    if (openFolderThread->isRunning()) {
        openFolderThread->quit();
        openFolderThread->wait();
    }

    Dicoms2Ply *mDicoms2Ply = new Dicoms2Ply(folderIn, qApp->applicationDirPath() + "/temp");
    mDicoms2Ply->moveToThread(openFolderThread);
    connect(openFolderThread, SIGNAL(started()), mDicoms2Ply, SLOT(parseDicomsFolder2Ply()));
    connect(openFolderThread, SIGNAL(finished()), mDicoms2Ply, SLOT(deleteLater()));
    connect(mDicoms2Ply, SIGNAL(parsingCompleted()), this, SLOT(setProgressPercentMode()));
    connect(mDicoms2Ply, SIGNAL(importFinishedPointCloud(QString)), this, SLOT(updateImportMesh(QString)));
    setProgressBusyMode();
    openFolderThread->start();
}

bool MainWindow::importMesh(QString fileName)
{
    if (!GLA())
    {
        this->createNewProject();
        if(!GLA())
            return false;
    }


    //QStringList suffixList;
    // HashTable storing all supported formats together with
    // the (1-based) index  of first plugin which is able to open it
    //QHash<QString, MeshIOInterface*> allKnownFormats;
    //PM.LoadFormats(suffixList, allKnownFormats,PluginManager::IMPORT);
    QStringList fileNameList;
    if (fileName.isEmpty())
        fileNameList = QFileDialog::getOpenFileNames(this,tr("Import Mesh"), lastUsedDirectory.path(), PM.inpFilters.join(";;"));
    else
        fileNameList.push_back(fileName);

    if (fileNameList.isEmpty())	return false;
    else
    {
        //save path away so we can use it again
        QString path = fileNameList.first();
        path.truncate(path.lastIndexOf("/"));
        lastUsedDirectory.setPath(path);
    }

    QTime allFileTime;
    allFileTime.start();
    foreach(fileName,fileNameList)
    {
        QFileInfo fi(fileName);
        QString extension = fi.suffix();
        MeshIOInterface *pCurrentIOPlugin = PM.allKnowInputFormats[extension.toLower()];
        //pCurrentIOPlugin->setLog(gla->log);
        if (pCurrentIOPlugin == NULL)
        {
            QString errorMsgFormat("Unable to open file:\n\"%1\"\n\nError details: file format " + extension + " not supported.");
            QMessageBox::critical(this, tr("Meshlab Opening Error"), errorMsgFormat.arg(fileName));
            return false;
        }

        RichParameterSet prePar;
        pCurrentIOPlugin->initPreOpenParameter(extension, fileName,prePar);
//        if(!prePar.isEmpty())
//        {
//            GenericParamDialog preOpenDialog(this, &prePar, tr("Pre-Open Options"));
//            preOpenDialog.setFocus();
//            preOpenDialog.exec();
//        }
        int mask = 0;
        //MeshModel *mm= new MeshModel(gla->meshDoc);
        QFileInfo info(fileName);
        MeshModel *mm=meshDoc()->addNewMesh(qPrintable(fileName),info.fileName());
//        qb->show();
        QTime t;t.start();
        bool open = loadMesh(fileName,pCurrentIOPlugin,mm,mask,&prePar);
        if(open)
        {
            GLA()->Logf(0,"Opened mesh %s in %i msec",qPrintable(fileName),t.elapsed());
            RichParameterSet par;
            pCurrentIOPlugin->initOpenParameter(extension, *mm, par);
            if(!par.isEmpty())
            {
//                GenericParamDialog postOpenDialog(this, &par, tr("Post-Open Processing"));
//                postOpenDialog.setFocus();
//                postOpenDialog.exec();
//                pCurrentIOPlugin->applyOpenParameter(extension, *mm, par);
            }
        }
        else
        {
            meshDoc()->delMesh(mm);
            GLA()->Logf(0,"Warning: Mesh %s has not been opened",qPrintable(fileName));
        }
    }// end foreach file of the input list
    GLA()->Logf(0,"All files opened in %i msec",allFileTime.elapsed());

    this->currentViewContainer()->resetAllTrackBall();
//    qb->reset();
    turnAllLightsOn();
    return true;
}

bool MainWindow::createEmptyMesh(QString name)
{
    if (!GLA()) {
        this->createNewProject();
        if (!GLA()) return false;
    }
    RenderMode rm;
    rm.drawMode = vcg::GLW::DMPoints;
    MeshModel *mm = meshDoc()->addNewMesh("", name, true, rm);
    this->currentViewContainer()->resetAllTrackBall();
    turnAllLightsOn();
    updateLayerDock();
    return true;
}

void MainWindow::updateMenus()
{
    bool activeDoc = (bool) !mdiarea->subWindowList().empty() && mdiarea->currentSubWindow();
    bool notEmptyActiveDoc = activeDoc && !meshDoc()->meshList.empty();
//    importMeshAct->setEnabled(activeDoc);

//    exportMeshAct->setEnabled(notEmptyActiveDoc);
//    exportMeshAsAct->setEnabled(notEmptyActiveDoc);
//    reloadMeshAct->setEnabled(notEmptyActiveDoc);
//    reloadAllMeshAct->setEnabled(notEmptyActiveDoc);
//    importRasterAct->setEnabled(activeDoc);

//    saveProjectAsAct->setEnabled(activeDoc);
//    saveProjectAct->setEnabled(activeDoc);
//    closeProjectAct->setEnabled(activeDoc);

//    saveSnapshotAct->setEnabled(activeDoc);

//    updateRecentFileActions();
//    updateRecentProjActions();
//    filterMenu->setEnabled(!filterMenu->actions().isEmpty());
//    if (!filterMenu->actions().isEmpty())
//        updateSubFiltersMenu(GLA() != NULL,notEmptyActiveDoc);
//    lastFilterAct->setEnabled(false);
//    lastFilterAct->setText(QString("Apply filter"));
//    editMenu->setEnabled(!editMenu->actions().isEmpty());
//    updateMenuItems(editMenu,activeDoc);
//    renderMenu->setEnabled(!editMenu->actions().isEmpty());
//    updateMenuItems(renderMenu,activeDoc);
//    fullScreenAct->setEnabled(activeDoc);
//    trackBallMenu->setEnabled(activeDoc);
//    logMenu->setEnabled(activeDoc);
//    windowsMenu->setEnabled(activeDoc);
//    preferencesMenu->setEnabled(activeDoc);

//    renderToolBar->setEnabled(activeDoc);

//    showToolbarRenderAct->setChecked(renderToolBar->isVisible());
//    showToolbarStandardAct->setChecked(mainToolBar->isVisible());
    if(activeDoc && GLA())
    {
        if(GLA()->getLastAppliedFilter() != NULL)
        {
//            lastFilterAct->setText(QString("Apply filter ") + GLA()->getLastAppliedFilter()->text());
//            lastFilterAct->setEnabled(true);
        }

        // Management of the editing toolbar
        // when you enter in a editing mode you can toggle between editing
        // and camera moving by esc;
        // you exit from editing mode by pressing again the editing button
        // When you are in a editing mode all the other editing are disabled.

        foreach (QAction *a,PM.editActionList)
        {
            a->setChecked(false);
            a->setEnabled( GLA()->getCurrentEditAction() == NULL );
        }

//        suspendEditModeAct->setChecked(GLA()->suspendedEditor);
//        suspendEditModeAct->setDisabled(GLA()->getCurrentEditAction() == NULL);

        if(GLA()->getCurrentEditAction())
        {
            GLA()->getCurrentEditAction()->setChecked(! GLA()->suspendedEditor);
            GLA()->getCurrentEditAction()->setEnabled(true);
        }

//        showInfoPaneAct->setChecked(GLA()->infoAreaVisible);
//        showTrackBallAct->setChecked(GLA()->isTrackBallVisible());
        RenderMode rendtmp;
        if (meshDoc()->meshList.size() > 0)
        {
            QMap<int,RenderMode>::iterator it = GLA()->rendermodemap.find(meshDoc()->meshList[0]->id());
//            if (it == GLA()->rendermodemap.end())
//                throw MeshLabException("Something really bad happened. Mesh id has not been found in rendermodemap.");
            rendtmp = it.value();
        }
//        bool checktext = (rendtmp.textureMode != GLW::TMNone);
        int ii = 0;
        while(ii < meshDoc()->meshList.size())
        {
            if (meshDoc()->meshList[ii] == NULL)
                return;
            QMap<int,RenderMode>::iterator it = GLA()->rendermodemap.find(meshDoc()->meshList[ii]->id());
//            if (it == GLA()->rendermodemap.end())
//                throw MeshLabException("Something really bad happened. Mesh id has not been found in rendermodemap.");
            RenderMode& rm = it.value();
            if (rendtmp.drawMode != rm.drawMode)
                rendtmp.setDrawMode(vcg::GLW::DMNone);

            if (rendtmp.colorMode != rm.colorMode)
                rendtmp.setColorMode(vcg::GLW::CMNone);

//            checktext &= (rm.textureMode != GLW::TMNone);

            rendtmp.setLighting(rendtmp.lighting && rm.lighting);
            rendtmp.setFancyLighting(rendtmp.fancyLighting && rm.fancyLighting);
            rendtmp.setDoubleFaceLighting(rendtmp.doubleSideLighting && rm.doubleSideLighting);
            rendtmp.setBackFaceCull(rendtmp.backFaceCull || rm.backFaceCull);
            rendtmp.setSelectedFaceRendering(rendtmp.selectedFace|| rm.selectedFace);
            rendtmp.setSelectedVertRendering(rendtmp.selectedVert || rm.selectedVert);
            ++ii;
        }

//        foreach(QAction* ac,renderModeGroupAct->actions())
//            ac->setChecked(false);

//        switch (rendtmp.drawMode)
//        {
//        case vcg::GLW::DMBox:				renderBboxAct->setChecked(true);                break;
//        case vcg::GLW::DMPoints:			renderModePointsAct->setChecked(true);      		break;
//        case vcg::GLW::DMWire: 			renderModeWireAct->setChecked(true);      			break;
//        case vcg::GLW::DMFlat:				renderModeFlatAct->setChecked(true);    				break;
//        case vcg::GLW::DMSmooth:			renderModeSmoothAct->setChecked(true);  				break;
//        case vcg::GLW::DMFlatWire:		renderModeFlatLinesAct->setChecked(true);				break;
//        default: break;
//        }

//        foreach(QAction* ac,colorModeGroupAct->actions())
//            ac->setChecked(false);

//        switch (rendtmp.colorMode)
//        {
//        case GLW::CMNone:	colorModeNoneAct->setChecked(true);	      break;
//        case GLW::CMPerMesh:	colorModePerMeshAct->setChecked(true);	      break;
//        case GLW::CMPerVert:	colorModePerVertexAct->setChecked(true);  break;
//        case GLW::CMPerFace:	colorModePerFaceAct->setChecked(true);    break;
//        default: break;
//        }

//        backFaceCullAct->setChecked(rendtmp.backFaceCull);
//        setLightAct->setIcon(rendtmp.lighting ? QIcon(":/images/lighton.png") : QIcon(":/images/lightoff.png") );
//        setLightAct->setChecked(rendtmp.lighting);

//        setFancyLightingAct->setChecked(rendtmp.fancyLighting);
//        setDoubleLightingAct->setChecked(rendtmp.doubleSideLighting);
//        setSelectFaceRenderingAct->setChecked(rendtmp.selectedFace);
//        setSelectVertRenderingAct->setChecked(rendtmp.selectedVert);
//        renderModeTextureWedgeAct->setChecked(checktext);

        // Decorator Menu Checking and unChecking
        // First uncheck and disable all the decorators
        foreach (QAction *a, PM.decoratorActionList)
        {
            a->setChecked(false);
            a->setEnabled(true);
        }
        // Check the decorator per Document of the current glarea
        foreach (QAction *a,   GLA()->iPerDocDecoratorlist)
        { a ->setChecked(true); }

        // Then check the decorator enabled for the current mesh.
        if(GLA()->mm())
            foreach (QAction *a,   GLA()->iCurPerMeshDecoratorList())
            a ->setChecked(true);
    } // if active
    else
    {
        foreach (QAction *a,PM.editActionList)
        {
            a->setEnabled(false);
        }
        foreach (QAction *a,PM.decoratorActionList)
            a->setEnabled(false);

    }

    if(GLA())
    {
//        showLayerDlgAct->setChecked(layerDialog->isVisible());
//        showRasterAct->setChecked(GLA()->isRaster());
//        showRasterAct->setEnabled(meshDoc()->rm() != 0);
        //if(GLA()->layerDialog->isVisible())
//        layerDialog->updateTable();
//        layerDialog->updateLog(meshDoc()->Log);
//        layerDialog->updateDecoratorParsView();
    }
    else
    {
        foreach (QAction *a,PM.decoratorActionList)
        {
            a->setChecked(false);
            a->setEnabled(false);
        }
//        if(layerDialog->isVisible())
//        {
//            layerDialog->updateTable();
//            layerDialog->updateDecoratorParsView();
//        }
    }
//    if (searchMenu != NULL)
//        searchMenu->searchLineWidth() = longestActionWidthInAllMenus();
}

void MainWindow::suspendEditMode()
{
    // return if no window is open
    if(!GLA()) return;

    // return if no editing action is currently ongoing
    if(!GLA()->getCurrentEditAction()) return;

    GLA()->suspendEditToggle();
    updateMenus();
    GLA()->update();
}

void MainWindow::applyEditMode()
{
    if(!GLA()) { //prevents crash without mesh
        QAction *action = qobject_cast<QAction *>(sender());
        action->setChecked(false);
        return;
    }

    QAction *action = qobject_cast<QAction *>(sender());

    if(GLA()->getCurrentEditAction()) //prevents multiple buttons pushed
    {
        if(action==GLA()->getCurrentEditAction()) // We have double pressed the same action and that means disable that actioon
        {
            if(GLA()->suspendedEditor)
            {
                suspendEditMode();
                return;
            }
            GLA()->endEdit();
            updateMenus();
            updateLayerDock();
            return;
        }
        assert(0); // it should be impossible to start an action without having ended the previous one.
        return;
    }

    //if this GLArea does not have an instance of this action's MeshEdit tool then give it one
    if(!GLA()->editorExistsForAction(action))
    {
        MeshEditInterfaceFactory *iEditFactory = qobject_cast<MeshEditInterfaceFactory *>(action->parent());
        MeshEditInterface *iEdit = iEditFactory->getMeshEditInterface(action);
        GLA()->addMeshEditor(action, iEdit);
    }

    GLA()->setCurrentEditAction(action);
    updateMenus();
}

void MainWindow::initToolBar() {
    // Initialize toolbar icons
    mainToolBar = ui->mainToolbar;

    RenderModeLightOnOffAction *renderModeLightAct = new RenderModeLightOnOffAction(this);
    renderModeLightAct->setCheckable(true);
    renderModeLightAct->setChecked(true);
    renderModeLightAct->setIcon(QIcon(":/images/lightOn.png"));
    connect(renderModeLightAct, SIGNAL(triggered()), this, SLOT(updateRenderMode()));
    mainToolBar->addAction(renderModeLightAct);

    // Initialize toolbar
    // Extract Edit Plugins
    foreach(MeshEditInterfaceFactory *iEditFactory,PM.meshEditFactoryPlugins()){
        foreach(QAction* editAction, iEditFactory->actions())
        {
            if(!editAction->icon().isNull()){
                if (editAction->iconText() == "Slice-Tool") {
                    connect(editAction, SIGNAL(triggered()), this, SLOT(applyEditMode()));
                    sliceToolAction = editAction;
                }
            } else qDebug() << "action was null";
        }
    }

    QVector<MeshFilterInterface*> listFilter = PM.meshFilterPlugins(); //TODO remove DEBUG
    foreach(MeshFilterInterface *filter, listFilter) {
        QList<QAction *> actionList = filter->actions();
    }

    // Extract Filter Plugins
    foreach(MeshFilterInterface *filter,PM.meshFilterPlugins()){
        foreach(QAction* filterAction, filter->actions())
        {
//            qDebug() << filterAction->iconText();
//            if(!editAction->icon().isNull()){

                if (filterAction->iconText() == "Surface Reconstruction: Poisson" ) {
                    poissonFilter = filter;

                    poissonToolBar = new QAction(QIcon(":/images/poissonFilter.png"),tr("Surface Reconstruction: Poisson"), this);
                    connect(poissonToolBar, SIGNAL(triggered()), this, SLOT(applyPoissonFilter()));
                    mainToolBar->addAction(this->poissonToolBar);
                } else if (filterAction->iconText() == "Compute normals for point sets") {
                    normalGenerateFilter = filter;
                    normalGenerateToolBar = new QAction(QIcon(":/images/computeNormal.png"),tr("Compute normals for point sets"), this);
                    connect(normalGenerateToolBar, SIGNAL(triggered()), this, SLOT(applyNormalGenerateFilter()));
                    mainToolBar->addAction(this->normalGenerateToolBar);
                } else if (filterAction->iconText() == "Invert Faces Orientation") {
                    invertFaceOrientationFilter = filter;
                    invertFaceOrientationToolBar = new QAction(QIcon(":/images/invertFacesOrientation.png"),tr("Invert Faces Orientation"), this);
                    connect(invertFaceOrientationToolBar, SIGNAL(triggered()), this, SLOT(applyInvertFacesOrientationFilter()));
                    mainToolBar->addAction(invertFaceOrientationToolBar);
                }
//            } else qDebug() << "action was null";
        }
    }

    connect(ui->buttonImportMesh, SIGNAL(triggered()), this, SLOT(onButtonImportMeshClick()));
    connect(ui->butonImportDICOM, SIGNAL(triggered()), this, SLOT(onButonImportDICOMClick()));

    QAction *measurementsAction = new QAction(QIcon(":/images/measurements.png"),tr("Measurements"), this);
    connect(measurementsAction, SIGNAL(triggered()), this, SLOT(onStatisticButtonClick()));
    mainToolBar->addSeparator();
    mainToolBar->addAction(measurementsAction);
}

void MainWindow::initLayerDock() {
    // Inite layer dock
    ui->layersDock->setWindowTitle("Mesh Layer");
    ui->layersDock->setVisible(false); // Hide layer dock as default
    connect(ui->layersDock, SIGNAL(visibilityChanged(bool)), this, SLOT(onLayerDockToggle(bool)));

    QTableWidget *table = ui->layerTable;
    table->setColumnCount(5);
    table->horizontalHeader()->setResizeMode(1, QHeaderView::Stretch);
    table->setColumnWidth(0, 35);
    table->setColumnWidth(2, 35);
    table->setColumnWidth(3, 35);
    table->setColumnWidth(4, 35);
    table->verticalHeader()->setVisible(false);
    table->horizontalHeader()->setVisible(false);
    table->setShowGrid(false);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    connect(table, SIGNAL(cellClicked(int,int)), this, SLOT(onlayerClick(int, int)));
    connect(table, SIGNAL(cellChanged(int,int)), this, SLOT(onEditLayerName(int, int)));

    // Init background slider
    backgroundColorSlider = ui->backgroundColorSlider;

    QPushButton *resetButton = ui->resetButton;
    backgroundColorSlider->setValue(GLAreaSetting::defaultBacktroundColor.red() * 100 / 255);
    connect(backgroundColorSlider, SIGNAL(valueChanged(int)), this, SLOT(onBackgroundChange(int)));
    connect(resetButton, SIGNAL(clicked()), this, SLOT(onResetBackground()));

}

void MainWindow::initMenus() {
    // Initialize render mode action
    renderModeGroupAct = new QActionGroup(this);
    renderModePointsAct = new RenderModePointsAction(renderModeGroupAct);
    renderModeWireAct = new RenderModeWireAction(renderModeGroupAct);
    renderModeFlatAct = new RenderModeFlatAction(renderModeGroupAct);
    renderModeSmoothAct = new RenderModeSmoothAction(renderModeGroupAct);

    ui->menuView->addActions(renderModeGroupAct->actions());

    foreach(QAction* ac, renderModeGroupAct->actions()) {
        connect(ac, SIGNAL(triggered()), this, SLOT(updateRenderMode()));
        // Because these actions are on menu view, we clearout theirs icon
        ac->setIcon(QIcon(""));
        // Then make them checkable
        ac->setCheckable(true);
    }

    foreach(MeshFilterInterface *filter,PM.meshFilterPlugins()){
        foreach(QAction* filterAction, filter->actions())
        {
                if (filterAction->iconText() == "Per Face Color Function" ) {
                    coloringFilter = filter;
                    coloringAction = filterAction;
                }
        }
    }

    foreach(MeshRenderInterface *render, PM.meshRenderPlugins()) {
        foreach(QAction* renderAction, render->actions()) {
            if (renderAction->iconText() == "xray.gdp") {
                xrayShaderAction = renderAction;
                xrayShaderRender = render;
            }
        }
    }

    connect(ui->menuExit, SIGNAL(triggered()), this, SLOT(onExitMenuClick()));
    connect(ui->menuApplyFilter, SIGNAL(triggered()), this, SLOT(onApplyFilterMenuClick()));
    connect(ui->menuGeneratePointCloud, SIGNAL(triggered()), this, SLOT(onMenuGeneratePointCloudClick()));
    connect(ui->menuOpenFile, SIGNAL(triggered()), this, SLOT(onOpenFileMenuClick()));
    connect(ui->menuOpenFolder, SIGNAL(triggered()), this, SLOT(onOpenFolderMenuClick()));
    connect(ui->menuColoringFilter,SIGNAL(triggered()), this, SLOT(onColoringMenuClick()));
    connect(ui->menuViewMeshLayer, SIGNAL(triggered()), this, SLOT(onMenuViewMeshLayerClick()));
    connect(ui->menuXrayShader, SIGNAL(changed()), this, SLOT(onMenuXrayShaderChanged()));

    // Init show trackball menu
    QAction *showTrackBallAct = ui->showTrackBallAct;
    showTrackBallAct->setCheckable(true);
    showTrackBallAct->setChecked(false);
    connect(showTrackBallAct, SIGNAL(triggered()), this, SLOT(showTrackBall()));
    ui->menuViewVolume->setChecked(true);
}

void MainWindow::initProject() {
    // When all initialization done, try to create new project named Empty as default
    createNewProject(QString("Empty"));
    // Hard code the mesh file
    QFile file(QCoreApplication::applicationDirPath() + "/data/bunnyData.ply");
    QString fileName = file.fileName();
    QFile file1(QCoreApplication::applicationDirPath() + "/data/liver_01.ply");
    QString fileName1 = file1.fileName();
    GLA()->showTrackBall(false); // Hide trackball by default

    // Add Volume property
    meshDoc()->setVolumeProperty(ui->volumeProperty);
    connect(ui->volumeProperty, SIGNAL(applyButtonClicked()), this, SLOT(updateGLArea()));
}

void MainWindow::updateGLArea() {
    qDebug() << "updateGLArea";
    GLA()->updatePaintAreaForcefully();
}

void MainWindow::initStatusBar() {
    setStatusBar(new QStatusBar(this));
    globalStatusBar()=statusBar();
    qb=new QProgressBar(this);
    qb->setMaximum(100);
    qb->setMinimum(0);
    qb->reset();
    statusBar()->addPermanentWidget(qb,0);
    globalStatusBar()->hide();
}

void MainWindow::onExitMenuClick()
{
    exit(EXIT_SUCCESS);
}

void MainWindow::showNormalMatrix(cv::Mat mat, QString name) {
    double min, max;
    cv::minMaxLoc(mat, &min, &max);
    cv::Mat tempMat = (mat - min) / (max - min);
    cv::Mat normalImageResult;
    cv::normalize(tempMat , normalImageResult, 0, 255, cv::NORM_MINMAX, CV_8UC1);

    cv::namedWindow(name.toStdString(), cv::WINDOW_AUTOSIZE );// Create a window for display.
    cv::imshow(name.toStdString(), normalImageResult);
}

TreeItem *MainWindow::readNode(QString dataPath, QDomElement element, QModelIndex index, QList<QFile *> *fileList, QList<TreeItem *> *nodeList)
{
    if (element.isNull()) return NULL;
    if (element.tagName() != "TreeNode") return NULL;
    QString name = element.attribute("name", "No name");
    int size = element.attribute("size", "").toInt();
    QString opacityLink = element.attribute("link", "");
    QDomElement transferfuncEl = element.firstChildElement("TransferFunc");
    QString transferType = transferfuncEl.attribute("type", "None");
    QDomElement colorTableEl = element.firstChildElement("ColorTable");
    QString colorPoints = colorTableEl.attribute("colorPoints", "");
    QPair<TreeItem*, QModelIndex>* newNode = treeManager->insertNewNode(name, index);
    TreeItem* node = (*newNode).first;

    node->opacity = new bool[size];
    node->maxIndex = size - 1;

    QFile *opacityFile = new QFile(dataPath + element.attribute("link", ""));
    if (!opacityFile->open(QIODevice::ReadOnly)) {
        return NULL;
    }
    fileList->push_back(opacityFile);
    nodeList->push_back(node);
    // Setting node options
    node->transfunc = defaultTransferfunction;
    TransfuncOptions defaultOptions;
    defaultOptions.type = TransfuncTypes::NONE;
    NodeViewOptions viewOptions;
    viewOptions.colorTable = node->colorTable;
    viewOptions.opacity = node->opacity;
    node->transfuncOptions = defaultOptions;
    node->viewOptions = viewOptions;

    // Build color table
    QString isUsedColorTable = colorTableEl.attribute("isUsed", "false");
    if (isUsedColorTable == "true") {
        QMap<float, vcg::Color4f>* colorDict = new QMap<float, vcg::Color4f>();
        QStringList list = colorPoints.split(" ");
        int number = list.at(0).toInt();
        int pointer = 1;
        for (int i = 0; i < number; ++i) {
            float intensty = list.at(pointer).toFloat();
            int red = list.at(pointer + 1).toFloat() * 255;
            int green = list.at(pointer + 2).toFloat() * 255;
            int blue = list.at(pointer + 3).toFloat() * 255;
            int alpha = list.at(pointer + 4).toFloat() * 255;
            pointer = pointer + 5;
            (*colorDict)[intensty] = vcg::Color4f(red, green, blue, alpha);
        }
        node->colorTable->setColorDict((*colorDict));
        node->viewOptions.isUseColorTable = true;
    } else {
        node->viewOptions.isUseColorTable = false;
    }


    QDomNodeList treeNodeDom = element.childNodes();
    for (int i = treeNodeDom.size() - 1; i >= 0; --i) {
        if (treeNodeDom.at(i).toElement().tagName() == "TreeNode") {
            readNode(dataPath, treeNodeDom.at(i).toElement(), (*newNode).second, fileList, nodeList);
        }
    }

    return node;
}

QString MainWindow::writeOpacity(QString pathData, TreeItem *item, int &level)
{
    QString nodeName = item->data(0).toString();
    QString fileName = "data/" + nodeName + QString::number(level) + ".mbk";
    level++;
    QFile file(pathData + fileName);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    int maxIndex = item->maxIndex;
    out << (qint32) (maxIndex + 1);

    int index = 0;
    int count = 100;
    int step = maxIndex / count;
    if (step == 0) step = 1;
    count = maxIndex / step;

    for (int i = 0; i <= maxIndex; ++i) {
        out << (bool)item->opacity[i];
        if (i % step == 0) {
            QCallBack(100 * index * 1.0/ count, "Writing opacity data");
            index++;
        }
    }
    return fileName; // "data/liver_0.mbk"
}

QString MainWindow::writeIntensity(QString pathData)
{
    QString filename = "data/intensity.mbk";
    QFile file(pathData + filename);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    int maxIndex = meshDoc()->rootNode->maxIndex;
    out << (qint32) (maxIndex + 1);

    int index = 0;
    int count = 100;
    int step = maxIndex / count;
    if (step == 0) step = 1;
    count = maxIndex / step;

    for (int i = 0; i <= maxIndex; ++i) {
        out << (qint16) (meshDoc()->globalTexture3DIntensityData[i]);
        if (i % step == 0) {
            QCallBack(100 * index * 1.0/ count, "Saving project");
            index++;
        }
    }

    out.setVersion(QDataStream::Qt_4_8);
    file.close();
    return filename;
}

void MainWindow::saveProject(QString filename)
{
    if (filename == "") return;
    QFileInfo xmlFile(filename);
    QString parentDirectoryPath = xmlFile.absoluteDir().absolutePath();
    QDir parentDirectory(parentDirectoryPath);
    parentDirectory.mkdir(xmlFile.baseName());
    QDir projectDirectory(parentDirectoryPath + "/" + xmlFile.baseName());
    projectDirectory.mkdir("data");

    // Clear all file in data (if any)
    QString dataPath = projectDirectory.absolutePath() + "/data";
    QDir dir(dataPath);
    dir.setFilter(QDir::Files);
    foreach(QString dirFile, dir.entryList())
    {
        dir.remove(dirFile);
    }

    QString path = parentDirectoryPath + "/" + xmlFile.baseName() + "/";
    QDomDocument document;
    QDomElement project = document.createElement("Project");
    document.appendChild(project);
    project.setAttribute("name", xmlFile.baseName());
    QDomElement intensityData = document.createElement("IntensityData");
    QString link = writeIntensity(path);
    intensityData.setAttribute("link", link);
    QString measureMili = QString::number(meshDoc()->measurementMillimeter.X()) + " "
            + QString::number(meshDoc()->measurementMillimeter.Y()) + " "
            + QString::number(meshDoc()->measurementMillimeter.Z());
    intensityData.setAttribute("measureMili", measureMili);
    QString measurePixel = QString::number(meshDoc()->measurementPixel.X()) + " "
            + QString::number(meshDoc()->measurementPixel.Y()) + " "
            + QString::number(meshDoc()->measurementPixel.Z());
    intensityData.setAttribute("measurePixel", measurePixel);
    project.appendChild(intensityData);

    QDomElement segmentationtree = document.createElement("SegmentationTree");
    int level = 0;
    writeNode(path, document, segmentationtree, meshDoc()->rootNode, level);
    project.appendChild(segmentationtree);

    QFile file(parentDirectoryPath + "/" + xmlFile.baseName() + "/" + xmlFile.baseName() + ".xml");
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(0, "Error!", "Error opening file");
        return;
    }
    QTextStream stream(&file);
    stream << document.toString();
    file.close();
    qDebug() << "Writing is done";
    qb->hide();
    MainWindow::globalStatusBar()->hide();
    meshDoc()->projectPath = parentDirectoryPath + "/" + xmlFile.baseName() + "/" + xmlFile.baseName() + ".xml"; // update project path in meshdocument
}

void MainWindow::writeNode(QString pathData, QDomDocument document, QDomElement parent, TreeItem *item, int &level)
{
    QDomElement treenode = document.createElement("TreeNode");
    QString link = writeOpacity(pathData, item, level);
    level++;
    treenode.setAttribute("link", link);
    treenode.setAttribute("name", item->data(0).toString());
    treenode.setAttribute("size", QString::number(item->maxIndex + 1));

    QDomElement transferfunc = document.createElement("TransferFunc");
    transferfunc.setAttribute("type", "None");
    treenode.appendChild(transferfunc);

    QDomElement colorTable = document.createElement("ColorTable");
    colorTable.setAttribute("colorPoints", item->colorTable->colorDictAttributeString());
    if (item->viewOptions.isUseColorTable) {
        colorTable.setAttribute("isUsed", "true");
    } else {
        colorTable.setAttribute("isUsed", "false");
    }
    treenode.appendChild(colorTable);
    for (int i = 0; i < item->childCount(); ++i) {
        TreeItem* child = item->child(i);
        writeNode(pathData, document, treenode, child, level);
    }
    parent.appendChild(treenode);
}

void MainWindow::onApplyFilterMenuClick()
{/*
    QDomDocument xmlBOM;
    QString homePath = QDir::currentPath();
    QString filename = homePath + "/Project/project.xml";
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Error while loading file";
        return;
    }
    xmlBOM.setContent(&file);
    file.close();
    // Project node
    QDomElement projectEl = xmlBOM.documentElement();
    // Intensity data node
    QDomElement intensityDataEl = projectEl.firstChildElement("IntensityData");
    QStringList measureMili = intensityDataEl.attribute("measureMili", "").split(" ");
    QStringList measurePixel = intensityDataEl.attribute("measurePixel", "").split(" ");
    // SegmentationTree node
    QDomElement segTreeEl = projectEl.firstChildElement("SegmentationTree");

    // Iterate tree nodes
    QDomElement rootNode = segTreeEl.firstChildElement("TreeNode");
    if (!rootNode.isNull()) {
        //Build up segmentation tree
        readNode(rootNode);
    }*/


    //    QFile file("intensity.mbk");
    //    file.open(QIODevice::WriteOnly);
    //    QDataStream out(&file);
    //    int maxIndex = meshDoc()->rootNode->maxIndex;
    //    out << (qint32) (maxIndex + 1);

    //    int index = 0;
    //    int count = 100;
    //    int step = maxIndex / count;
    //    if (step == 0) step = 1;
    //    count = maxIndex / step;

    //    for (int i = 0; i <= maxIndex; ++i) {
    //        out << (qint16) (meshDoc()->globalTexture3DIntensityData[i]);
    //        if (i % step == 0) {
    //            QCallBack(100 * index * 1.0/ count, "Saving project");
    //            index++;
    //        }
    //    }

    //    out.setVersion(QDataStream::Qt_4_8);
    //    file.close();
}

void MainWindow::onMenuGeneratePointCloudClick() {
    QList<Point3i> topLeftBoundaryPoints = detectBoundaryPoints(currentTreeItem, DetectBoundary::StartDirection::TOP_LEFT);
    QList<Point3i> rightTopBoundaryPoints = detectBoundaryPoints(currentTreeItem, DetectBoundary::StartDirection::RIGHT_TOP);
    QList<Point3i> bottomRightBoundaryPoints = detectBoundaryPoints(currentTreeItem, DetectBoundary::StartDirection::BOTTOM_RIGHT);
    QList<Point3i> leftBottomBoundaryPoints = detectBoundaryPoints(currentTreeItem, DetectBoundary::StartDirection::LEFT_BOTTOM);

    int width = meshDoc()->measurementPixel.X();
    int height = meshDoc()->measurementPixel.Y();
    int depth = meshDoc()->measurementPixel.Z();
    float spaceX = meshDoc()->measurementMillimeter.X() / width;
    float spaceY = meshDoc()->measurementMillimeter.Y() / height;
    float spaceZ = meshDoc()->measurementMillimeter.Z() / (depth - 1);

    int size = width*height*depth;
    bool *selected =  new bool[size];
    for (int idx = 0; idx < size; idx++) {
        selected[idx] = false;
    }

    QList<Point3f> finalPointCloud;
//    QList<Point3f> tlPointCloud;
    for (int i = 0; i < topLeftBoundaryPoints.size(); i++) {
        int x = topLeftBoundaryPoints.at(i).X();
        int y = topLeftBoundaryPoints.at(i).Y();
        int z = topLeftBoundaryPoints.at(i).Z();

//        tlPointCloud.push_back(Point3f(x*1.0*spaceX, y*1.0*spaceY, z*1.0*spaceZ));

        if (!selected[z*width*height + y*width + x]) {
            selected[z*width*height + y*width + x] = true;
            finalPointCloud.push_back(Point3f(x*1.0*spaceX, y*1.0*spaceY, z*1.0*spaceZ));
        }
    }

//    QList<Point3f> rtPointCloud;
    for (int i = 0; i < rightTopBoundaryPoints.size(); i++) {
        int x = rightTopBoundaryPoints.at(i).X();
        int y = rightTopBoundaryPoints.at(i).Y();
        int z = rightTopBoundaryPoints.at(i).Z();

//        rtPointCloud.push_back(Point3f(x*1.0*spaceX, y*1.0*spaceY, z*1.0*spaceZ));

        if (!selected[z*width*height + y*width + x]) {
            selected[z*width*height + y*width + x] = true;
            finalPointCloud.push_back(Point3f(x*1.0*spaceX, y*1.0*spaceY, z*1.0*spaceZ));
        }
    }

//    QList<Point3f> brPointCloud;
    for (int i = 0; i < bottomRightBoundaryPoints.size(); i++) {
        int x = bottomRightBoundaryPoints.at(i).X();
        int y = bottomRightBoundaryPoints.at(i).Y();
        int z = bottomRightBoundaryPoints.at(i).Z();

//        brPointCloud.push_back(Point3f(x*1.0*spaceX, y*1.0*spaceY, z*1.0*spaceZ));

        if (!selected[z*width*height + y*width + x]) {
            selected[z*width*height + y*width + x] = true;
            finalPointCloud.push_back(Point3f(x*1.0*spaceX, y*1.0*spaceY, z*1.0*spaceZ));
        }
    }

//    QList<Point3f> lbPointCloud;
    for (int i = 0; i < leftBottomBoundaryPoints.size(); i++) {
        int x = leftBottomBoundaryPoints.at(i).X();
        int y = leftBottomBoundaryPoints.at(i).Y();
        int z = leftBottomBoundaryPoints.at(i).Z();

//        lbPointCloud.push_back(Point3f(x*1.0*spaceX, y*1.0*spaceY, z*1.0*spaceZ));

        if (!selected[z*width*height + y*width + x]) {
            selected[z*width*height + y*width + x] = true;
            finalPointCloud.push_back(Point3f(x*1.0*spaceX, y*1.0*spaceY, z*1.0*spaceZ));
        }
    }

//    importMesh("Top-Left Point Cloud", tlPointCloud);
//    importMesh("Right-Top Point Cloud", rtPointCloud);
//    importMesh("Bottom-Right Point Cloud", brPointCloud);
//    importMesh("Left-Bottom Point Cloud", lbPointCloud);
    importMesh("Composed Point Cloud", finalPointCloud);

    if (selected != NULL) {
        delete [] selected;
        selected = NULL;
    }
}

void MainWindow::onMenuBoundaryPointClick()
{
}

QList<Point3f> MainWindow::detectBoundaryPoints(TreeItem *item){
    int width = meshDoc()->measurementPixel.X();
    int height = meshDoc()->measurementPixel.Y();
    int depth = meshDoc()->measurementPixel.Z();
    float spaceX = meshDoc()->measurementMillimeter.X() / width;
    float spaceY = meshDoc()->measurementMillimeter.Y() / height;
    float spaceZ = meshDoc()->measurementMillimeter.Z() / (depth - 1);

    int aHeight = height + 2;
    int aWidth = width + 2;
    uchar** addBorderSlice = new uchar*[aHeight];
    for (int i = 0; i < aHeight; i++) {
        addBorderSlice[i] = new uchar[aWidth];
        for (int j = 0; j < aWidth; j++) {
            addBorderSlice[i][j] = 0;
        }
    }


    QList<Point3f> pointcloud;
    DetectBoundary detectBoundary;
    for (int i = 0; i < depth; i++) {

        // Initialize slice's value
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                uchar opacity = (uchar) item->opacity[i*height*width + y*width + x];
                addBorderSlice[y+1][x+1] = opacity;
            }
        }

        // Get boundary points of each slice
        Point2i uperLeftPoint = detectBoundary.findUperLeftPoint(addBorderSlice, aWidth, aHeight);
        QList<Point2i> boundaryPoints = detectBoundary.findBoundaryPoints(uperLeftPoint, addBorderSlice, aWidth, aHeight);

        // Append to list of point cloud
        for (int idx = 0; idx < boundaryPoints.size(); idx++) {
            pointcloud.push_back(Point3f((boundaryPoints.at(idx).X()-1) * 1.0 * spaceX,
                                         (boundaryPoints.at(idx).Y()-1) * 1.0 * spaceY,
                                         i * 1.0 * spaceZ));
        }
    }

    for (int i = 0; i < aHeight; i++) {
        if (addBorderSlice[i] != NULL) {
            delete [] addBorderSlice[i];
            addBorderSlice[i] = NULL;
        }
    }

    if (addBorderSlice != NULL) {
        delete [] addBorderSlice;
        addBorderSlice = NULL;
    }

    return pointcloud;
}

QList<Point3i> MainWindow::detectBoundaryPoints(TreeItem *item, DetectBoundary::StartDirection direction)
{
    int width = meshDoc()->measurementPixel.X();
    int height = meshDoc()->measurementPixel.Y();
    int depth = meshDoc()->measurementPixel.Z();

    int aHeight = height + 2;
    int aWidth = width + 2;
    uchar** addBorderSlice = new uchar*[aHeight];
    for (int i = 0; i < aHeight; i++) {
        addBorderSlice[i] = new uchar[aWidth];
        for (int j = 0; j < aWidth; j++) {
            addBorderSlice[i][j] = 0;
        }
    }


    QList<Point3i> pointcloud;
    DetectBoundary detectBoundary;
    for (int i = 0; i < depth; i++) {

        // Initialize slice's value
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                uchar opacity = (uchar) item->opacity[i*height*width + y*width + x];
                addBorderSlice[y+1][x+1] = opacity;
            }
        }

        // Get boundary points of each slice
        QList<Point2i> boundaryPoints;
        if (direction == DetectBoundary::StartDirection::RIGHT_TOP) {
            Point2i rightTopPoint = detectBoundary.findRightTopPoint(addBorderSlice, aWidth, aHeight);
            boundaryPoints = detectBoundary.findBoundaryPoints(rightTopPoint, DetectBoundary::StartDirection::RIGHT_TOP, addBorderSlice, aWidth, aHeight);
        } else if (direction == DetectBoundary::StartDirection::BOTTOM_RIGHT) {
            Point2i bottomRightPoint = detectBoundary.findBottomRightPoint(addBorderSlice, aWidth, aHeight);
            boundaryPoints = detectBoundary.findBoundaryPoints(bottomRightPoint, DetectBoundary::StartDirection::BOTTOM_RIGHT, addBorderSlice, aWidth, aHeight);
        } else if (direction == DetectBoundary::StartDirection::LEFT_BOTTOM) {
            Point2i leftBottomPoint = detectBoundary.findLeftBottomPoint(addBorderSlice, aWidth, aHeight);
            boundaryPoints = detectBoundary.findBoundaryPoints(leftBottomPoint, DetectBoundary::StartDirection::LEFT_BOTTOM, addBorderSlice, aWidth, aHeight);
        } else {
            Point2i topLeftPoint = detectBoundary.findTopLeftPoint(addBorderSlice, aWidth, aHeight);
            boundaryPoints = detectBoundary.findBoundaryPoints(topLeftPoint, DetectBoundary::StartDirection::TOP_LEFT, addBorderSlice, aWidth, aHeight);

        }
        // Append to list of point cloud
        for (int idx = 0; idx < boundaryPoints.size(); idx++) {
            pointcloud.push_back(Point3i(boundaryPoints.at(idx).X()-1,
                                         boundaryPoints.at(idx).Y()-1,
                                         i));
        }
    }

    for (int i = 0; i < aHeight; i++) {
        if (addBorderSlice[i] != NULL) {
            delete [] addBorderSlice[i];
            addBorderSlice[i] = NULL;
        }
    }

    if (addBorderSlice != NULL) {
        delete [] addBorderSlice;
        addBorderSlice = NULL;
    }

    return pointcloud;
}

QList<Point3f> MainWindow::applyPCA(QList<Point3f> boundaryPoints, int dim) {

    dim = 2;

    int pointNum = boundaryPoints.size();

    cv::Mat X(3, pointNum, CV_64F);
    for (int idx = 0; idx < pointNum; idx++) {
        X.at<double>(0, idx) = boundaryPoints.at(idx).X();
        X.at<double>(1, idx) = boundaryPoints.at(idx).Y();
        X.at<double>(2, idx) = boundaryPoints.at(idx).Z();
    }

    cv::PCA pca(X, cv::noArray(), CV_PCA_DATA_AS_COL, dim);

    cv::Mat pcPoints = pca.project(X);
    cv::Mat X1 = pca.backProject(pcPoints);
    QList<Point3f> pcaBackProjectPoints;
    for (int idx = 0; idx < pointNum; idx++) {
        float x = static_cast<float>(X1.at<double>(0, idx));
        float y = static_cast<float>(X1.at<double>(1, idx));
        float z = static_cast<float>(X1.at<double>(2, idx));

        pcaBackProjectPoints.push_back(Point3f(x, y, z));
    }

//    cv::Mat bpcSample = X1(cv::Rect(100, 0, 1, 3)).clone();

//    cv::Mat eigenVectors = pca.eigenvectors;
//    cv::Mat mean = pca.mean;
//    cv::Mat matrixA = eigenVectors(cv::Rect(0, 0, 3, 2)).clone();
//    cv::Mat matrixAT;
//    cv::transpose(matrixA, matrixAT);

//    cv::Mat sample = X(cv::Rect(100, 0, 1, 3)).clone();
//    cv::Mat subMatrix = sample - mean;
//    cv::Mat ySample = matrixA * subMatrix;

//    cv::Mat reconstructX = matrixAT* ySample + mean;

//    std::cout << "Reconstruct X:" << std::endl;
//    printMatrix(reconstructX);

//    std::cout << "Lib Reconstruct X:" << std::endl;
//    printMatrix(bpcSample);

    return pcaBackProjectPoints;
}

void MainWindow::printMatrix(cv::Mat matrix)
{
    for (int y = 0; y < matrix.rows; y++) {
        for (int x = 0; x < matrix.cols; x++) {
            std::cout << matrix.at<double>(y, x) << " ";
        }
        std::cout << std::endl;
    }
}

Point3i MainWindow::paddingIndex(Point3i coord, int width, int height, int numSlice)
{
    int x = coord.X(), y = coord.Y(), z = coord.Z();
    if (x < 0) x = 0; else if (x >= width) x = width - 1;
    if (y < 0) y = 0; else if (y >= height) y = height - 1;
    if (z < 0) z = 0; else if (z >= numSlice) z = numSlice - 1;
    return vcg::Point3i(x, y, z);
}

void MainWindow::onOpenFileMenuClick()
{
//    QMessageBox::information(this, tr("Information"), tr("This feature will be available soon! Come back later"));
    importMesh("");
    updateLayerDock();
}

void MainWindow::onOpenFolderMenuClick()
{
    openDICOMFolder();
}

void MainWindow::onColoringMenuClick() {
    backupCurrentFaceColor();
    colorDialog->setVisible(true);
}

void MainWindow::onButonImportDICOMClick()
{
    openDICOMFolder();
}

void MainWindow::updateImportMesh(QString file) {
    qDebug() << "updateImportMesh";
    importMesh(file);
    updateLayerDock();
}

void MainWindow::setProgressBusyMode() {
    MainWindow::globalStatusBar()->show();
    MainWindow::globalStatusBar()->showMessage("Parsing DICOMs to point cloud");
    qb->setMaximum(0);
    qb->setMinimum(0);
}

void MainWindow::setProgressPercentMode() {
    qb->setMaximum(100);
    qb->setMinimum(0);
    MainWindow::globalStatusBar()->hide();
}

void MainWindow::onButtonImportMeshClick()
{
    importMesh("");
    updateLayerDock();
}

void MainWindow::updateRenderMode( )
{
    if ((GLA() == NULL) || (meshDoc() == NULL))
        return;
    QMap<int,RenderMode>& rmode = GLA()->rendermodemap;

    RenderModeAction* act = qobject_cast<RenderModeAction*>(sender());
    RenderModeTexturePerVertAction* textact = qobject_cast<RenderModeTexturePerVertAction*>(act);

    //act->data contains the meshid to which the action is referred.
    //if the meshid is -1 the action is intended to be per-document and not per mesh
    bool isvalidid = true;
    int meshid = act->data().toInt(&isvalidid);
    if (!isvalidid)
        throw MeshLabException("A RenderModeAction contains a non-integer data id.");

    if (meshid == -1)
    {
        for(QMap<int,RenderMode>::iterator it =	rmode.begin();it != rmode.end();++it)
        {
            act->updateRenderMode(it.value());
            //horrible trick caused by MeshLab GUI. In MeshLab exists just a button turning on/off the texture visualization.
            //Unfortunately the RenderMode::textureMode member field is not just a boolean value but and enum one.
            //The enum-value depends from the enabled attributes of input mesh.
            if (textact != NULL)
                setBestTextureModePerMesh(textact,it.key(),it.value());
        }
    }
    else
    {
        QMap<int,RenderMode>::iterator it = rmode.find(meshid);
        if (it == rmode.end())
            throw MeshLabException("A RenderModeAction contains a non-valid data meshid.");
        act->updateRenderMode(it.value());
        updateMenus();
        //horrible trick caused by MeshLab GUI. In MeshLab exists just a button turning on/off the texture visualization.
        //Unfortunately the RenderMode::textureMode member field is not just a boolean value but and enum one.
        //The enum-value depends from the enabled attributes of input mesh.
        if (textact != NULL)
            setBestTextureModePerMesh(textact,meshid,it.value());
    }
    GLA()->update();
}

vcg::GLW::TextureMode MainWindow::getBestTextureRenderModePerMesh(const int meshid)
{
    MeshModel* mesh = NULL;
    if ((meshDoc() == NULL) || ((mesh  = meshDoc()->getMesh(meshid)) == NULL))
        return vcg::GLW::TMNone;

    if (mesh->hasDataMask(MeshModel::MM_WEDGTEXCOORD))
        return vcg::GLW::TMPerWedgeMulti;

    if (mesh->hasDataMask(MeshModel::MM_VERTTEXCOORD))
        return vcg::GLW::TMPerVert;

    return vcg::GLW::TMNone;
}

void MainWindow::setBestTextureModePerMesh(RenderModeAction* textact,const int meshid, RenderMode& rm)
{
    if ((textact == NULL) || !textact->isChecked())
        rm.setTextureMode(vcg::GLW::TMNone);
    else
    {
        vcg::GLW::TextureMode texmode = getBestTextureRenderModePerMesh(meshid);
        rm.setTextureMode(texmode);
    }
}

void MainWindow::updateLayerDock() {
    QSignalMapper* layerMapper = new QSignalMapper(this);
    QTableWidget *table = ui->layerTable;
    MeshModel *currentMesh = meshDoc()->mm();

    foreach (MeshModel * mesh, this->meshDoc()->meshList) {
        int meshIndex = meshDoc()->meshList.indexOf(mesh); // We will use meshIndex as index of each row
        // Setting visibility button
        QPushButton *visibilityButton = new QPushButton();
        if (mesh->visible) {
            visibilityButton->setIcon(QIcon(":/images/vi.png"));
            visibilityButton->setToolTip("Hide layer");

        } else {
            visibilityButton->setIcon(QIcon(":/images/invi.png"));
            visibilityButton->setToolTip("Show layer");
        }
        visibilityButton->setIconSize(QSize(24, 24));
        connect(visibilityButton, SIGNAL(clicked(bool)), layerMapper, SLOT(map()));
        layerMapper->setMapping(visibilityButton, meshIndex % 100);// index for this event ranges from 0->99

        // Setting delete button
        QPushButton *deleteButton = new QPushButton();
        deleteButton->setToolTip("Delete layer");
        deleteButton->setIconSize(QSize(24, 24));
        deleteButton->setIcon(QIcon(":/images/delete.png"));
        connect(deleteButton, SIGNAL(clicked(bool)), layerMapper, SLOT(map()));
        layerMapper->setMapping(deleteButton, (meshIndex % 100 + 100));// index for this event ranges from 100 -> 199

        // Coloring
        QPushButton *colorButton = new QPushButton();
        colorButton->setToolTip("Change mesh color");
        colorButton->setIconSize(QSize(24, 24));
        colorButton->setIcon(QIcon(":/images/colorPalette.png"));
        if (mesh->cm.fn == 0) { // color  button only available for mesh has faces
            colorButton->setDisabled(true);
        }
        connect(colorButton, SIGNAL(clicked(bool)), layerMapper, SLOT(map()));
        layerMapper->setMapping(colorButton, (meshIndex % 100 + 200));// index for this event ranges from 200 -> 299

        // Setting volume rendering button
        QPushButton *volumeButton = new QPushButton();
        if (mesh->isVisibleVolume) {
            // if volume is rendering then set icon to hide volume
            volumeButton->setToolTip("Hide volume");
            volumeButton->setIcon(QIcon(":/images/volumeHide.png"));
        } else {
            volumeButton->setToolTip("Show volume");
            volumeButton->setIcon(QIcon(":/images/volumeShow.png"));
        }
        volumeButton->setEnabled(mesh->texture3DRawData != NULL);
        volumeButton->setIconSize(QSize(24, 24));
        connect(volumeButton, SIGNAL(clicked(bool)), layerMapper, SLOT(map()));
        layerMapper->setMapping(volumeButton, (meshIndex % 100 + 300));// index for this event ranges from 300 -> 399



        table->setRowCount(meshIndex + 1);
        table->setCellWidget(meshIndex, 0, visibilityButton);

        // Give layer a new label if it do not has one
        if (mesh->label() == "") {
            mesh->setLabel("* new layer " + QString::number(mesh->id()));
        }
        QTableWidgetItem *layerName = new QTableWidgetItem(mesh->label());

        layerName->setToolTip("Double click to edit");
        table->setItem(meshIndex, 1, layerName);
        table->setCellWidget(meshIndex, 2, colorButton);
        table->setCellWidget(meshIndex, 3, deleteButton);
        table->setCellWidget(meshIndex, 4, volumeButton);
    }

    connect(layerMapper, SIGNAL(mapped(int)), this, SLOT(mapLayerEvents(int)));
    // Set current selected layer
    table->selectRow(meshDoc()->meshList.indexOf(currentMesh));
}

void MainWindow::toggleLayerVisibility(int meshIndex) {
    MainWindow* mw = qobject_cast<MainWindow*>(this);
    MeshModel *md = meshDoc()->meshList.at(meshIndex);
    QPushButton *button = qobject_cast<QPushButton*> (ui->layerTable->cellWidget(meshIndex, 0));
    // Toggle visibility and icon of button too
    if (md->visible) {
        mw->GLA()->meshSetVisibility(md, false);
        button->setIcon(QIcon(":/images/invi.png"));
        button->setToolTip("Show layer");
    } else {
        mw->GLA()->meshSetVisibility(md, true);
        button->setIcon(QIcon(":/images/vi.png"));
        button->setToolTip("Hide layer");
    }
    meshDoc()->setCurrentMesh(meshDoc()->meshList.at(meshIndex)->id());
    GLA()->update();
}

void MainWindow::toggleVolumeVisibility(int meshIndex) {
    MeshModel *md = meshDoc()->meshList.at(meshIndex);
    if (md->isVisibleVolume) {
        meshDoc()->removeOneVolumeFromGlobalVolume(md, QCallBack);
    } else {
        meshDoc()->addOneVolumeToGlobalVolume(md, QCallBack);
    }
    qb->hide();
}

void MainWindow::onlayerClick(int row, int col) {
//    QTableWidgetItem *item = ui->layerTable->selectedItems()[0];
    // TODO recheck this condition
//    if (item->column() == 1) { // Only change current mesh if user click on layer's name (column 1)
        meshDoc()->setCurrentMesh(meshDoc()->meshList.at(row)->id());
//    }
    GLA()->update();
}

void MainWindow::deleteLayer(int meshIndex) {
    qDebug() << meshIndex;
    MeshModel *md = meshDoc()->meshList.at(meshIndex);
    meshDoc()->delMesh(md);
    ui->layerTable->reset(); // If we don't execute this line, "out of index" error will shown out
    ui->layerTable->removeRow(meshIndex);
    updateLayerDock();
    GLA()->update();
}

void MainWindow::onMenuViewMeshLayerClick()
{
    bool isShown = ui->layersDock->isVisible();
    ui->layersDock->setVisible(!isShown);
    if (ui->layersDock->isVisible()) {
        QMainWindow::tabifyDockWidget(ui->volumePropertyDock, ui->layersDock);
        ui->layersDock->raise();
    }
    GLA()->update();
}

void MainWindow::onLayerDockToggle(bool isShown) {
    ui->menuViewMeshLayer->setChecked(isShown);
}

void MainWindow::onVolumePropertyDockToggle(bool isShown)
{
    ui->menuViewVolumeProperty->setChecked(isShown);
}

void MainWindow::onTreeManagerDockToggle(bool isShown)
{
    ui->menuViewTreeManager->setChecked(isShown);
}

void MainWindow::onSliceDockToggle(bool isShown)
{
    ui->menuViewSliceView->setChecked(isShown);
}

void MainWindow::onEditLayerName(int row, int column) {
    MeshModel *md = meshDoc()->meshList.at(row);
    QString label = ui->layerTable->item(row, column)->text();
    md->setLabel(label);
}

void MainWindow::onStatisticButtonClick() {
    if (meshDoc()->meshList.length() == 0) return;
    qDebug() << "Show statistic";
    CMeshO &m=meshDoc()->mm()->cm;
    vcg::tri::Inertia<CMeshO> I(m);
    float Area = vcg::tri::Stat<CMeshO>::ComputeMeshArea(m);
    float Volume = I.Mass();
    QMessageBox::information(this, "Measurements: " + meshDoc()->mm()->label(), "Area: \t" + QString::number((int)Area) + "\t(mm2)\n Volume: \t" + QString::number((int)Volume) + "\t(mm3)");
}

void MainWindow::cancelColoringFilter() {
    restorePreviousFaceColor();
}

void MainWindow::backupCurrentFaceColor() {
    // Change to color mode
//    GLA()->setColorMode(vcg::GLW::CMPerFace);
//    meshDoc()->mm()->updateDataMask(MeshModel::MM_FACECOLOR);

    CMeshO &m = meshDoc()->mm()->cm;
    if (m.fn > 0) {
        CMeshO::FaceIterator fi = m.face.begin();
        vcg::Color4b color = (*fi).C();
        qDebug() << "cancelColoringFilter RGB" << color[0] << " " << color[1] << " " << color[2] << " " << color[3];
        backupColor.setRgb(color[0], color[1], color[2], color[3]);
    }
}

void MainWindow::restorePreviousFaceColor() {
    applyColoringFilter(backupColor);
}

void MainWindow::mapLayerEvents(int index) {
    int meshIndex = index % 100;
    // Update current mesh
    meshDoc()->setCurrentMesh(meshDoc()->meshList.at(meshIndex)->id());
    if (index < 100) { // Visibility event
        qDebug() << "Toggle visibility of layer " + QString::number(meshIndex);
        toggleLayerVisibility(meshIndex);
    } else if (index < 200) { // Delete event
        qDebug() << "Delete layer " + QString::number(meshIndex);
        deleteLayer(meshIndex);
    } else if (index < 300) { // Color event
        onColoringMenuClick();
        qDebug() << "Color layer " + QString::number(meshIndex);
    } else if (index < 400) {
        qDebug() << "Toggle visibility of volume " + QString::number(meshIndex);
        toggleVolumeVisibility(meshIndex);
    } else {
        qDebug() << "Unknown event on layerdock" + QString::number(index);
    }
    ui->layerTable->selectRow(meshIndex);
}

void MainWindow::onMenuXrayShaderChanged() {
    if (ui->menuXrayShader->isChecked()) {
        applyXrayShaderRender();
    } else {
        resetRender();
    }
}

void MainWindow::applyXrayShaderRender() {
    if ((GLA() != NULL) && (GLA()->getRenderer() != NULL)) {
        GLA()->getRenderer()->Finalize(GLA()->getCurrentShaderAction(), meshDoc(), GLA());
        GLA()->setRenderer(NULL, NULL);
    }

    bool initsupport = false;
    if (xrayShaderRender != NULL) {
        xrayShaderRender->Init(xrayShaderAction, (*meshDoc()), GLA()->rendermodemap, GLA());
        initsupport = xrayShaderRender->isSupported();
        if (initsupport) {
            GLA()->setRenderer(xrayShaderRender, xrayShaderAction);
        } else {
            qDebug() << "The selected shader is not supported!";
            xrayShaderRender->Finalize(xrayShaderAction, meshDoc(), GLA());
        }
    }

    GLA()->update();
}

void MainWindow::resetRender() {
    if ((GLA() != NULL) && (GLA()->getRenderer() != NULL)) {
        GLA()->getRenderer()->Finalize(GLA()->getCurrentShaderAction(), meshDoc(), GLA());
        GLA()->setRenderer(NULL, NULL);
    }

    GLA()->update();
}

void MainWindow::startTransferFunc(TransfuncTypes transfuncType, TreeItem *, TreeItem *, TreeItem *)
{
    switch (transfuncType) {
    case TransfuncTypes::PROBABILITY_MODEL:
    case TransfuncTypes::HISTOGRAM:{
        ui->volumePropertyDock->raise();
        ui->volumePropertyDock->setVisible(true);
        break;
    }
    case TransfuncTypes::REGION_GROWING: {
        QMainWindow::tabifyDockWidget(ui->volumePropertyDock, ui->sliceDock);
        ui->sliceDock->setVisible(true);
        ui->sliceDock->raise();
        if (!sliceToolAction->isChecked()) {
            sliceToolAction->trigger();
            ui->actionSlice_Tool->setChecked(true);
        }
        break;
    }
    case TransfuncTypes::HISTOGRAM2D: {
        QMainWindow::tabifyDockWidget(ui->volumePropertyDock, ui->histogram2DDock);
        ui->histogram2DDock->setVisible(true);
        ui->histogram2DDock->raise();
    break;
    }
    case TransfuncTypes::MEDIAN_FILTER: {
        ui->volumePropertyDock->raise();
        ui->volumePropertyDock->setVisible(true);
        break;
    }
    case TransfuncTypes::SPACE_SELECTION: {
        QMainWindow::tabifyDockWidget(ui->volumePropertyDock, ui->spaceSelectionDock);
        ui->spaceSelectionDock->setVisible(true);
        ui->spaceSelectionDock->raise();
        break;
    }
    case TransfuncTypes::OPACITY_FILE: {
        QMainWindow::tabifyDockWidget(ui->volumePropertyDock, ui->opacityFromFileDock);
        ui->opacityFromFileDock->setVisible(true);
        ui->opacityFromFileDock->raise();
        break;
    }
    case TransfuncTypes::OPACITY_MASK: {
        QMainWindow::tabifyDockWidget(ui->volumePropertyDock, ui->opacityFromMaskDock);
        ui->opacityFromMaskDock->setVisible(true);
        ui->opacityFromMaskDock->raise();
        break;
    }
    default:
        break;
    }
}

void MainWindow::endTransferFunc(TransfuncTypes transfuncType)
{
    switch (transfuncType) {
    case TransfuncTypes::PROBABILITY_MODEL:
    case TransfuncTypes::HISTOGRAM:{
//        ui->volumePropertyDock->setVisible(false);
        break;
    }
    case TransfuncTypes::REGION_GROWING: {
        ui->sliceDock->setVisible(false);
        sliceToolAction->trigger();
        ui->actionSlice_Tool->setChecked(sliceToolAction->isChecked());
        meshDoc()->sliceMesh = NULL;
        break;
    }
    default:
        break;
    }
}

void MainWindow::turnAllLightsOn() {
    QMap<int, RenderMode> &rmode = GLA()->rendermodemap;
    foreach (MeshModel* mesh, meshDoc()->meshList) {
        QMap<int, RenderMode>::iterator it = rmode.find(mesh->id());
        it.value().setLighting(true);
    }
}

void MainWindow::onBackgroundChange(int value) {
//    qDebug() << value;
    // TODO change background here
    int newRed = value * 255 / 100;
    if (value == 100) newRed = 255;
//    int newGreen = GLAreaSetting::defaultBacktroundColor.green() * newRed / GLAreaSetting::defaultBacktroundColor.red();
//    int newBlue = GLAreaSetting::defaultBacktroundColor.blue() * newRed / GLAreaSetting::defaultBacktroundColor.red();
    setBackgroundColor(QColor(newRed, newRed, newRed));
}

void MainWindow::setBackgroundColor(QColor color) {
    RichParameterSet rps;
    GLA()->glas.initGlobalParameterSet(&rps);
    rps.removeParameter(GLA()->glas.backgroundBotColorParam());
    rps.addParam(new RichColor(GLA()->glas.backgroundBotColorParam(), color,"MeshLab Bottom BackGround Color","MeshLab GLarea's BackGround Color(bottom corner)"));
    rps.removeParameter(GLA()->glas.backgroundTopColorParam());
    rps.addParam(new RichColor(GLA()->glas.backgroundTopColorParam(), color,"MeshLab Top BackGround Color","MeshLab GLarea's BackGround Color(top corner)"));
    GLA()->glas.updateGlobalParameterSet(rps);
    GLA()->update();
}

void MainWindow::initVolumePropertyDock()
{
    connect(ui->volumePropertyDock, SIGNAL(visibilityChanged(bool)), this, SLOT(onVolumePropertyDockToggle(bool)));
    ui->volumePropertyDock->setVisible(true);
}

void MainWindow::onResetBackground() {
    setBackgroundColor(GLAreaSetting::defaultBacktroundColor);
    backgroundColorSlider->setValue(GLAreaSetting::defaultBacktroundColor.red() * 100 / 255);
}

void MainWindow::showTrackBall() {
    if (GLA() != 0) {
        GLA()->showTrackBall(!GLA()->isTrackBallVisible());
    }
}

void MainWindow::on_menuViewAxis_triggered(bool checked)
{
    if (checked) {
//        vcg::CoordinateFrame(meshDoc()->bbox().Diag() / 2.0).Render(GLA(), );
        //TODO
    }

}

void MainWindow::on_actionImport_DICOM_triggered()
{
    ImportDICOMDialog *importDialog = new ImportDICOMDialog(this);
    importDialog->show();
}

void MainWindow::on_menuViewVolumeProperty_triggered(bool checked)
{
    ui->volumePropertyDock->setVisible(checked);
    ui->volumePropertyDock->raise();
}

void MainWindow::on_menuViewVolume_triggered(bool checked)
{
    this->meshDoc()->isRenderVolume = checked;
    GLA()->update();
}

void MainWindow::on_menuViewTreeManager_triggered(bool checked)
{
    ui->treeDock->setVisible(checked);
    ui->treeDock->raise();
}

void MainWindow::on_menuViewSliceView_triggered(bool checked)
{
    ui->sliceDock->setVisible(checked);
    if (checked) {
        QMainWindow::tabifyDockWidget(ui->volumePropertyDock, ui->sliceDock);
        ui->sliceDock->raise();
    }
}

void MainWindow::changeCurrentTreeItem(TreeItem *item)
{
    currentTreeItem = item;
}

void MainWindow::on_actionSlice_Tool_triggered()
{
    if (meshDoc()->numOfVisibleVolume < 1) {
        ui->actionSlice_Tool->setChecked(false);
        return;
    }
    sliceToolAction->trigger();
    if (sliceToolAction->isChecked()) {
        QMainWindow::tabifyDockWidget(ui->volumePropertyDock, ui->sliceDock);
        ui->sliceDock->setVisible(true);
        ui->sliceDock->raise();
        ui->sliceViewWidget->setToolboxVisibility(false);
    } else {
        ui->sliceViewWidget->sliceView->clearAllSelectionData();
        ui->sliceViewWidget->sliceView->currentSlectionMode = SliceView::SelectionMode::NONE;
        ui->sliceDock->setVisible(false);
    }
    ui->actionSlice_Tool->setChecked(sliceToolAction->isChecked());
}

void MainWindow::on_actionOpen_project_triggered()
{
    QString homePath = QDir::currentPath();
    QString filter = "XML files (*.xml)";
    QString projectFilePath = QFileDialog::getOpenFileName(this, "Open project", homePath, filter, &filter);
    if (projectFilePath == "")
        return;

    QDomDocument xmlBOM;
    QFile file(projectFilePath);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Error while loading file";
        return;
    }

    xmlBOM.setContent(&file);
    file.close();
    // Clear all old project
    if (treeManager->treeModel->hasChildren()) {
        treeManager->treeModel->removeRows(0, treeManager->treeModel->rowCount(), QModelIndex());
    }

    // Project node
    QDomElement projectEl = xmlBOM.documentElement();
    // Intensity data node
    QDomElement intensityDataEl = projectEl.firstChildElement("IntensityData");
    QStringList measureMiliList = intensityDataEl.attribute("measureMili", "").split(" ");
    QStringList measurePixelList = intensityDataEl.attribute("measurePixel", "").split(" ");


    // Construct bouding box in real world (millimeter unit)
    Measurement3f measurementMillimeter = Measurement3f(measureMiliList.at(0).toFloat(),
                             measureMiliList.at(1).toFloat(),
                             measureMiliList.at(2).toFloat());

    Measurement3i measurementPixel = Measurement3i(measurePixelList.at(0).toInt(),
                                                   measurePixelList.at(1).toInt(),
                                                   measurePixelList.at(2).toInt());

    // Set measurements for MeshDocument
    meshDoc()->setMeasurementMillimeter(measurementMillimeter);
    meshDoc()->setMeasurementPixel(measurementPixel);
    meshDoc()->setTextureBBoxMillimeter();

    QDir patientDir(projectFilePath);
    patientDir.cdUp();
    QString intensityPath = patientDir.absolutePath() + "/" + intensityDataEl.attribute("link", "");

    QFile intensityFile(intensityPath);
    if (!intensityFile.open(QIODevice::ReadOnly)) {
        return;
    }
    QDataStream in(&intensityFile);
    quint32 size;
    in >> size;

    int index = 0;
    int count = 100;
    int step = size / count;
    if (step == 0) step = 1;
    count = size / step;
    for (int i = 0; i < size; ++i) {
        qint16 intensity;
        in >> intensity;
        meshDoc()->globalTexture3DIntensityData[i] = intensity;
        if (i % step == 0) {
            QCallBack(100 * index * 1.0/ count, "Building global texture intensity data");
            index++;
        }
    }
    file.close();

    qDebug() << "BBox:" << measurementMillimeter.X() << measurementMillimeter.Y() << measurementMillimeter.Z();
    qDebug() << "BBox pixel:" << measurementPixel.X() << measurementPixel.Y() << measurementPixel.Z();

    // Get 8 vertices of Bound Box
    cv::Point3f v0(0.0, 0.0, 0.0);
    cv::Point3f v1(measurementMillimeter.X(), 0.0, 0.0);
    cv::Point3f v2(measurementMillimeter.X(), measurementMillimeter.Y(), 0.0);
    cv::Point3f v3(0.0, measurementMillimeter.Y(), 0.0);
    cv::Point3f v4(0.0, 0.0, measurementMillimeter.Z());
    cv::Point3f v5(measurementMillimeter.X(), 0.0, measurementMillimeter.Z());
    cv::Point3f v6(measurementMillimeter.X(), measurementMillimeter.Y(), measurementMillimeter.Z());
    cv::Point3f v7(0.0, measurementMillimeter.Y(), measurementMillimeter.Z());
    QList<cv::Point3f> boundBoxVertices;

    boundBoxVertices.push_back(v0);
    boundBoxVertices.push_back(v1);
    boundBoxVertices.push_back(v2);
    boundBoxVertices.push_back(v3);
    boundBoxVertices.push_back(v4);
    boundBoxVertices.push_back(v5);
    boundBoxVertices.push_back(v6);
    boundBoxVertices.push_back(v7);


    importMesh("Bounding mesh", boundBoxVertices);
    meshDoc()->mm()->isVisibleVolume = true;
    meshDoc()->numOfVisibleVolume++;

//    TreeItem *rootNode = this->treeManager->addNewNode("Patient", QCallBack);
//    meshDoc()->rootNode = rootNode;


    // SegmentationTree node
    QDomElement segTreeEl = projectEl.firstChildElement("SegmentationTree");

    // Iterate tree nodes
    QDomElement rootNode = segTreeEl.firstChildElement("TreeNode");
    QList<QFile*> *fileList = new QList<QFile*>();
    QList<TreeItem*> *nodeList = new QList<TreeItem*>();
    QList<QDataStream*> streamList;

    if (!rootNode.isNull()) {
        //Build up segmentation tree
        meshDoc()->rootNode = readNode(patientDir.absolutePath() + "/", rootNode, QModelIndex(), fileList, nodeList);
    }
    for (int f = 0; f < fileList->size(); ++f) {
        QDataStream *in = new QDataStream(fileList->at(f));
        streamList.push_back(in);
        quint32 size;
        (*in) >> size;
    }
//    signed short *originalIntensity = meshDoc()->globalTexture3DIntensityData;

    index = 0; //reset
    for (int i = 0; i < size; ++i) {
        for (int s = 0; s < streamList.size(); ++s) {
            bool mask;
            (*streamList.at(s)) >> mask;
            nodeList->at(s)->opacity[i] = mask;
        }
        if (i % step == 0) {
            QCallBack(100 * index * 1.0/ count, "Building opacity mask");
            index++;
        }
    }
    // Close all file after
    for (int i = 0; i < nodeList->size(); ++i) {
        fileList->at(i)->close();
    }
//    meshDoc()->updateTexture3DRgbaData(QCallBack, true);
    qb->hide();
    MainWindow::globalStatusBar()->hide();
    meshDoc()->projectPath = projectFilePath; // Set current project path in meshdocument
}

void MainWindow::on_actionSave_Project_triggered()
{
    if (meshDoc()->rootNode == NULL) return;
    QString dicomPath = meshDoc()->getDICOMPath();
    QDir patientDir(dicomPath);
    patientDir.cdUp();
    QString filename;
    if (meshDoc()->projectPath == "") {
        QString filter = "XML File (*.xml)";
        filename = QFileDialog::getSaveFileName(this, "Save to", patientDir.absolutePath() + "/myproject.xml", filter, &filter);
    } else {
        QFileInfo xmlFile(meshDoc()->projectPath);
        QDir projectPath(xmlFile.absoluteDir().absolutePath());
        projectPath.cdUp();
        filename = projectPath.absolutePath() + "/" + xmlFile.baseName();
    }
    saveProject(filename);
}

void MainWindow::on_actionSave_Project_as_triggered()
{
    if (meshDoc()->rootNode == NULL) return;
    QString dicomPath = meshDoc()->getDICOMPath();
    QDir patientDir(dicomPath);
    patientDir.cdUp();
    QString filename;
    QString filter = "XML File (*.xml)";
    filename = QFileDialog::getSaveFileName(this, "Save to", patientDir.absolutePath() + "/myproject.xml", filter, &filter);
    saveProject(filename);
}
