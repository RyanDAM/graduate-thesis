#include "oelm.h"
#include <float.h>

void OSELM::onlineSequenceLearning(mat * dataSet, int N0, int Ni, double C, double threshold) {

    uword numberOfTrainData = (uword)size((*(dataSet)))[0];

    int totalBatch = 1;
    int totalOnlineBatch = (numberOfTrainData - N0) % Ni == 0 ? (numberOfTrainData - N0) / Ni : (numberOfTrainData - N0) / Ni + 1;
    totalBatch += totalOnlineBatch;

    int currentBatch = 1;

    uword nextIndex = 0;
    while (firstLearn
            || (nextIndex + Ni < numberOfTrainData)
            || ((nextIndex + Ni > numberOfTrainData) && (nextIndex < numberOfTrainData))) {

        bool notEnoughChunk = ((nextIndex + Ni > numberOfTrainData) && (nextIndex < numberOfTrainData));

        uword nextBorderSize;
        if (notEnoughChunk) {
            nextBorderSize = numberOfTrainData - nextIndex;
        } else {
            if (firstLearn) {
                nextBorderSize = N0;
            } else {
                nextBorderSize = Ni;
            }
        }

        arma::mat batchData;
        arma::mat batchTarget;

        batchData = dataSet->rows(nextIndex, nextIndex + nextBorderSize - 1);

        nextIndex += nextBorderSize;

        startLearning(&batchData, C);

        currentBatch++;
    }

    findArphiNumber(*dataSet, threshold);
}

void OSELM::startLearning(mat * dataSet, double C) {
    int numberOfTrainData = size((*(dataSet)))[0];

    // Extend the bias matrix
    arma::mat tempBias = arma::mat(numberOfHiddenNeurons, numberOfTrainData, arma::fill::zeros);
    for (int col = 0; col < numberOfTrainData; col++) {
        tempBias.col(col) = hiddenBiases;
    }

    // Calculate H
    arma::mat tempH = hiddenWeights * (*(dataSet)).t();
    tempH = tempH + tempBias;
    arma::mat H = arma::mat(size(tempH), arma::fill::zeros);
    activation(tempH, &H);
    arma::mat HT = H.t();

    if (firstLearn) {

        int size = arma::size(HT)[1];
        arma::mat I = arma::eye<arma::mat>(size, size);
        I = I / C;

        // Calculate P
        lastP = I + HT.t()*HT;

        lastP = lastP.i();

        // Calculate beta
        outputWeights = lastP * HT.t() * (*dataSet);

        firstLearn = false;

    } else {

        int size = arma::size(HT)[0];
        arma::mat I = arma::eye<arma::mat>(size, size);

        // Calculate P
        arma::mat Pk0 = lastP;
        arma::mat temp = I + HT*Pk0*HT.t();
        lastP = Pk0 - Pk0*HT.t()*temp.i()*HT*Pk0;

        arma::mat Bk0 = outputWeights;
        temp = (*dataSet) - HT*Bk0;
        outputWeights = Bk0 + lastP*HT.t()*temp;
    }
}

arma::mat OSELM::feedForward(mat &data) {
    int numberOfData = size(data)[0];

    // Extend the bias matrix
    arma::mat tempBias = arma::mat(numberOfHiddenNeurons, numberOfData, arma::fill::zeros);

    for (int col = 0; col < numberOfData; col++) {
        tempBias.col(col) = hiddenBiases;
    }

    arma::mat tempH = hiddenWeights * data.t();
    tempH = tempH + tempBias;

    arma::mat H = arma::mat(size(tempH), arma::fill::zeros);
    activation(tempH, &H);

    arma::mat Y = H.t() * outputWeights;

    return Y;
}

arma::mat OSELM::classify(mat &data) {

    int numberOfData = size(data)[0];

    arma::mat Y = feedForward(data);

    arma::mat ret = arma::mat(numberOfData, 1, arma::fill::zeros);

    for (int i = 0; i < numberOfData; i++) {
        mat y = Y.row(i);
        mat t = data.row(i);
        mat minus = t - y;
        minus = abs(minus);
        double errorRate = arma::accu(minus);
        ret.row(i)[0] = errorRate;
    }

    return ret;
}

arma::mat OSELM::classifyClass(mat &data) {
    arma::mat tempRet = classify(data);
    arma::mat delta = arphi - tempRet;
    arma::mat flagMat = sign(delta);
    flagMat.transform( [](int val) { return val < 0 ? 0 : val; } );
    return flagMat;
}

void OSELM::findArphiNumber(mat &data, double threshold) {
    arma::mat tempRet = classify(data);
    trainD = sort(tempRet, "descend");

    setThresholdTo(threshold);
}

double OSELM::setThresholdTo(double threshold) {
    this->threshold = threshold;

    uword numberOfData = size(trainD)[0];
    uword arphiIndex = floor(threshold*numberOfData);
    arphi = trainD[arphiIndex];

    std::cout << "Arphi: " << arphi << std::endl;

    return arphi;
}

void OSELM::printErrorRate(arma::mat &data) {
    uword numberOfData = size(data)[0];

    double maxT = DBL_MIN;
    double minT = DBL_MAX;

    double sum = 0;

    for (uword i = 0; i < numberOfData; i++) {
        arma::mat input = data.row(i);
        arma::mat ret = classify(input);
        double errorRate = ret[0];
        sum += errorRate;
        if (errorRate > maxT) maxT = errorRate;
        else if (errorRate < minT) minT = errorRate;
    }

    double mean = sum / double(numberOfData);

    std::cout << "Max: " << maxT << std::endl
    << "Min: " << minT << std::endl
    << "Mean: " << mean << std::endl
    << "Left: " << maxT - mean << std::endl
    << "Right: " << mean - minT << std::endl << std::endl;
}

double OSELM::testAccurate(ELMData * data) {
    uword numberOfData = size(*data->data)[0];

    int * failArray = new int[2];
    for (int i = 0; i < 2; i++) {
        failArray[i] = 0;
    }

    arma::mat tempRet = classify(*data->data);
    arma::mat delta = arphi - tempRet;

    arma::mat flagMat = sign(delta);
    flagMat.transform( [](int val) { return val < 0 ? 0 : val; } );

    int numOfFail = 0;
    for (uword i = 0; i < numberOfData; i++) {
        int flag = (int)flagMat.row(i)[0];
        int actualClass = (int)(*(data->target)).row(i)[0];
        if (flag != actualClass) {
            failArray[actualClass] += 1;
            numOfFail++;
        }
    }

    double acc = 1.0 - (double)numOfFail/(double)numberOfData;
    std::cout << "Accurate (%): " << acc*100.0 << std::endl
    << "Fail: " << numOfFail << std::endl
    << "Total: " << numberOfData << std::endl;
    for (int i = 0; i < 2; i++) {
        std::cout << "Label " << i << ": " << failArray[i] << std::endl;
    }
    return acc;
}

void OSELM::activation(arma::mat &m, arma::mat *ret) {
    int totalNum = size(m)[0] * size(m)[1];
    for (int i = 0; i < totalNum; i++) {
        (*ret)[i] = activation((double)m[i]);
    }
}

double OSELM::activation(double input) {
    if (activeFunc == NONE) {
        return input;
    } else if (activeFunc == SIGMOID) {
        return 1 / (1 + exp(-input));
    }
    return input;
}

void OSELM::printNet() {
    hiddenBiases.print("Hidden biases");
    hiddenWeights.print("Hidden weights");
    outputWeights.print("Output weights");
    lastP.print("Last P");
}

bool OSELM::saveNet(std::string path) {
    bool ret = true;

    ret = ret && hiddenBiases.save(path + "hiddenBiases", arma::raw_ascii);
    ret = ret && hiddenWeights.save(path + "hiddenWeights", arma::raw_ascii);
    ret = ret && outputWeights.save(path + "outputWeights", arma::raw_ascii);
    ret = ret && lastP.save(path + "lastP", arma::raw_ascii);
    ret = ret && trainD.save(path + "trainD", arma::raw_ascii);

    arma::mat netInfo = arma::mat(6, 1);
    netInfo.row(0)[0] = numberOfHiddenNeurons;
    netInfo.row(1)[0] = numberOfInput;
    netInfo.row(2)[0] = activeFunc;
    netInfo.row(3)[0] = firstLearn;
    netInfo.row(4)[0] = threshold;
    netInfo.row(5)[0] = arphi;

    ret = ret && netInfo.save(path + "netInfo", arma::raw_ascii);

    return ret;
}

bool OSELM::loadNet(std::string path) {
    bool ret = true;

    ret = ret && hiddenBiases.load(path + "hiddenBiases", arma::raw_ascii);
    ret = ret && hiddenWeights.load(path + "hiddenWeights", arma::raw_ascii);
    ret = ret && outputWeights.load(path + "outputWeights", arma::raw_ascii);
    ret = ret && lastP.load(path + "lastP", arma::raw_ascii);
    ret = ret && trainD.load(path + "trainD", arma::raw_ascii);

    arma::mat netInfo = arma::mat(6, 1);
    ret = ret && netInfo.load(path + "netInfo", arma::raw_ascii);

    numberOfHiddenNeurons = (int) netInfo.row(0)[0];
    numberOfInput = (int) netInfo.row(1)[0];
    int func = netInfo.row(2)[0];
    activeFunc = (ActivationFunction)func;
    firstLearn = (int) netInfo.row(3)[0];
    threshold = netInfo.row(4)[0];
    arphi = netInfo.row(5)[0];

    return ret;
}
