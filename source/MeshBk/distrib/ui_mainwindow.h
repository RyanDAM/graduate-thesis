/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDockWidget>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMdiArea>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QTableWidget>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include <sliceview.h>
#include "volumepropertywidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *menuExit;
    QAction *menuApplyFilter;
    QAction *menuOpenFile;
    QAction *menuOpenFolder;
    QAction *buttonImportMesh;
    QAction *menuViewMeshLayer;
    QAction *butonImportDICOM;
    QAction *menuColoringFilter;
    QAction *menuXrayShader;
    QAction *showTrackBallAct;
    QAction *menuViewAxis;
    QAction *actionImport_DICOM;
    QAction *menuViewVolumeProperty;
    QAction *menuViewVolume;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QMdiArea *mdiarea;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuView;
    QToolBar *mainToolbar;
    QDockWidget *volumePropertyDock;
    QWidget *volumePropertyDockContent;
    QVBoxLayout *verticalLayout_3;
    VolumePropertyWidget *volumeProperty;
    QDockWidget *sliceDock;
    QWidget *dockWidgetContents_2;
    QVBoxLayout *verticalLayout_4;
    SliceView *sliceView;
    QDockWidget *layersDock;
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout;
    QTableWidget *layerTable;
    QVBoxLayout *sliderLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QSlider *backgroundColorSlider;
    QPushButton *resetButton;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(856, 904);
        MainWindow->setStyleSheet(QString::fromUtf8("#volumePropertyDock {\n"
"	background-color: rgb(107, 107, 107);\n"
"}"));
        menuExit = new QAction(MainWindow);
        menuExit->setObjectName(QString::fromUtf8("menuExit"));
        menuApplyFilter = new QAction(MainWindow);
        menuApplyFilter->setObjectName(QString::fromUtf8("menuApplyFilter"));
        menuOpenFile = new QAction(MainWindow);
        menuOpenFile->setObjectName(QString::fromUtf8("menuOpenFile"));
        menuOpenFolder = new QAction(MainWindow);
        menuOpenFolder->setObjectName(QString::fromUtf8("menuOpenFolder"));
        buttonImportMesh = new QAction(MainWindow);
        buttonImportMesh->setObjectName(QString::fromUtf8("buttonImportMesh"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/imortMesh.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonImportMesh->setIcon(icon);
        menuViewMeshLayer = new QAction(MainWindow);
        menuViewMeshLayer->setObjectName(QString::fromUtf8("menuViewMeshLayer"));
        menuViewMeshLayer->setCheckable(true);
        butonImportDICOM = new QAction(MainWindow);
        butonImportDICOM->setObjectName(QString::fromUtf8("butonImportDICOM"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/importDICOM.png"), QSize(), QIcon::Disabled, QIcon::Off);
        butonImportDICOM->setIcon(icon1);
        menuColoringFilter = new QAction(MainWindow);
        menuColoringFilter->setObjectName(QString::fromUtf8("menuColoringFilter"));
        menuXrayShader = new QAction(MainWindow);
        menuXrayShader->setObjectName(QString::fromUtf8("menuXrayShader"));
        menuXrayShader->setCheckable(true);
        showTrackBallAct = new QAction(MainWindow);
        showTrackBallAct->setObjectName(QString::fromUtf8("showTrackBallAct"));
        menuViewAxis = new QAction(MainWindow);
        menuViewAxis->setObjectName(QString::fromUtf8("menuViewAxis"));
        menuViewAxis->setCheckable(true);
        actionImport_DICOM = new QAction(MainWindow);
        actionImport_DICOM->setObjectName(QString::fromUtf8("actionImport_DICOM"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/importicon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionImport_DICOM->setIcon(icon2);
        menuViewVolumeProperty = new QAction(MainWindow);
        menuViewVolumeProperty->setObjectName(QString::fromUtf8("menuViewVolumeProperty"));
        menuViewVolumeProperty->setCheckable(true);
        menuViewVolume = new QAction(MainWindow);
        menuViewVolume->setObjectName(QString::fromUtf8("menuViewVolume"));
        menuViewVolume->setCheckable(true);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/volumeShow.png"), QSize(), QIcon::Normal, QIcon::On);
        menuViewVolume->setIcon(icon3);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        mdiarea = new QMdiArea(centralWidget);
        mdiarea->setObjectName(QString::fromUtf8("mdiarea"));

        verticalLayout_2->addWidget(mdiarea);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 856, 20));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QString::fromUtf8("menuEdit"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        MainWindow->setMenuBar(menuBar);
        mainToolbar = new QToolBar(MainWindow);
        mainToolbar->setObjectName(QString::fromUtf8("mainToolbar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolbar);
        volumePropertyDock = new QDockWidget(MainWindow);
        volumePropertyDock->setObjectName(QString::fromUtf8("volumePropertyDock"));
        volumePropertyDock->setMinimumSize(QSize(340, 47));
        volumePropertyDock->setAutoFillBackground(false);
        volumePropertyDock->setStyleSheet(QString::fromUtf8("#volumePropertyDock::title{\n"
"background-color: rgb(206, 206, 206);\n"
"padding: 2px;\n"
"}"));
        volumePropertyDockContent = new QWidget();
        volumePropertyDockContent->setObjectName(QString::fromUtf8("volumePropertyDockContent"));
        volumePropertyDockContent->setMinimumSize(QSize(340, 0));
        volumePropertyDockContent->setStyleSheet(QString::fromUtf8("#volumePropertyDockContent{\n"
"background-color: rgb(206, 206, 206);\n"
"border: 1px solid rgb(138, 138, 138);\n"
"}"));
        verticalLayout_3 = new QVBoxLayout(volumePropertyDockContent);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        volumeProperty = new VolumePropertyWidget(volumePropertyDockContent);
        volumeProperty->setObjectName(QString::fromUtf8("volumeProperty"));
        volumeProperty->setMinimumSize(QSize(150, 0));

        verticalLayout_3->addWidget(volumeProperty);

        volumePropertyDock->setWidget(volumePropertyDockContent);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(1), volumePropertyDock);
        sliceDock = new QDockWidget(MainWindow);
        sliceDock->setObjectName(QString::fromUtf8("sliceDock"));
        sliceDock->setStyleSheet(QString::fromUtf8("padding: 0px;margin: 0px;"));
        sliceDock->setFeatures(QDockWidget::DockWidgetFloatable|QDockWidget::DockWidgetMovable);
        sliceDock->setAllowedAreas(Qt::LeftDockWidgetArea|Qt::RightDockWidgetArea);
        dockWidgetContents_2 = new QWidget();
        dockWidgetContents_2->setObjectName(QString::fromUtf8("dockWidgetContents_2"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(dockWidgetContents_2->sizePolicy().hasHeightForWidth());
        dockWidgetContents_2->setSizePolicy(sizePolicy);
        dockWidgetContents_2->setStyleSheet(QString::fromUtf8("padding: 0px; margin: 0px;"));
        verticalLayout_4 = new QVBoxLayout(dockWidgetContents_2);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        sliceView = new SliceView(dockWidgetContents_2);
        sliceView->setObjectName(QString::fromUtf8("sliceView"));
        sizePolicy.setHeightForWidth(sliceView->sizePolicy().hasHeightForWidth());
        sliceView->setSizePolicy(sizePolicy);
        sliceView->setMinimumSize(QSize(300, 300));
        sliceView->setStyleSheet(QString::fromUtf8("padding: 0px;"));

        verticalLayout_4->addWidget(sliceView);

        sliceDock->setWidget(dockWidgetContents_2);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), sliceDock);
        layersDock = new QDockWidget(MainWindow);
        layersDock->setObjectName(QString::fromUtf8("layersDock"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(layersDock->sizePolicy().hasHeightForWidth());
        layersDock->setSizePolicy(sizePolicy1);
        layersDock->setMinimumSize(QSize(300, 300));
        layersDock->setMaximumSize(QSize(300, 524287));
        layersDock->setCursor(QCursor(Qt::ArrowCursor));
        layersDock->setFloating(false);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        verticalLayout = new QVBoxLayout(dockWidgetContents);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetMaximumSize);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        layerTable = new QTableWidget(dockWidgetContents);
        layerTable->setObjectName(QString::fromUtf8("layerTable"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(layerTable->sizePolicy().hasHeightForWidth());
        layerTable->setSizePolicy(sizePolicy2);
        layerTable->setStyleSheet(QString::fromUtf8(""));

        verticalLayout->addWidget(layerTable);

        sliderLayout = new QVBoxLayout();
        sliderLayout->setSpacing(6);
        sliderLayout->setObjectName(QString::fromUtf8("sliderLayout"));
        sliderLayout->setSizeConstraint(QLayout::SetMinimumSize);
        label = new QLabel(dockWidgetContents);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy3);

        sliderLayout->addWidget(label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        backgroundColorSlider = new QSlider(dockWidgetContents);
        backgroundColorSlider->setObjectName(QString::fromUtf8("backgroundColorSlider"));
        backgroundColorSlider->setOrientation(Qt::Horizontal);

        horizontalLayout->addWidget(backgroundColorSlider);

        resetButton = new QPushButton(dockWidgetContents);
        resetButton->setObjectName(QString::fromUtf8("resetButton"));

        horizontalLayout->addWidget(resetButton);


        sliderLayout->addLayout(horizontalLayout);


        verticalLayout->addLayout(sliderLayout);

        layersDock->setWidget(dockWidgetContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), layersDock);
        layersDock->raise();

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuFile->addAction(menuOpenFile);
        menuFile->addAction(menuOpenFolder);
        menuFile->addAction(menuExit);
        menuEdit->addAction(menuApplyFilter);
        menuEdit->addAction(menuColoringFilter);
        menuView->addSeparator();
        menuView->addAction(menuViewMeshLayer);
        menuView->addAction(menuViewVolumeProperty);
        menuView->addAction(menuXrayShader);
        menuView->addAction(showTrackBallAct);
        menuView->addSeparator();
        menuView->addAction(menuViewVolume);
        menuView->addAction(menuViewAxis);
        mainToolbar->addAction(actionImport_DICOM);
        mainToolbar->addAction(buttonImportMesh);
        mainToolbar->addAction(butonImportDICOM);
        mainToolbar->addSeparator();
        mainToolbar->addAction(menuViewVolume);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        menuExit->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        menuApplyFilter->setText(QApplication::translate("MainWindow", "Apply Filter", 0, QApplication::UnicodeUTF8));
        menuOpenFile->setText(QApplication::translate("MainWindow", "Open File", 0, QApplication::UnicodeUTF8));
        menuOpenFolder->setText(QApplication::translate("MainWindow", "Open Folder", 0, QApplication::UnicodeUTF8));
        buttonImportMesh->setText(QApplication::translate("MainWindow", "Import Mesh", 0, QApplication::UnicodeUTF8));
        menuViewMeshLayer->setText(QApplication::translate("MainWindow", "Mesh Layer", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        menuViewMeshLayer->setToolTip(QApplication::translate("MainWindow", "Show Mesh Layer Dock", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        butonImportDICOM->setText(QApplication::translate("MainWindow", "Import DICOM", 0, QApplication::UnicodeUTF8));
        menuColoringFilter->setText(QApplication::translate("MainWindow", "Per Face Color Function", 0, QApplication::UnicodeUTF8));
        menuXrayShader->setText(QApplication::translate("MainWindow", "Shader xray", 0, QApplication::UnicodeUTF8));
        showTrackBallAct->setText(QApplication::translate("MainWindow", "Show Track Ball", 0, QApplication::UnicodeUTF8));
        menuViewAxis->setText(QApplication::translate("MainWindow", "Show Axis", 0, QApplication::UnicodeUTF8));
        actionImport_DICOM->setText(QApplication::translate("MainWindow", "Import DICOM", 0, QApplication::UnicodeUTF8));
        menuViewVolumeProperty->setText(QApplication::translate("MainWindow", "Volume Property", 0, QApplication::UnicodeUTF8));
        menuViewVolume->setText(QApplication::translate("MainWindow", "Volume Rendering", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menuEdit->setTitle(QApplication::translate("MainWindow", "Edit", 0, QApplication::UnicodeUTF8));
        menuView->setTitle(QApplication::translate("MainWindow", "View", 0, QApplication::UnicodeUTF8));
        mainToolbar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0, QApplication::UnicodeUTF8));
        volumePropertyDock->setWindowTitle(QApplication::translate("MainWindow", "Volume Properties", 0, QApplication::UnicodeUTF8));
        sliceDock->setWindowTitle(QApplication::translate("MainWindow", "Slice View", 0, QApplication::UnicodeUTF8));
        layersDock->setWindowTitle(QString());
        label->setText(QApplication::translate("MainWindow", "Brightness", 0, QApplication::UnicodeUTF8));
        resetButton->setText(QApplication::translate("MainWindow", "Reset", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
