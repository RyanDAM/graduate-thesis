#include "dicominfo.h"

DicomInfo::DicomInfo(QString path) {
     std::unique_ptr<imebra::DataSet> temp(imebra::CodecFactory::load(path.toStdString()));
     mDataSet = std::move(temp);
}

unsigned long DicomInfo::getInstanceNumber() {
    unsigned long instanceNumber = mDataSet->getUnsignedLong(imebra::TagId(imebra::tagId_t::InstanceNumber_0020_0013), 0);
    return instanceNumber;
}

double DicomInfo::getSliceThickness() {
    double sliceThickness = mDataSet->getDouble(imebra::TagId(imebra::tagId_t::SliceThickness_0018_0050), 0);
    return sliceThickness;
}

double DicomInfo::getSpacingBetweenSlices() {
    double spacingBetweenSlices = mDataSet->getDouble(imebra::TagId(imebra::tagId_t::SpacingBetweenSlices_0018_0088), 0);
    return spacingBetweenSlices;
}

double DicomInfo::getPixelSpacingX() {
    double pixelSpacingX = mDataSet->getDouble(imebra::TagId(imebra::tagId_t::PixelSpacing_0028_0030 ), 0);
    return pixelSpacingX;
}

double DicomInfo::getPixelSpacingY() {
    double pixelSpacingY = mDataSet->getDouble(imebra::TagId(imebra::tagId_t::PixelSpacing_0028_0030 ), 1);
    return pixelSpacingY;
}

QString DicomInfo::getPatientName() {
    std::string name = mDataSet->getString(imebra::TagId(imebra::tagId_t::PatientName_0010_0010), 0);
    return QString::fromStdString(name);
}

QString DicomInfo::getPatientSex() {
    std::string sex = mDataSet->getString(imebra::TagId(imebra::tagId_t::PatientSex_0010_0040), 0);
    return QString::fromStdString(sex);
}

cv::Mat DicomInfo::getImage() {
    std::unique_ptr<imebra::Image> loadedImage(mDataSet->getImage(0));

    long width = loadedImage->getWidth();
    long height = loadedImage->getHeight();

    cv::Mat grayImage = cv::Mat::zeros(height, width, CV_8UC1);

    std::unique_ptr<imebra::ReadingDataHandlerNumeric> dataHandler(loadedImage->getReadingDataHandler());

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            long luminance = dataHandler->getSignedLong(y*width + x);
            grayImage.at<uchar>(y, x) = luminance;
        }
    }
    return grayImage;
}

cv::Mat DicomInfo::getOrginIntensityImage(signed short complement) {
    //Because intensity of CT/MRI in range [-1024 1023], for convenient computation,
    //we will add complement part (for bitwise operators).
    //Before apply some mapping (color mapping), we must subtract complement part to convert it into real intensity
    signed short COMPLEMENT_INTENSITY = complement;

    std::unique_ptr<imebra::Image> loadedImage(mDataSet->getImage(0));

    long width = loadedImage->getWidth();
    long height = loadedImage->getHeight();

    cv::Mat image = cv::Mat::zeros(height, width, CV_16SC1);

    std::unique_ptr<imebra::ReadingDataHandlerNumeric> dataHandler(loadedImage->getReadingDataHandler());

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            signed short intensity = (signed short)dataHandler->getSignedLong(y*width + x);
            image.at<signed short>(y, x) = intensity + COMPLEMENT_INTENSITY;
        }
    }
    return image;
}

DicomInfo::~DicomInfo() {

}
