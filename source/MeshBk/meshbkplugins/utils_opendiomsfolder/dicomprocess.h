#ifndef DICOMPROCESS_H
#define DICOMPROCESS_H

#include "dicominfo.h"
#include <wrap/callback.h>
#include <QDir>
#include <QFileInfoList>
#include <QMap>
#include <QTextStream>
#include <QDebug>
#include <QObject>
#include <QMessageBox>

typedef struct {
    float x;
    float y;
    float z;
    uchar value; // gray-scalse R = G = B = value
    uchar alpha; // Opacity
} DataPoint;

typedef struct {
    cv::Mat mat;
    cv::Mat mask;
    float xScale;
    float yScale;
    float zScale;
    float zSpace;
    int instanceNumber;
} DataSlice;

class DicomProcess: public QObject {
Q_OBJECT
public:
    DicomProcess();
    DicomProcess(QString, QString, QString);
    ~DicomProcess();

    QString writeVolumeDataPlyFile(QString folderIn, QString folderOut, QString fileName);
    cv::Mat extractAnOrgran(cv::Mat original, cv::Mat mask);
    QMap<int, DataSlice> extractOrganSlices(QString folderIn, vcg::CallBackPos* = 0);
private:
    QString getOriginalFolder(QString maskFolder); // find PATIENT_DICOM folder
    void addDataPoint(QList<DataPoint> &list, cv::Mat mat, float z);

public:
    const QString ORIGINAL_FOLDER_NAME = "PATIENT_DICOM";
private:
    QString folderIn;
    QString folderOut;
    QString fileName;

public slots:
    void startProcess();
signals:
    void importVolumeData(QString);
    void writeFinished();
    void dirNotFoundMessage();
};

#endif // DICOMPROCESS_H
