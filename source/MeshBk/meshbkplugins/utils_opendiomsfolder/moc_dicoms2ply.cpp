/****************************************************************************
** Meta object code from reading C++ file 'dicoms2ply.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "dicoms2ply.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dicoms2ply.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Dicoms2Ply[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      46,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      65,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Dicoms2Ply[] = {
    "Dicoms2Ply\0\0importFinishedPointCloud(QString)\0"
    "parsingCompleted()\0parseDicomsFolder2Ply()\0"
};

void Dicoms2Ply::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Dicoms2Ply *_t = static_cast<Dicoms2Ply *>(_o);
        switch (_id) {
        case 0: _t->importFinishedPointCloud((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->parsingCompleted(); break;
        case 2: _t->parseDicomsFolder2Ply(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Dicoms2Ply::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Dicoms2Ply::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Dicoms2Ply,
      qt_meta_data_Dicoms2Ply, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Dicoms2Ply::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Dicoms2Ply::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Dicoms2Ply::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Dicoms2Ply))
        return static_cast<void*>(const_cast< Dicoms2Ply*>(this));
    return QObject::qt_metacast(_clname);
}

int Dicoms2Ply::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void Dicoms2Ply::importFinishedPointCloud(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Dicoms2Ply::parsingCompleted()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
