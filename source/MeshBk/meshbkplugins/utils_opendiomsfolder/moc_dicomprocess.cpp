/****************************************************************************
** Meta object code from reading C++ file 'dicomprocess.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "dicomprocess.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dicomprocess.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DicomProcess[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,
      40,   13,   13,   13, 0x05,
      56,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      77,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DicomProcess[] = {
    "DicomProcess\0\0importVolumeData(QString)\0"
    "writeFinished()\0dirNotFoundMessage()\0"
    "startProcess()\0"
};

void DicomProcess::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DicomProcess *_t = static_cast<DicomProcess *>(_o);
        switch (_id) {
        case 0: _t->importVolumeData((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->writeFinished(); break;
        case 2: _t->dirNotFoundMessage(); break;
        case 3: _t->startProcess(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DicomProcess::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DicomProcess::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_DicomProcess,
      qt_meta_data_DicomProcess, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DicomProcess::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DicomProcess::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DicomProcess::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DicomProcess))
        return static_cast<void*>(const_cast< DicomProcess*>(this));
    return QObject::qt_metacast(_clname);
}

int DicomProcess::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void DicomProcess::importVolumeData(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DicomProcess::writeFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void DicomProcess::dirNotFoundMessage()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
