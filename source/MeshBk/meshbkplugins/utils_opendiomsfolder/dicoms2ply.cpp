#include "dicoms2ply.h"
#include <QDebug>

Dicoms2Ply::Dicoms2Ply() {
    fileName = QString("");
}

Dicoms2Ply::Dicoms2Ply(QString in, QString out) {
    fileName = QString("");
    folderIn = in;
    folderOut = out;
}

Dicoms2Ply::~Dicoms2Ply() {
}

void Dicoms2Ply::parseDicomsFolder2Ply() {
    if (folderIn.length() != 0 && folderOut.length() != 0)
        dicomFolder2Ply(folderIn, folderOut);
}

QString Dicoms2Ply::dicomFolder2Ply(QString folderIn, QString folderOut, vcg::CallBackPos *cb) {
    QList<cv::Point3f> point3DList = transform2PointCloud(folderIn, cb);
    QString fileOut = pointCloud2Ply(point3DList, folderOut, cb);

    emit parsingCompleted();
    emit importFinishedPointCloud(fileOut);

    return fileOut;
}

QList<cv::Point3f> Dicoms2Ply::transform2PointCloud(QString path, vcg::CallBackPos *cb) {
    return folder2PointCloud(path, cb);
}

QList<cv::Point3f> Dicoms2Ply::folder2PointCloud(QString folder, vcg::CallBackPos *cb) {
    QList<QString> fileNames = getFileNamesInFolder(folder);

    QList<cv::Point3f> pointCloud;

    int size = fileNames.size();
    int count = 100;
    int index = 0;
    int step = size / count;
    count = size / step;
    for (int i = 0; i < size; i++) {
        if (i % step == 0) {
            if (cb) {
                cb(100 * index * 1.0 / count, "Generating PointCloud");
            }
            index++;
        }
        QList<cv::Point3f> point3DList = file2PointCloud(fileNames.at(i));
        pointCloud.append(point3DList); // append point3DList into pointCloud

    }

    return pointCloud;
}

QList<QString> Dicoms2Ply::getFileNamesInFolder(QString path) {
    QDir dir(path);
    dir.setFilter(QDir::Files | QDir::NoSymLinks);
    dir.setSorting(QDir::Name);

    fileName = dir.dirName() + ".ply" ;

    QFileInfoList fileInfoList = dir.entryInfoList();

    QList<QString> fileNameList;
    for (int i = 0; i < fileInfoList.size(); ++i) {
        QFileInfo fileInfo = fileInfoList.at(i);
        fileNameList.push_back(fileInfo.absoluteFilePath());
    }

    return fileNameList;
}

QList<cv::Point3f> Dicoms2Ply::file2PointCloud(QString file)  {
    QList<cv::Point3f> edgePoint3DList;

    try {
        DicomInfo dicomInfo(file);
        cv::Mat dicomImage = dicomInfo.getImage();
        cv::Mat edgeImage = ImPreProcess::edgeDetect(dicomImage);

        QList<QPair<int, int>> edgePoint2DList = ImPreProcess::getEdgePoints(edgeImage);

        float spacingX = dicomInfo.getPixelSpacingX();
        float spacingY = dicomInfo.getPixelSpacingY();
        float spacingZ = dicomInfo.getSliceThickness();
        float zOrderNumber = dicomInfo.getInstanceNumber();

        QList<QPair<int, int>>::iterator it;
        for (it = edgePoint2DList.begin(); it != edgePoint2DList.end(); ++it) {
            float x = it->first * spacingX;
            float y = it->second * spacingY;
            float z = zOrderNumber * spacingZ;

            edgePoint3DList.push_back(cv::Point3f(x, y, z));
        }
    } catch (imebra::StreamOpenError exception) {
        std::cout << "imebra::StreamOpenError" << std::endl;
    } catch (imebra::MissingTagError exception) {
        std::cout << "imebra::MissingTagError" << std::endl;
    } catch (imebra::StreamEOFError exception) {
        std::cout << "imebra::StreamEOFError" << std::endl;
    } catch (imebra::CodecWrongFormatError exception) {
        std::cout << "imebra::CodecWrongFormatError" << std::endl;
    }

    return edgePoint3DList;
}

QString Dicoms2Ply::pointCloud2Ply(QList<cv::Point3f> point3DList, QString outFolder, vcg::CallBackPos *cb) {
    QDir dir(outFolder);
    if (!dir.exists()) {
        dir.mkdir(outFolder);
    }

    QString fileFullPath = dir.absolutePath() + "/" + fileName;

    QFile file(fileFullPath);
    file.open(QFile::WriteOnly);

    QTextStream outStream(&file);
    outStream << "ply" << "\n";
    outStream << "format ascii 1.0" << "\n";
    outStream << "element vertex " << point3DList.size() <<"\n";
    outStream << "property float x" << "\n";
    outStream << "property float y" << "\n";
    outStream << "property float z" << "\n";
    outStream << "end_header" << "\n";

    QList<cv::Point3f>::iterator it;
    for (it = point3DList.begin(); it != point3DList.end(); ++it) {
        outStream << it->x << " " << it->y << " " << it->z << "\n";
    }

    file.close();

    return fileFullPath;
}


