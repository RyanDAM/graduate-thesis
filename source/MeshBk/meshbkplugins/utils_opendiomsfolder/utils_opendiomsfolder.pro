include (../../shared.pri)

QMAKE_CXXFLAGS += -std=c++11 #T

INCLUDEPATH += ../../external

LIBS += -L../../external/lib/opencv -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc #T
LIBS += -L../../external/lib/imebra -limebra #T

SOURCES += dicominfo.cpp \
			impreprocess.cpp \
                        dicoms2ply.cpp \
                        dicomprocess.cpp
HEADERS  += dicominfo.h \
			impreprocess.h \
                        dicoms2ply.h \
                        dicomprocess.h

TARGET        = utils_opendicomsfolder

