#ifndef DICOMINFO_H
#define DICOMINFO_H

#include <iostream>
#include <opencv2/opencv.hpp>
#include <imebra/imebra.h>
#include <QString>
#include <QList>

class DicomInfo {
public:
       DicomInfo(QString);
       ~DicomInfo();

       unsigned long getInstanceNumber();
       double getSliceThickness();
       double getSpacingBetweenSlices();
       double getPixelSpacingX();
       double getPixelSpacingY();
       QString getPatientName();
       QString getPatientSex();
       cv::Mat getImage();
       cv::Mat getOrginIntensityImage(signed short complement);

private:
        std::unique_ptr<imebra::DataSet> mDataSet;
};

#endif // DICOMINFO_H
