#ifndef DICOMS2PLY_H
#define DICOMS2PLY_H

#include <iostream>
#include <wrap/callback.h>
#include <opencv2/opencv.hpp>
#include <QDir>
#include <QFileInfoList>
#include <QTextStream>
#include <QObject>

#include "dicominfo.h"
#include "impreprocess.h"


class Dicoms2Ply: public QObject{
Q_OBJECT
public:
    Dicoms2Ply();
    Dicoms2Ply(QString, QString);
    ~Dicoms2Ply();

    QString dicomFolder2Ply(QString, QString, vcg::CallBackPos = 0);
    QList<cv::Point3f> transform2PointCloud(QString, vcg::CallBackPos* = 0);
private:
    QString pointCloud2Ply(QList<cv::Point3f>, QString, vcg::CallBackPos* = 0);
    QList<cv::Point3f> folder2PointCloud(QString, vcg::CallBackPos* = 0);
    QList<cv::Point3f> file2PointCloud(QString);
    QList<QString> getFileNamesInFolder(QString);

    QString fileName;
    QString folderIn;
    QString folderOut;

public slots:
     void parseDicomsFolder2Ply();
signals:
     void importFinishedPointCloud(QString);
     void parsingCompleted();
};
#endif // DICOMS2PLY_H
