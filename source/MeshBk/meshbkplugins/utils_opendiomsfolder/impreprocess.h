#ifndef IMPREPROCESS_H
#define IMPREPROCESS_H

#include <QList>
#include <QPair>
#include <opencv2/opencv.hpp>

class ImPreProcess {
public:
    static cv::Mat edgeDetect(cv::Mat);
    static QList<QPair<int, int>> getEdgePoints(cv::Mat);
};

#endif // IMPREPROCESS_H
