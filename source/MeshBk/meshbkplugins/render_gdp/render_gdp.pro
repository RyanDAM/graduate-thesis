include (../../shared.pri)

QMAKE_CXXFLAGS += -std=c++11 #T

INCLUDEPATH += ../../external/muparser_v132/include
INCLUDEPATH += ../../external \
                ../../meshbkplugins \

LIBS += -L../../external/lib/opencv -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc #T
LIBS += -L../../external/lib/imebra -limebra #T
LIBS += -L../../distrib/plugins -lutils_opendicomsfolder

HEADERS       = meshrender.h \
                textfile.h \
                shaderStructs.h \
                shaderDialog.h 

SOURCES       = meshrender.cpp \
		textfile.cpp \  
        shaderDialog.cpp 

TARGET        = render_gdp

FORMS		= shaderDialog.ui

