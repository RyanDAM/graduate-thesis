include (../../shared.pri)

QMAKE_CXXFLAGS += -std=c++11 #T

INCLUDEPATH += ../../external \
                ../../meshbkplugins \

LIBS += -L../../external/lib/opencv -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc #T
LIBS += -L../../external/lib/imebra -limebra #T
LIBS += -L../../distrib/plugins -lutils_opendicomsfolder


HEADERS       += baseio.h \
		$$VCGDIR/wrap/io_trimesh/import_obj.h \
		$$VCGDIR/wrap/io_trimesh/import_off.h \
		$$VCGDIR/wrap/io_trimesh/import_ptx.h \
		$$VCGDIR/wrap/io_trimesh/import_stl.h \
		$$VCGDIR/wrap/io_trimesh/export_ply.h \
		$$VCGDIR/wrap/io_trimesh/export_obj.h \
		$$VCGDIR/wrap/io_trimesh/export_off.h \
		$$VCGDIR/wrap/ply/plylib.h \
		$$VCGDIR/wrap/io_trimesh/io_material.h

SOURCES       += baseio.cpp \
		$$VCGDIR//wrap/ply/plylib.cpp\


TARGET        = io_base
