/****************************************************************************
 * MeshLab                                                           o o     *
 * A versatile mesh processing toolbox                             o     o   *
 *                                                                _   O  _   *
 * Copyright(C) 2005                                                \/)\/    *
 * Visual Computing Lab                                            /\/|      *
 * ISTI - Italian National Research Council                           |      *
 *                                                                    \      *
 * All rights reserved.                                                      *
 *                                                                           *
 * This program is free software; you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation; either version 2 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License (http://www.gnu.org/licenses/gpl.txt)          *
 * for more details.                                                         *
 *                                                                           *
 ****************************************************************************/

#ifndef EDITSLICE_H
#define EDITSLICE_H

#include <GL/glew.h>
#include <wrap/gl/pick.h>
#include <main/glarea.h>
#include <common/interfaces.h>
#include <vcglib/vcg/math/quaternion.h>

enum ControlModes {TRANSLATE, ROTATE, DEFAULT};

class EditSlicePlugin : public QObject, public MeshEditInterface {
	Q_OBJECT
	Q_INTERFACES(MeshEditInterface)

public:
	EditSlicePlugin();
	virtual ~EditSlicePlugin();

	static const QString Info();

	virtual bool StartEdit(MeshModel &/*m*/, GLArea * /*parent*/);
	virtual void EndEdit(MeshModel &/*m*/, GLArea * /*parent*/);
    virtual void Decorate(MeshModel &/*m*/, GLArea * /*parent*/);
	virtual void mousePressEvent(QMouseEvent *event, MeshModel &/*m*/, GLArea * );
	virtual void mouseMoveEvent(QMouseEvent *event, MeshModel &/*m*/, GLArea * );
	virtual void mouseReleaseEvent(QMouseEvent *event, MeshModel &/*m*/, GLArea * );
    virtual void keyReleaseEvent  (QKeyEvent *, MeshModel &/*m*/, GLArea *);
    virtual void keyPressEvent    (QKeyEvent *, MeshModel &/*m*/, GLArea *);
	virtual void tabletEvent(QTabletEvent *, MeshModel & , GLArea *);

signals:
    void updateIntersectionMesh(MeshModel *mm, MeshDocument *md, vcg::Point3f normal);

public slots:
	void update();
	
private:
		
    MeshDocument* md = NULL;
    MeshModel* cuttingMesh = NULL;
    MeshModel* tempSliceMesh = NULL;
    vcg::Point3f cuttingNormal;
    vcg::Point3f cuttingOrigin;
    vcg::Point3f lastPoint;
    MeshModel *boundingMesh = NULL; // Bounding of current mesh, it is a sphere
    ControlModes controlMode;
    vcg::Quaternionf rotation;
    int meshListSize;
    bool isDrag = false;	
	GLArea * glarea;
		
    void initCuttingPlane(vcg::Point3f normal, vcg::Point3f origin);
    MeshModel* initBoundingBoxMesh(MeshModel* currentMesh);
    MeshModel* initBoundingSphere();
    MeshModel* getIntersectionWithAPlane(MeshModel* inputMesh, vcg::Point3f planeAxis, vcg::Point3f planeCenter, vcg::Color4b = vcg::Color4b(62, 128, 248, 100));
    void emitUpdateSignal();
};

/*********OpenGL Drawing Routines************/
void drawNormalPercentualCircle(GLArea *, QPoint &, MeshModel &, GLfloat* , double* , double* , GLint* , float );
void drawVertex(CVertexO* );
void drawLine(GLArea *, QPoint &, QPoint &);
void drawSimplePolyLine(GLArea * gla, QPoint & gl_cur, float scale, std::vector<QPointF> * points);
void drawPercentualPolyLine(GLArea * , QPoint &, MeshModel &, GLfloat* , double* , double* , GLint* , float , std::vector<QPointF> * );

void generateCircle(std::vector<QPointF> &, int segments = 18);
void generateSquare(std::vector<QPointF> &, int segments = 1);
bool isOutOfBox(vcg::Box3f box, vcg::Point3f);
#endif
